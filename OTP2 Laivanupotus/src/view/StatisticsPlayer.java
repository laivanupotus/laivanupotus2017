package view;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * Luokkaa käytetään TableView näkymän populoimiseen.
 *
 * @author Joonas Kallinen
 *
 */

public class StatisticsPlayer {
	private final SimpleStringProperty p1name;
	private final SimpleStringProperty p1shots;
	private final SimpleStringProperty p1hits;
	private final SimpleStringProperty p2name;
	private final SimpleStringProperty p2shots;
	private final SimpleStringProperty p2hits;

	public StatisticsPlayer (String p1name, String p1shots, String p1hits, String p2name, String p2shots,
			String p2hits) {
		this.p1name = new SimpleStringProperty(p1name);
		this.p1shots = new SimpleStringProperty(p1shots);
		this.p1hits = new SimpleStringProperty(p1hits);
		this.p2name = new SimpleStringProperty(p2name);
		this.p2shots = new SimpleStringProperty(p2shots);
		this.p2hits = new SimpleStringProperty(p2hits);
	}

    public String getP1Name() {
        return p1name.get();
    }

    public void setP1Name(String fName) {
    	p1name.set(fName);
    }

    public String getP1Shots() {
        return p1shots.get();
    }

    public void setP1Shots(String fName) {
    	p1shots.set(fName);
    }

    public String getP1Hits() {
        return p1hits.get();
    }

    public void setP1Hits(String fName) {
    	p1hits.set(fName);
    }

    public String getP2Name() {
        return p2name.get();
    }

    public void setP2Name(String fName) {
    	p2name.set(fName);
    }

    public String getP2Shots() {
        return p2shots.get();
    }

    public void setP2Shots(String fName) {
    	p2shots.set(fName);
    }

    public String getP2Hits() {
        return p2hits.get();
    }

    public void setP2Hits(String fName) {
    	p2hits.set(fName);
    }
}
