package view;

import controller.Controller;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import model.KoordinaattiPari;
import model.PelaajanNimi;

/**
 *
 * Ohjelman GameGrid toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class GameGrid_Ctrl {

	private Language l = Language.getInstance();
	private static final int shipCountDefault = 24;
	private static final PelaajanNimi AI = new PelaajanNimi("AI");
	private int shipCount = shipCountDefault;
	private static boolean reset;

	private static boolean flagAddingShips;
	private static boolean flagShipInProgress;
	private static boolean flagPlay;
	private static boolean flagMulti;
	private static boolean flagIsTurn;
	private static boolean flagPlayerWins;

	private static ArrayList<GameButton> buttonPath;
	private static ArrayList<GameButton> playerShipButtons = new ArrayList<GameButton>();
	private static GameGrid_Ctrl instance;

	@FXML
	BorderPane gameGridBP;

	@FXML
	GridPane pane_GameGridPlayer;

	@FXML
	GridPane pane_GameGridOpponent;

	@FXML
	Label labelPelaajanNimi;
	@FXML
	Label labelVastustajanNimi;

	private GameGrid_Ctrl() {
	}

	/**
	 * Method to return instance of the class or create it before returning it
	 *
	 * @return Return instance of the class itself.
	 */
	public static GameGrid_Ctrl getInstance() {
		if (instance == null) {
			instance = new GameGrid_Ctrl();
		}
		return instance;
	}

	/**
	 * Action for a single button in the game grid
	 *
	 * @param e
	 *            FXML ActionEvent
	 */
	@FXML
	public void buttonAction(ActionEvent e) {
		GameButton button = new GameButton((Button) e.getSource());
		if (buttonPath == null) {
			resetColors();
		}
		Controller ctrl = Controller.getInstance();
		if (flagAddingShips && shipCount > 0) {
			if (!flagShipInProgress && !ctrl.onkoRuudussaLaiva(Settings_SavedValues.getInstance().getActivePlayer(),
					button.getKoordinaatit())) {
				buttonPath = new ArrayList<GameButton>();
				buttonPath.add(button);
				flagShipInProgress = true;
				decreaseShipCount(1);
				button.changeColorShipBeingAdded();
				MainGUI_Ctrl.getInstance().getCbShipSize().setDisable(true);
				if (shipCount == 0) {
					addShipFromButtonPath();
					playerShipButtons.add(button);
				}
			} else if (flagShipInProgress && buttonPath.size() != 0
					&& buttonPath.get(0).getKoordinaatit().getX() == button.getKoordinaatit().getX()
					&& buttonPath.get(0).getKoordinaatit().getY() == button.getKoordinaatit().getY()) {
				MainGUI_Ctrl.getInstance().getCbShipSize().setDisable(false);
				increaseShipCount(buttonPath.size());
				resetButtonPathColors();
				buttonPath.clear();
				flagShipInProgress = false;
			}
		}
		if (flagPlay) {
			// SINGLE PLAYER
			if (!flagMulti && ctrl.ammu(AI, button.getKoordinaatit())) {
				// Pelaajan vuoro
				if (ctrl.onkoRuudussaLaiva(AI, button.getKoordinaatit())) {
					playerHitOpponentShip(button);
					MainGUI_Ctrl.getInstance().setLabelOsumatVastustajanRuudukkoon(String.valueOf(
							Integer.valueOf(MainGUI_Ctrl.getInstance().getLabelOsumatVastustajanRuudukkoon()) + 1));
					if (ctrl.pelaajanLaivatUponnut(AI)) {
						playerSinkedOpponentFleet();
						gameRoundIsOver();
					}
				} else {
					playerMissedOpponentShip(button);
				}
				// Tekoälyn vuoro
				if (!flagPlayerWins && ctrl.ammu_AI(Settings_SavedValues.getInstance().getActivePlayer())) {
					KoordinaattiPari aiCoord = ctrl.aiCoord();
					if (ctrl.onkoRuudussaLaiva(Settings_SavedValues.getInstance().getActivePlayer(), aiCoord)) {
						getGameButton(aiCoord).changeColorShipHitPlayer();
						setStatusOpponentHitPlayerShip(aiCoord);
						MainGUI_Ctrl.getInstance().setLabelOsumatPelaajanRuudukkoon(String.valueOf(
								Integer.valueOf(MainGUI_Ctrl.getInstance().getLabelOsumatPelaajanRuudukkoon()) + 1));

						if (ctrl.pelaajanLaivatUponnut(Settings_SavedValues.getInstance().getActivePlayer())) {
							setStatusPlayerLost();
							gameRoundIsOver();
						}
					} else {
						setStatusOpponentMissedPlayerShip(aiCoord);
						opponentMissedPlayerShip(aiCoord);
					}
				}
			} else if (!flagMulti && !ctrl.ammu(AI, button.getKoordinaatit())) {
				MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nCantHitSameGrid") + "("
						+ button.getKoordinaatit().getX() + "," + button.getKoordinaatit().getY() + ")");
			}
			// MULTIPLAYER
			if (flagMulti && flagIsTurn) {
				Settings_SavedValues.getInstance().getClient().setGb(button);
			}
		}
	}

	@FXML
	private void collectButtonPath(Event event) {
		if (!labelPelaajanNimi.getText()
				.equalsIgnoreCase(Settings_SavedValues.getInstance().getActivePlayer().getNimi())) {
			labelPelaajanNimi.setText(Settings_SavedValues.getInstance().getActivePlayer().getNimi());
			if (!flagMulti)
				labelVastustajanNimi.setText(AI.getNimi());
			else
				labelVastustajanNimi.setText("");
		}
		if (flagShipInProgress) {
			boolean uniqueButton = true;
			GameButton button = new GameButton((Button) event.getSource());
			for (int i = 0; i < buttonPath.size(); i++) {
				if (buttonPath.get(i).getKoordinaatit().getX() == button.getKoordinaatit().getX()
						&& buttonPath.get(i).getKoordinaatit().getY() == button.getKoordinaatit().getY()) {
					uniqueButton = false;
				}
			}
			if (uniqueButton) {
				buttonPath.add(button);
				decreaseShipCount(1);
				button.changeColorShipBeingAdded();
			}
			addShipFromButtonPath();
			MainGUI_Ctrl.getInstance().setLabelShipsAvailable(String.valueOf(shipCount));
		}
		if (shipCount == 0 && !flagPlay) {
			setStatusPlayerAddedAllShips();
			flagAddingShips = false;
			MainGUI_Ctrl.getInstance().getCbShipSize().setDisable(true);
			flagIsTurn = true;
			disablePlayerGridButtons();
			enableOpponentGridButtons();
			flagPlay = true;
		}
	}

	private void addShipFromButtonPath() {
		if (shipCount < 1
				|| buttonPath.size() == Integer.valueOf(MainGUI_Ctrl.getInstance().getCbShipSize().getValue())) {
			if (!shipsOnArea()) {
				Controller.getInstance().lisaaLaiva(Settings_SavedValues.getInstance().getActivePlayer(), buttonPath);
				for (GameButton bP : buttonPath) {
					bP.changeColorShipAddedPlayer();
				}
				playerShipButtons.addAll(buttonPath);
				flagShipInProgress = false;
				MainGUI_Ctrl.getInstance().getCbShipSize().setDisable(false);
			} else {
				resetButtonPathColors();
				MainGUI_Ctrl.getInstance().getCbShipSize().setDisable(false);
				flagShipInProgress = false;
				increaseShipCount(buttonPath.size());
				MainGUI_Ctrl.getInstance().setLabelShipsAvailable(String.valueOf(shipCount));
				buttonPath.clear();
			}
		}
	}

	private void opponentMissedPlayerShip(KoordinaattiPari koordinaatit) {
		ObservableList<Node> pGB = pane_GameGridPlayer.getChildren();
		GameButton gb = new GameButton(
				(Button) pGB.get(Integer.valueOf(koordinaatit.getX() + "" + koordinaatit.getY())));
		gb.changeColorShipMissPlayer();
	}

	private void increaseShipCount(int value) {
		shipCount = shipCount + value;
		MainGUI_Ctrl.getInstance().setLabelShipsAvailable(String.valueOf(shipCount));
	}

	private void decreaseShipCount(int value) {
		shipCount = shipCount - value;
		MainGUI_Ctrl.getInstance().setLabelShipsAvailable(String.valueOf(shipCount));
	}

	private void resetButtonPathColors() {
		for (GameButton b : buttonPath) {
			if (shipsOnCoordinate(b)) {
				b.changeColorShipAddedPlayer();
			} else {
				b.changeColorShipDefault();
			}
		}
	}

	private GameButton getGameButton(KoordinaattiPari koordinaatit) {
		GameButton gb = null;
		if (buttonPath != null) {
			for (GameButton b : buttonPath) {
				if (b.getKoordinaatit().getX() == koordinaatit.getX()
						&& b.getKoordinaatit().getY() == koordinaatit.getY()) {
					gb = b;
				}
			}
		}
		if (gb == null || playerShipButtons != null) {
			for (GameButton b : playerShipButtons) {
				if (b.getKoordinaatit().getX() == koordinaatit.getX()
						&& b.getKoordinaatit().getY() == koordinaatit.getY()) {
					gb = b;
				}
			}
		}
		return gb;
	}

	/**
	 * Method used by buttonAction()
	 *
	 * @return Return boolean of the fact is the new ship trying to go over an
	 *         old ship.
	 */
	private boolean shipsOnArea() {
		boolean foundShip = false;
		for (GameButton b : buttonPath) {
			if (!foundShip) {
				foundShip = Controller.getInstance()
						.onkoRuudussaLaiva(Settings_SavedValues.getInstance().getActivePlayer(), b.getKoordinaatit());
			}
		}
		return foundShip;
	}

	private boolean shipsOnCoordinate(GameButton b) {
		if (Controller.getInstance().onkoRuudussaLaiva(Settings_SavedValues.getInstance().getActivePlayer(),
				b.getKoordinaatit())) {
			return true;
		} else {
			return false;
		}
	}

	private void setStatusPlayerAddedAllShips() {
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nAllShipsAdded"));

	}

	private void setStatusPlayerHitOpponentShip(GameButton button) {
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nYouHit") + "("
				+ button.getKoordinaatit().getX() + "," + button.getKoordinaatit().getY() + ")");

	}

	private void setStatusPlayedMissedOpponentShip(GameButton button) {
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nYouDidntHit") + "("
				+ button.getKoordinaatit().getX() + "," + button.getKoordinaatit().getY() + ")");

	}

	private void setStatusOpponentHitPlayerShip(KoordinaattiPari koordinaatit) {
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nOpponentHitYou") + "("
				+ koordinaatit.getX() + "," + koordinaatit.getY() + ")");
	}

	public void setStatusPlayerLost() {
		MainGUI_Ctrl.getInstance().setTfStatus(l.messages().getString("nYouLost"));
	}

	private void setStatusOpponentMissedPlayerShip(KoordinaattiPari koordinaatit) {
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nOpponentDidntHit") + "("
				+ koordinaatit.getX() + "," + koordinaatit.getY() + ")");
	}

	private void enablePlayerGridButtons() {
		ObservableList<Node> pGBp = pane_GameGridPlayer.getChildren();
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				GameButton gb = new GameButton((Button) pGBp.get(Integer.valueOf(x + "" + y)));
				gb.enableDisabledState();
			}
		}
	}

	private void disablePlayerGridButtons() {
		ObservableList<Node> pGBp = pane_GameGridPlayer.getChildren();
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				GameButton gb = new GameButton((Button) pGBp.get(Integer.valueOf(x + "" + y)));
				gb.disableDisabledState();
			}
		}
	}

	private void enableOpponentGridButtons() {
		ObservableList<Node> pGBo = pane_GameGridOpponent.getChildren();
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				GameButton gb = new GameButton((Button) pGBo.get(Integer.valueOf(x + "" + y)));
				gb.enableDisabledState();
			}
		}
	}

	private void disableOpponentGridButtons() {
		ObservableList<Node> pGBo = pane_GameGridOpponent.getChildren();
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				GameButton gb = new GameButton((Button) pGBo.get(Integer.valueOf(x + "" + y)));
				gb.disableDisabledState();
			}
		}
	}

	private void resetColors() {
		if (pane_GameGridPlayer != null) {
			ObservableList<Node> pGBp = pane_GameGridPlayer.getChildren();
			ObservableList<Node> pGBo = pane_GameGridOpponent.getChildren();
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					GameButton gb1 = new GameButton((Button) pGBp.get(Integer.valueOf(x + "" + y)));
					gb1.resetColor();
					GameButton gb2 = new GameButton((Button) pGBo.get(Integer.valueOf(x + "" + y)));
					gb2.resetColor();
				}
			}
		}
	}

	public void multiPlayerGridUpdate(boolean osu, KoordinaattiPari koordinaatit) {
		if (pane_GameGridPlayer != null) {
			ObservableList<Node> pGBp = pane_GameGridPlayer.getChildren();
			GameButton gb = new GameButton(
					(Button) pGBp.get(Integer.valueOf(koordinaatit.getX() + "" + koordinaatit.getY())));
			if (osu) {
				setStatusOpponentHitPlayerShip(koordinaatit);
				gb.changeColorShipHitPlayer();
			} else {
				opponentMissedPlayerShip(koordinaatit);
				gb.changeColorShipMissPlayer();
			}
		}
	}

	public boolean hasFleetSinked() {
		return Controller.getInstance().pelaajanLaivatUponnut(Settings_SavedValues.getInstance().getActivePlayer());
	}

	@FXML
	private void setGridPaneValue(Event event) {
		if (pane_GameGridPlayer == null) {
			pane_GameGridPlayer = (GridPane) event.getSource();
		}
	}

	/**
	 * Method used by buttonAction()
	 *
	 * @param gb
	 *            Button which was hit.
	 * @param xCoord
	 *            Button x coordinate.
	 * @param yCoord
	 *            Button y coordinate.
	 */
	public void playerHitOpponentShip(GameButton gb) {
		setStatusPlayerHitOpponentShip(gb);
		gb.changeColorShipHitOpponent();
	}

	/**
	 * Method used by buttonAction()
	 */
	public void playerSinkedOpponentFleet() {
		flagPlayerWins = true;
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nYouWon"));
	}

	/**
	 * Method used by buttonAction()
	 *
	 * @param gb
	 *            Button which was not hit.
	 * @param xCoord
	 *            Button x coordinate.
	 * @param yCoord
	 *            Button y coordinate.
	 */
	public void playerMissedOpponentShip(GameButton gb) {
		setStatusPlayedMissedOpponentShip(gb);
		gb.changeColorShipMissOpponent();
	}

	/**
	 * Actions when game round is over
	 */
	private void gameRoundIsOver() {
		roundEnd();
		MainGUI_Ctrl.getInstance().setLabelShipsAvailable(String.valueOf(shipCount));
		MainGUI_Ctrl.getInstance().setLabelOsumatPelaajanRuudukkoon(String.valueOf(0));
		MainGUI_Ctrl.getInstance().setLabelOsumatVastustajanRuudukkoon(String.valueOf(0));
		setReset(false);
	}

	public void gameRoundIsOverMP() {
		roundEnd();
		if (Settings_SavedValues.getInstance().getClient() != null) {
			Settings_SavedValues.getInstance().getClient().closeSocket();
		}
		if (Settings_SavedValues.getInstance().getServer() != null) {
			Settings_SavedValues.getInstance().getServer().closeServer();
		}
		setReset(false);
	}

	private void roundEnd() {
		shipCount = shipCountDefault;
		buttonPath = null;
		Controller.getInstance().roundEnd();
		enablePlayerGridButtons();
		disableOpponentGridButtons();
		flagAddingShips = true;
		flagPlay = false;
		flagPlayerWins = false;
		Controller.getInstance().uusiPelaaja(Settings_SavedValues.getInstance().getActivePlayer());
		MainGUI_Ctrl.getInstance().setTfStatus("* " + l.messages().getString("nNewGameBegins"));
		MainGUI_Ctrl.getInstance().getCbShipSize().setDisable(false);
	}

	@FXML
	private void enterGGBP(Event event) {
		if (isReset()) {
			gameRoundIsOver();
			resetColors();
		}
	}

	/*
	 * Getters and setters
	 */

	public static ArrayList<GameButton> getPlayerShipButtons() {
		return playerShipButtons;
	}

	public GridPane getPane_GameGridPlayer() {
		return pane_GameGridPlayer;
	}

	public GridPane getPane_GameGridOpponent() {
		return pane_GameGridOpponent;
	}

	public boolean isFlagAddingShips() {
		return flagAddingShips;
	}

	public void setFlagAddingShips(boolean flagAddingShips) {
		GameGrid_Ctrl.flagAddingShips = flagAddingShips;
	}

	public boolean isFlagShipInProgress() {
		return flagShipInProgress;
	}

	public void setFlagShipInProgress(boolean flagShipInProgress) {
		GameGrid_Ctrl.flagShipInProgress = flagShipInProgress;
	}

	public boolean isFlagPlay() {
		return flagPlay;
	}

	public void setFlagPlay(boolean flagPlay) {
		GameGrid_Ctrl.flagPlay = flagPlay;
	}

	public boolean isFlagMulti() {
		return flagMulti;
	}

	public void setFlagMulti(boolean flagMulti) {
		GameGrid_Ctrl.flagMulti = flagMulti;
	}

	public boolean isFlagIsTurn() {
		return flagIsTurn;
	}

	public void setFlagIsTurn(boolean flagIsTurn) {
		GameGrid_Ctrl.flagIsTurn = flagIsTurn;
	}

	public boolean isFlagPlayerWins() {
		return flagPlayerWins;
	}

	public void setFlagPlayerWins(boolean flagPlayerWins) {
		GameGrid_Ctrl.flagPlayerWins = flagPlayerWins;
	}

	public static boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		GameGrid_Ctrl.reset = reset;
	}

	public static int getShipcountdefault() {
		return shipCountDefault;
	}
}
