package view;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Client;
import model.Server;

/**
 *
 * Ohjelman MultiplayerGUI toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class MultiplayerGUI_Ctrl {

	@FXML
	ToggleGroup mpMulti;
	@FXML
	RadioButton createGame;
	@FXML
	RadioButton joinGame;
	@FXML
	TextField mpPort1;
	@FXML
	TextField mpPort2;
	@FXML
	TextField mpIP1;
	@FXML
	TextField mpIP2;
	@FXML
	TextField mpIP3;
	@FXML
	TextField mpIP4;
	@FXML
	Button okButton;
	@FXML
	Label ipaddress;
	@FXML
	HBox hboxCreateGame;
	@FXML
	HBox hboxJoinGame;

	private static MultiplayerGUI_Ctrl instance;

	private MultiplayerGUI_Ctrl() {
	}

	/*
	 * Method to return instance of the class or create it before returning it
	 */
	static MultiplayerGUI_Ctrl getInstance() {
		if (instance == null) {
			instance = new MultiplayerGUI_Ctrl();
		}
		return instance;
	}

	public void initialize() {
		try {
			ipaddress.setText(InetAddress.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			ipaddress.setText("");
			e.printStackTrace();
		}
		createGame.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				hboxJoinGame.setDisable(true);
				hboxCreateGame.setDisable(false);
			}
		});
		joinGame.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				hboxJoinGame.setDisable(false);
				hboxCreateGame.setDisable(true);
			}
		});

		mpPort1.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 5)
				mpPort1.setText(mpPort1.getText().substring(0, 5));
			try {
				Integer.parseInt(mpPort1.getText());
			} catch (NumberFormatException npe) {
				mpPort1.setText("");
			} catch (IllegalArgumentException iae) {
				mpPort1.setText("");
			}
		});
		mpPort2.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 5)
				mpPort2.setText(mpPort2.getText().substring(0, 5));
			try {
				Integer.parseInt(mpPort2.getText());
			} catch (NumberFormatException npe) {
				mpPort2.setText("");
			} catch (IllegalArgumentException iae) {
				mpPort2.setText("");
			}
		});
		mpIP1.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 3)
				mpIP1.setText(mpIP1.getText().substring(0, 3));
			try {
				Integer.parseInt(mpIP1.getText());
			} catch (NumberFormatException npe) {
				mpIP1.setText("");
			} catch (IllegalArgumentException iae) {
				mpIP1.setText("");
			}
		});
		mpIP2.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 3)
				mpIP2.setText(mpIP2.getText().substring(0, 3));
			try {
				Integer.parseInt(mpIP2.getText());
			} catch (NumberFormatException npe) {
				mpIP2.setText("");
			} catch (IllegalArgumentException iae) {
				mpIP2.setText("");
			}
		});
		mpIP3.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 3)
				mpIP3.setText(mpIP3.getText().substring(0, 3));
			try {
				Integer.parseInt(mpIP3.getText());
			} catch (NumberFormatException npe) {
				mpIP3.setText("");
			} catch (IllegalArgumentException iae) {
				mpIP3.setText("");
			}
		});
		mpIP4.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 3)
				mpIP4.setText(mpIP4.getText().substring(0, 3));
			try {
				Integer.parseInt(mpIP4.getText());
			} catch (NumberFormatException npe) {
				mpIP4.setText("");
			} catch (IllegalArgumentException iae) {
				mpIP4.setText("");
			}
		});
		hboxJoinGame.setDisable(true);
	}

	public static boolean isInstanced() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}

	@FXML
	private void fieldPort(KeyEvent event) {

	}

	@FXML
	private void fieldIP(KeyEvent event) {

	}

	@FXML
	private void ok(ActionEvent event) {
		Button b = (Button) event.getSource();
		Stage stage = (Stage) b.getScene().getWindow();
		try {
			RadioButton rb = (RadioButton) mpMulti.getSelectedToggle();
			if (rb.getId().equalsIgnoreCase("mpcreategame")) {
				// CREATE
				Server s = new Server(Integer.valueOf(mpPort1.getText()));
				Client c = new Client(Integer.valueOf(mpPort1.getText()));
				Settings_SavedValues.getInstance().setClientServer(c, s);
				Settings_SavedValues.getInstance().getServer().start();
				Settings_SavedValues.getInstance().getClient().start();
			} else if (rb.getId().equalsIgnoreCase("mpjoingame")) {
				// JOIN
				Client c = new Client(mpIP1.getText() + "." + mpIP2.getText() + "." + mpIP3.getText() + "." + mpIP4.getText(),
						Integer.valueOf(mpPort2.getText()));
				Settings_SavedValues.getInstance().setClientServer(c, null);
				Settings_SavedValues.getInstance().getClient().start();
			}
			System.out.println("Ikkuna suljettu.");
			GameGrid_Ctrl.getInstance().setFlagMulti(true);
		} catch (Exception e) {
			System.out.println("ERROR");
		}
		destroyInstance();
		stage.close();
	}

	public void destroyInstance() {
		System.out.println("B L147");
		instance = null;
	}
}
