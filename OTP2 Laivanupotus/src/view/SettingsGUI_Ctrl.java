package view;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * Ohjelman SettingsGUI toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class SettingsGUI_Ctrl {

	private Color väriPD, väriPO, väriPH, väriVD, väriVO, väriVH;
	private Language language = Language.getInstance();
	private String[] shipSizeOptions;
	private static SettingsGUI_Ctrl instance;

	@FXML
	ToggleGroup vaikeusaste;
	@FXML
	Button setShipSize;
	@FXML
	Button buttonResetColors;
	@FXML
	Button applyChanges;
	@FXML
	Button cancelChanges;
	@FXML
	ColorPicker väriPelaajaDefault;
	@FXML
	ColorPicker väriPelaajaOsuttu;
	@FXML
	ColorPicker väriPelaajaHuti;
	@FXML
	ColorPicker väriVastustajaDefault;
	@FXML
	ColorPicker väriVastustajaOsuttu;
	@FXML
	ColorPicker väriVastustajaHuti;
	@FXML
	TextField tfShipSize;

	private SettingsGUI_Ctrl() {
	}

	/*
	 * Method to return instance of the class or create it before returning it
	 */
	static SettingsGUI_Ctrl getInstance() {
		if (instance == null) {
			instance = new SettingsGUI_Ctrl();
		}
		return instance;
	}

	public void initialize() {
		System.out.println("Settings:initialize()");
		Settings_SavedValues.getInstance().initialize();
		System.out.println("vaikeusaste: " + (Controller.getInstance().getAIvaikeus() - 1));
		if ((0 <= Settings_SavedValues.getInstance().getAidif() - 1)
				&& (Settings_SavedValues.getInstance().getAidif() - 1) < vaikeusaste.getToggles().size())
			vaikeusaste.getToggles().get(Settings_SavedValues.getInstance().getAidif() - 1).setSelected(true);
		else
			vaikeusaste.getToggles().get(Integer.valueOf(Controller.getInstance().getAIvaikeus() - 1))
					.setSelected(true);
		väriPelaajaDefault.setValue(Settings_SavedValues.getInstance().getColorArray()[0]);
		väriPelaajaOsuttu.setValue(Settings_SavedValues.getInstance().getColorArray()[1]);
		väriPelaajaHuti.setValue(Settings_SavedValues.getInstance().getColorArray()[2]);
		väriVastustajaDefault.setValue(Settings_SavedValues.getInstance().getColorArray()[3]);
		väriVastustajaOsuttu.setValue(Settings_SavedValues.getInstance().getColorArray()[4]);
		väriVastustajaHuti.setValue(Settings_SavedValues.getInstance().getColorArray()[5]);
	}

	/*
	 * Method to check if the class has already been instanced. Returns true if
	 * instance of the class already exists, false otherwise
	 */
	public static boolean isInstanced() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Method for changing Player's and Opponent's fleet's colors
	 */
	@FXML
	private void ChangeColor(ActionEvent event) {
		ColorPicker cp = (ColorPicker) event.getSource();
		System.out.println("Color: " + cp.getValue());
		if (cp.getId().equalsIgnoreCase("väriPelaajaDefault")) {
			väriPD = cp.getValue();
		} else if (cp.getId().equalsIgnoreCase("väriPelaajaOsuttu")) {
			väriPO = cp.getValue();
		} else if (cp.getId().equalsIgnoreCase("väriPelaajaHuti")) {
			väriPH = cp.getValue();
		} else if (cp.getId().equalsIgnoreCase("väriVastustajaDefault")) {
			väriVD = cp.getValue();
		} else if (cp.getId().equalsIgnoreCase("väriVastustajaOsuttu")) {
			väriVO = cp.getValue();
		} else if (cp.getId().equalsIgnoreCase("väriVastustajaHuti")) {
			väriVH = cp.getValue();
		}
	}

	/*
	 * Method to change ship size options (drop down thing with numbers in the
	 * game).
	 */
	@FXML
	private void ChangeShipSize(ActionEvent event) {
		Button b;
		if (event == null)
			b = setShipSize;
		else
			b = (Button) event.getSource();
		b.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue,
					Boolean newPropertyValue) {
				if (!newPropertyValue) {
					b.setText(language.messages().getString("set"));
				}
			}
		});
		int intShipSize;
		try {
			intShipSize = Integer.parseInt(tfShipSize.getText());
			if (tfShipSize.getText().length() < 2 && intShipSize >= 2) {
				shipSizeOptions = getStringArrayFromInteger(intShipSize);
				b.setText(language.messages().getString("saved"));
			} else {
				if (tfShipSize.getText().length() >= 2)
					tfShipSize.setText(tfShipSize.getText().substring(0, 1));
				b.setText(language.messages().getString("notsaved"));
			}
		} catch (NumberFormatException nfe) {
			b.setText(language.messages().getString("set"));
			tfShipSize.setText("");
		}
	}

	@FXML
	private void textFieldEnter(KeyEvent ke) {
		if (ke.getCode() == KeyCode.ENTER) {
			ChangeShipSize(null);
		}
	}

	@FXML
	private void resetColors(ActionEvent event) {
		Settings_SavedValues.getInstance().setDefaultColors();
		initialize();
	}

	/*
	 * Forms a string from numbers starting with 2 and ending with given
	 * integer, splits string into a string array and returns it. Needed by
	 * ChangeShipSize() method.
	 */
	public String[] getStringArrayFromInteger(int integer) {
		String newString = "";
		for (int i = 2; i <= integer; i++) {
			newString += String.valueOf(i + ",");
		}
		return newString.split(",");
	}

	/*
	 * Method saves (or cancels) the value changes made in the Settings
	 * permanently per a session so they work in the game as intended.
	 */
	@FXML
	private void apply(ActionEvent event) {
		Button b = (Button) event.getSource();
		Stage stage = (Stage) b.getScene().getWindow();
		if (väriPD == null)
			väriPD = väriPelaajaDefault.getValue();
		if (väriPO == null)
			väriPO = väriPelaajaOsuttu.getValue();
		if (väriPH == null)
			väriPH = väriPelaajaHuti.getValue();
		if (väriVD == null)
			väriVD = väriVastustajaDefault.getValue();
		if (väriVO == null)
			väriVO = väriVastustajaOsuttu.getValue();
		if (väriVH == null)
			väriVH = väriVastustajaHuti.getValue();
		if (shipSizeOptions != null && shipSizeOptions.length > 0) {
			MainGUI_Ctrl.getInstance().setCbShipSize(shipSizeOptions);
		}
		RadioButton rb = (RadioButton) vaikeusaste.getSelectedToggle();
		int aidif = Integer.valueOf(String.valueOf(rb.getId().charAt(1)));
		Controller.getInstance().setAIvaikeus(aidif);
		Settings_SavedValues.getInstance().setAidif(aidif);
		Settings_SavedValues.getInstance().saveColors(väriPD, väriPO, väriPH, väriVD, väriVO, väriVH);
		destroyInstance();
		stage.close();
	}

	@FXML
	private void cancel(ActionEvent event) {
		Button b = (Button) event.getSource();
		Stage stage = (Stage) b.getScene().getWindow();
		destroyInstance();
		stage.close();
	}

	public Color getVäriPD() {
		return väriPD;
	}

	public Color getVäriPO() {
		return väriPO;
	}

	public Color getVäriPH() {
		return väriPH;
	}

	public Color getVäriVD() {
		return väriVD;
	}

	public Color getVäriVO() {
		return väriVO;
	}

	public Color getVäriVH() {
		return väriVH;
	}

	/*
	 * Method to reset class instance so it can be created again at later time.
	 */
	public void destroyInstance() {
		instance = null;
	}

}
