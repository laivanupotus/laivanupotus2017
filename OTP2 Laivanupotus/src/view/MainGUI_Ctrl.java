package view;

import java.io.IOException;
import java.util.ResourceBundle;
import controller.Controller;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.FWriter;

/**
 *
 * Ohjelman MainGUI toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class MainGUI_Ctrl {

	@FXML
	AnchorPane root;

	@FXML
	private ResourceBundle resources = Language.getInstance().messages();
	@FXML
	private Menu menuTiedosto;
	@FXML
	private Menu menuPeli;
	@FXML
	private Menu menuApua;
	@FXML
	private Menu menuLanguage;

	@FXML
	private Menu menuUusiPeli;

	@FXML
	private ComboBox<String> cbShipSize;

	@FXML
	private TextArea tfStatus;

	@FXML
	private TabPane mainTabPane;

	@FXML
	private Tab defaultViewTab;
	@FXML
	private Tab activeGameTab;

	@FXML
	private Label labelShipsAvailable;
	@FXML
	private Label labelPelaajanLaivastonKoko;
	@FXML
	private Label labelOsumatPelaajanRuudukkoon;
	@FXML
	private Label labelOsumatVastustajanRuudukkoon;
	@FXML
	private Label difficultyActive;
	@FXML
	private AnchorPane gameGridWrapper;

	private static MainGUI_Ctrl instance;

	private MainGUI_Ctrl() {
	}

	/*
	 * Method to return instance of the class or create it before returning it
	 */
	public static MainGUI_Ctrl getInstance() {
		if (instance == null) {
			instance = new MainGUI_Ctrl();
		}
		return instance;
	}

	public void initialize() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setController(GameGrid_Ctrl.getInstance());
			loader.setLocation(getClass().getResource("/view/GameGrid.fxml"));
			loader.setResources(Language.getInstance().messages());
			Parent root;
			root = loader.load();
			gameGridWrapper.getChildren().add(root);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method to create actions for menu in the main gui. - Different menu
	 * selections are divided by their text, some actions might call for other
	 * methods to keep this structure as clean as possible.
	 */
	@FXML
	private void MenuItemAction(ActionEvent event) {
		SingleSelectionModel<Tab> selectionModel = mainTabPane.getSelectionModel();
		MenuItem mItem = (MenuItem) event.getSource();
		String mText = mItem.getText();
		if (mText.equalsIgnoreCase(resources.getString("open"))) {
		} else if (mText.equalsIgnoreCase(resources.getString("saveas"))) {
		} else if (mText.equalsIgnoreCase(resources.getString("close"))) {
			exitPlatform();
		} else if (mText.equalsIgnoreCase(resources.getString("sp"))) {
			gameGridWrapper.setDisable(false);
			prepareForNewGame();
			selectionModel.select(activeGameTab);
		} else if (mText.equalsIgnoreCase(resources.getString("mp"))) {
			multiplayerStage();
			gameGridWrapper.setDisable(true);
			prepareForNewGame();
			selectionModel.select(activeGameTab);
		} else if (mText.equalsIgnoreCase(resources.getString("player"))) {
			pelaajaStage();
			menuUusiPeli.setDisable(false);
		} else if (mText.equalsIgnoreCase(resources.getString("stats"))) {
			tilastotStage();
		} else if (mText.equalsIgnoreCase(resources.getString("settings"))) {
			asetuksetStage();
		} else if (mText.equalsIgnoreCase(resources.getString("checkupdates"))) {
			root.setRotate(root.getRotate() * (-1));
		} else if (mText.equalsIgnoreCase(resources.getString("guide"))) {
			opasStage();
		} else {
			System.out.println("ERROR - NOT FOUND");
		}
	}

	/**
	 *
	 * @param event
	 */
	@FXML
	private void changeLanguage(ActionEvent event) {
		RadioMenuItem rmi = (RadioMenuItem) event.getSource();
		for (int i = 0; i < rmi.getParentMenu().getItems().size(); i++) {
			RadioMenuItem r = (RadioMenuItem) rmi.getParentMenu().getItems().get(i);
			if (r.isSelected()) {
				r.setSelected(false);
			}
		}
		Language.getInstance().setLanguage(rmi.getId().substring(0, 2).toLowerCase(),
				rmi.getId().substring(0, 2).toUpperCase());

		Scene scene = root.getScene();
		try {
			MainGUI_Ctrl controller = MainGUI_Ctrl.getInstance();
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/MainGUI.fxml"));
			fxmlLoader.setResources(Language.getInstance().messages());
			fxmlLoader.setController(controller);
			scene.setRoot(fxmlLoader.load());
			MainGUI.getStage().setTitle(resources.getString("title"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		rmi.setSelected(true);
	}

	/**
	 * Tasks performed before starting a new game from the main menu.
	 */
	private void prepareForNewGame() {
		if (!(Controller.getInstance()
				.getPelaajanLaivojenYhteiskoko(Settings_SavedValues.getInstance().getActivePlayer()) == 0)) {
			Controller.getInstance().poistaPelaaja(Settings_SavedValues.getInstance().getActivePlayer());
			Controller.getInstance().uusiPelaaja(Settings_SavedValues.getInstance().getActivePlayer());
		}
		if (getCbShipSize() == null || getCbShipSize().getItems().size() == 0) {
			GameGrid_Ctrl.getInstance().setFlagAddingShips(true);
			setLabelShipsAvailable(String.valueOf(GameGrid_Ctrl.getShipcountdefault()));
			setCbShipSize(SettingsGUI_Ctrl.getInstance().getStringArrayFromInteger(5));
			SettingsGUI_Ctrl.getInstance().destroyInstance();
		}
		if (cbShipSize.isDisabled())
			cbShipSize.setDisable(false);
		switch (Controller.getInstance().getAIvaikeus()) {
		case 1:
			difficultyActive.setText(resources.getString("difeasy"));
			break;
		case 2:
			difficultyActive.setText(resources.getString("difnormal"));
			break;
		case 3:
			difficultyActive.setText(resources.getString("difhard"));
			break;
		case 4:
			difficultyActive.setText(resources.getString("difcheater"));
			break;
		}
		activeGameTab.setDisable(false);
		menuPeli.setDisable(true);
		menuLanguage.setDisable(true);
	}

	/**
	 * Tab action when tab is changed.
	 */
	@FXML
	private void TabAction(Event event) {
		mainTabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> o, Object o1, Object o2) {
				if (((Tab) o2).getId().equalsIgnoreCase("defaultViewTab")
						&& ((Tab) o1).getId().equalsIgnoreCase("activeGameTab")) {
					((Tab) o1).setDisable(true);
					menuPeli.setDisable(false);
					menuLanguage.setDisable(false);
					GameGrid_Ctrl.getInstance().setReset(true);
				}
			}
		});
	}


	/**
	 * Method to load SettingsGUI.fxml into a scene.
	 */
	private void asetuksetStage() {
		try {
			if (!SettingsGUI_Ctrl.isInstanced()) {
				Stage settingsStage = new Stage();
				settingsStage.setTitle(resources.getString("title"));

				FXMLLoader loader = new FXMLLoader();
				loader.setController(SettingsGUI_Ctrl.getInstance());
				loader.setLocation(getClass().getResource("/view/SettingsGUI.fxml"));
				loader.setResources(Language.getInstance().messages());
				Parent root;
				root = loader.load();

				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				settingsStage.setScene(scene);
				settingsStage.show();
				settingsStage.setMinWidth(420);
				settingsStage.setMinHeight(440);
				settingsStage.setMaxWidth(420);

				settingsStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
					public void handle(WindowEvent we) {
						SettingsGUI_Ctrl.getInstance().destroyInstance();
					}
				});
				// SettingsGUI_Ctrl.getInstance().setLanguageOptions();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Method to load PlayerSettingsGUI.fxml into a scene.
	 */
	private void pelaajaStage() {
		try {
			if (!PlayerSettingsGUI_Ctrl.isInstanced()) {
				Stage playerSettingsStage = new Stage();
				playerSettingsStage.setTitle(resources.getString("title"));

				FXMLLoader loader = new FXMLLoader();
				loader.setController(PlayerSettingsGUI_Ctrl.getInstance());
				loader.setLocation(getClass().getResource("/view/PlayerSettingsGUI.fxml"));
				loader.setResources(Language.getInstance().messages());
				Parent root;
				root = loader.load();

				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				playerSettingsStage.setScene(scene);
				playerSettingsStage.show();
				playerSettingsStage.setMinWidth(420);
				playerSettingsStage.setMinHeight(440);
				playerSettingsStage.setMaxWidth(420);

				playerSettingsStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
					public void handle(WindowEvent we) {
						PlayerSettingsGUI_Ctrl.getInstance().destroyInstance();
					}
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Method to load StatisticsGUI.fxml into a scene.
	 */
	private void tilastotStage() {
		try {
			if (!StatisticsGUI_Ctrl.isInstanced()) {
				Stage tilastotStage = new Stage();
				tilastotStage.setTitle(resources.getString("title"));

				FXMLLoader loader = new FXMLLoader();
				loader.setController(StatisticsGUI_Ctrl.getInstance());
				loader.setLocation(getClass().getResource("/view/StatisticsGUI.fxml"));
				loader.setResources(Language.getInstance().messages());
				Parent root;
				root = loader.load();

				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				tilastotStage.setScene(scene);
				tilastotStage.show();
				tilastotStage.setMinWidth(640);
				tilastotStage.setMinHeight(360);
				tilastotStage.setMaxWidth(640);
				tilastotStage.setMaxHeight(360);

				tilastotStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
					public void handle(WindowEvent we) {
						StatisticsGUI_Ctrl.getInstance().destroyInstance();
					}
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Method to load GuideGUI.fxml into a scene.
	 */
	private void opasStage() {
		try {
			if (!GuideGUI_Ctrl.isInstanced()) {
				Stage guideStage = new Stage();
				guideStage.setTitle(resources.getString("title"));

				FXMLLoader loader = new FXMLLoader();
				loader.setController(GuideGUI_Ctrl.getInstance());
				loader.setLocation(getClass().getResource("/view/GuideGUI.fxml"));
				loader.setResources(Language.getInstance().messages());
				Parent root;
				root = loader.load();

				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				guideStage.setScene(scene);
				guideStage.show();
				guideStage.setMinWidth(420);
				guideStage.setMinHeight(440);
				guideStage.setMaxWidth(420);

				guideStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
					public void handle(WindowEvent we) {
						GuideGUI_Ctrl.getInstance().destroyInstance();
					}
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to load MultiPlayerGUI.fxml into a scene.
	 */
	private void multiplayerStage() {
		try {
			if (!GuideGUI_Ctrl.isInstanced()) {
				Stage multiplayerStage = new Stage();
				multiplayerStage.setTitle(resources.getString("title"));

				FXMLLoader loader = new FXMLLoader();
				loader.setController(MultiplayerGUI_Ctrl.getInstance());
				loader.setLocation(getClass().getResource("/view/MultiplayerGUI.fxml"));
				loader.setResources(Language.getInstance().messages());
				Parent root;
				root = loader.load();

				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				multiplayerStage.setScene(scene);
				multiplayerStage.show();
				multiplayerStage.setMinWidth(420);
				multiplayerStage.setMinHeight(240);
				multiplayerStage.setMaxWidth(420);

				multiplayerStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
					public void handle(WindowEvent we) {
						MultiplayerGUI_Ctrl.getInstance().destroyInstance();
					}
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method for Platform.exit()
	 */
	private void exitPlatform() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				System.out.println("MainGUI_Ctrl:exitPlatform():run()");
				FWriter.getInstance().saveOnExit();
				Controller.getInstance().gameExit();
				Platform.exit();
			}
		});
	}

	/*
	 * Getters & Setters
	 */

	public AnchorPane getGameGridWrapper() {
		return gameGridWrapper;
	}

	public ComboBox<String> getCbShipSize() {
		return cbShipSize;
	}

	public void setCbShipSize(String[] shipOptions) {
		this.cbShipSize.setItems(FXCollections.observableArrayList(shipOptions));
		cbShipSize.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> o, Object o1, Object o2) {
				// GameGrid_Ctrl.getInstance().setShipSize(Integer.valueOf(o2.toString()));
			}
		});
		cbShipSize.setValue(shipOptions[0]);
	}

	public TextArea getTfStatus() {
		return tfStatus;
	}

	public void setTfStatus(String text) {
		tfStatus.setEditable(true);
		this.tfStatus.setText(text + "\n" + tfStatus.getText());
		;
		tfStatus.setEditable(false);
	}

	public Label getLabelShipsAvailable() {
		return labelShipsAvailable;
	}

	public void setLabelShipsAvailable(String labelShipsAvailable) {
		this.labelShipsAvailable.setText(labelShipsAvailable);
	}

	public String getLabelPelaajanLaivastonKoko() {
		return labelPelaajanLaivastonKoko.getText();
	}

	public void setLabelPelaajanLaivastonKoko(String labelPelaajanLaivastonKoko) {
		this.labelPelaajanLaivastonKoko.setText(labelPelaajanLaivastonKoko);
	}

	public String getLabelOsumatPelaajanRuudukkoon() {
		return labelOsumatPelaajanRuudukkoon.getText();
	}

	public void setLabelOsumatPelaajanRuudukkoon(String labelOsumatPelaajanRuudukkoon) {
		this.labelOsumatPelaajanRuudukkoon.setText(labelOsumatPelaajanRuudukkoon);
	}

	public String getLabelOsumatVastustajanRuudukkoon() {
		return labelOsumatVastustajanRuudukkoon.getText();
	}

	public void setLabelOsumatVastustajanRuudukkoon(String labelOsumatVastustajanRuudukkoon) {
		this.labelOsumatVastustajanRuudukkoon.setText(labelOsumatVastustajanRuudukkoon);
	}

}
