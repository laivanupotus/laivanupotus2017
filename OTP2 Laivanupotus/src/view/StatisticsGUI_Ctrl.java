package view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tilastot.Kaksinpeli;
import tilastot.PelaajanKokonaistilastot;
import tilastot.PelinPelaaja;
import tilastot.Tilastot_IF;
import controller.Controller;

/**
 *
 * Ohjelman StatisticsGUI toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class StatisticsGUI_Ctrl {

	@FXML
	TableView<StatisticsTotal> tableTotal;
	@FXML
	TableView<StatisticsPlayer> tablePlayer;
	@FXML
	TableColumn<StatisticsTotal, String> name;
	@FXML
	TableColumn<StatisticsTotal, String> statgames;
	@FXML
	TableColumn<StatisticsTotal, String> statwins;
	@FXML
	TableColumn<StatisticsTotal, String> statlosses;
	@FXML
	TableColumn<StatisticsTotal, String> statshots;
	@FXML
	TableColumn<StatisticsTotal, String> stathits;

	@FXML
	TableColumn<StatisticsPlayer, String> p1name;
	@FXML
	TableColumn<StatisticsPlayer, String> p1shots;
	@FXML
	TableColumn<StatisticsPlayer, String> p1hits;
	@FXML
	TableColumn<StatisticsPlayer, String> p2name;
	@FXML
	TableColumn<StatisticsPlayer, String> p2shots;
	@FXML
	TableColumn<StatisticsPlayer, String> p2hits;

	private static StatisticsGUI_Ctrl instance;

	private StatisticsGUI_Ctrl() {
	}

	/*
	 * Method to return instance of the class or create it before returning it
	 */
	public static StatisticsGUI_Ctrl getInstance() {
		if (instance == null) {
			instance = new StatisticsGUI_Ctrl();
		}
		return instance;
	}

	public void initialize() {
		System.out.println("StatisticsGUI_Ctrl:initialize()");
		Tilastot_IF t = Controller.getInstance().getTilastot();
		t.downloadKaksinpelit();
		// KOKONAISTILASTOT
		List<StatisticsTotal> temp1 = new ArrayList<StatisticsTotal>();
		Iterator<String> iNimi = t.getKaksinpelit().getKokonaistilastot().getPelaajienNimet().iterator();
		while (iNimi.hasNext()) {
			PelaajanKokonaistilastot pkt = t.getKaksinpelit().getKokonaistilastot()
					.getPelaajanKokonaistilastot(iNimi.next());
			temp1.add(new StatisticsTotal(pkt.getNimi(), String.valueOf(pkt.getPelit()),
					String.valueOf(pkt.getVoitot()), String.valueOf(pkt.getTappiot()),
					String.valueOf(pkt.getLaukaukset()), String.valueOf(pkt.getOsumat())));
		}
		ObservableList<StatisticsTotal> dataTotal = FXCollections.observableArrayList(temp1);
		temp1.clear();
		if (dataTotal != null && dataTotal.size() == 0) {
			System.out.println("NO TOTAL DATA AVAILABLE");
			dataTotal.add(new StatisticsTotal("-","-","-","-","-","-"));
		}
		name.setCellValueFactory(new PropertyValueFactory<StatisticsTotal, String>("name"));
		statgames.setCellValueFactory(new PropertyValueFactory<StatisticsTotal, String>("statgames"));
		statwins.setCellValueFactory(new PropertyValueFactory<StatisticsTotal, String>("statwins"));
		statlosses.setCellValueFactory(new PropertyValueFactory<StatisticsTotal, String>("statlosses"));
		statshots.setCellValueFactory(new PropertyValueFactory<StatisticsTotal, String>("statshots"));
		stathits.setCellValueFactory(new PropertyValueFactory<StatisticsTotal, String>("stathits"));
		tableTotal.setItems(dataTotal);

		// PELAAJATILASTOT
		List<StatisticsPlayer> temp2 = new ArrayList<StatisticsPlayer>();
		Map<String, Kaksinpeli> mskp = t.getKaksinpelit().getKaksinpelit();
		for (Kaksinpeli kp : mskp.values()) {
			PelinPelaaja p1 = kp.getPelinPelaaja1();
			PelinPelaaja p2 = kp.getPelinPelaaja2();
			temp2.add(new StatisticsPlayer(p1.getPelaajanNimi(), String.valueOf(p1.getLaukaukset()),
					String.valueOf(p1.getOsumat()), p2.getPelaajanNimi(), String.valueOf(p2.getLaukaukset()),
					String.valueOf(p2.getOsumat())));
		}
		ObservableList<StatisticsPlayer> dataPlayer = FXCollections.observableArrayList(temp2);
		temp2.clear();
		if (dataPlayer != null && dataPlayer.size() == 0) {
			System.out.println("NO PLAYER DATA AVAILABLE");
			dataPlayer.add(new StatisticsPlayer("-","-","-","-","-","-"));
		}
		p1name.setCellValueFactory(new PropertyValueFactory<StatisticsPlayer, String>("P1Name"));
		p1shots.setCellValueFactory(new PropertyValueFactory<StatisticsPlayer, String>("P1Shots"));
		p1hits.setCellValueFactory(new PropertyValueFactory<StatisticsPlayer, String>("P1Hits"));
		p2name.setCellValueFactory(new PropertyValueFactory<StatisticsPlayer, String>("P2Name"));
		p2shots.setCellValueFactory(new PropertyValueFactory<StatisticsPlayer, String>("P2Shots"));
		p2hits.setCellValueFactory(new PropertyValueFactory<StatisticsPlayer, String>("P2Hits"));
		tablePlayer.setItems(dataPlayer);
	}

	/*
	 * Method to check if the class has already been instanced. Returns true if
	 * instance of the class already exists, false otherwise
	 */
	public static boolean isInstanced() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Method to reset class instance so it can be created again at later time.
	 */
	public void destroyInstance() {
		instance = null;
	}

}
