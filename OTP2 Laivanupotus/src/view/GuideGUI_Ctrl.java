package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 *
 * Ohjelman GuideGUI toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class GuideGUI_Ctrl {

	private static GuideGUI_Ctrl instance;

	@FXML
	TextArea taNotes;

	private GuideGUI_Ctrl() {
	}

	/*
	 * Method to return instance of the class or create it before returning it
	 */
	static GuideGUI_Ctrl getInstance() {
		if (instance == null) {
			instance = new GuideGUI_Ctrl();
		}
		return instance;
	}

	public void initialize() {
		taNotes.setText(Settings_SavedValues.getInstance().loadNotes());
	}

	/*
	 * Method to check if the class has already been instanced. Returns true if
	 * instance of the class already exists, false otherwise
	 */
	public static boolean isInstanced() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}

	@FXML
	private void ok(ActionEvent event) {
		Button b = (Button) event.getSource();
		Stage stage = (Stage) b.getScene().getWindow();
		Settings_SavedValues.getInstance().saveNotes(taNotes.getText());
		System.out.println("Ikkuna suljettu.");
		destroyInstance();
		stage.close();
	}

	/*
	 * Method to reset class instance so it can be created again at later time.
	 */
	public void destroyInstance() {
		instance = null;
	}

}
