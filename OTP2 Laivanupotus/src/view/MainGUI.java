package view;

import java.util.ResourceBundle;

import controller.Controller;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.FWriter;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 *
 * MainGUI class starts the program, includes 'public static void main(String[] args)' method
 * - Opens the primaryStage with MainGUI.fxml set as its scene
 * - All primary actions of MainGUI.fxml scene will be performed in MainGUI_Ctrl (or MainGUI_Ctrl
 * calls methods from other classes)
 *
 * @author Joonas Kallinen
 *
 */

public class MainGUI extends Application {
	private FXMLLoader loader;
	private static MainGUI instance;
	private Parent root;
	public static Stage stage;

	public void init() {

	}

	public static MainGUI getInstance() {
		if (instance == null) {
			instance = new MainGUI();
		}
		return instance;
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			stage = primaryStage;
			Language lng = Language.getInstance();
			lng.setLanguage("en", "EN");

			stage.setTitle(lng.messages().getString("title"));

			loader = new FXMLLoader();
			ResourceBundle bundle = lng.messages();
			loader.setController(MainGUI_Ctrl.getInstance());
			loader.setResources(bundle);
			loader.setLocation(getClass().getResource("/view/MainGUI.fxml"));
			root = loader.load();

			Controller.getInstance();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
			stage.show();

			stage.setMinWidth(960.0);
			stage.setMinHeight(480.0);
			stage.minWidthProperty().bind(scene.heightProperty().multiply(2));
			stage.minHeightProperty().bind(scene.widthProperty().divide(2));


			stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				public void handle(WindowEvent we) {
					System.out.println("MainGUI:setOnCloseRequest():handle()");
					Controller.getInstance().gameExit();
					FWriter.getInstance().saveOnExit();
					Platform.exit();
					System.exit(0);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Parent getRoot() {
		return root;
	}

	public static Stage getStage() {
		return stage;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
