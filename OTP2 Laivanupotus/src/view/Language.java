package view;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * Ohjelman kieliä käsittelevä luokka.
 *
 * @author Jesse Vauhkonen
 *
 */

public class Language {
	private static Language INSTANCE = null;
	private ResourceBundle messages;
	private Locale locale;

	private Language(){

	}

    public static Language getInstance(){
        if ( INSTANCE == null ){
            INSTANCE = new Language();
        }
        return INSTANCE;
    }

    public void setLanguage(String lan, String cou){
    	System.out.println("lan: " + lan);
    	System.out.println("cou: " + cou);
    	locale = new Locale(lan, cou);
    	if ( lan.equals("fi")){
    		messages = ResourceBundle.getBundle("properties.MessagesBundle_fi_FI", locale);
    	} else if ( lan.equals("en")){
    		messages = ResourceBundle.getBundle("properties.MessagesBundle_en_EN", locale);
    	} else if ( lan.equals("ja")){
    		messages = ResourceBundle.getBundle("properties.MessagesBundle_ja_JA", locale);
    	} else {
    		messages = ResourceBundle.getBundle("properties.MessagesBundle_en_EN", locale);
    	}
    }

    public ResourceBundle messages(){
    	return messages;
    }

}
