package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import model.Client;
import model.FReader;
import model.PelaajanNimi;
import model.Server;
import view.Converter;

/**
 *
 * Säilyttää ohjelman ajonaikaisia arvoja, pyytää lukemaan niitä tiedostoista sekä palauttaa niitä tarvitussa muodossa.
 *
 * @author Joonas Kallinen
 *
 */

public class Settings_SavedValues {
	private int aidif;
	private List<String> playerNames;
	private List<Color> colors;
	private PelaajanNimi activePlayer;
	private List<String> notes;
	private List<String> loadColors;
	private static Settings_SavedValues instance;
	private ResourceBundle resources = Language.getInstance().messages();
	private Client client;
	private Server server;

	private Settings_SavedValues() {
	}

	public static Settings_SavedValues getInstance() {
		if (instance == null) {
			instance = new Settings_SavedValues();
		}
		return instance;
	}

	public void initialize() {
		System.out.println("savedValues:initialize()");
		playerNames = FReader.getInstance().readFileIgnoreComments("localplayers");
		if (loadColors == null) {
			loadColors = FReader.getInstance().readFileIgnoreComments("colors");
			if (loadColors != null) {
				colors = Converter.stringListToColorList(loadColors);
			}
		}
		List<String> lDif = FReader.getInstance().readFileIgnoreComments("difficulty");
		if (lDif != null)
			aidif = Integer.valueOf(lDif.get(0));
	}

	// COLOR METHODS

	public void saveColors(Color väriPD, Color väriPO, Color väriPH, Color väriVD, Color väriVO, Color väriVH) {
		List<Color> lc = new ArrayList<Color>();
		lc.add(väriPD);
		lc.add(väriPO);
		lc.add(väriPH);
		lc.add(väriVD);
		lc.add(väriVO);
		lc.add(väriVH);
		if (colors == null) {
			colors = new ArrayList<Color>();
		}
		List<Color> temp = new ArrayList<Color>();
		Iterator<Color> itr = lc.iterator();
		int i = 0;
		while (itr.hasNext()) {
			Color c = itr.next();
			if (c != null) {
				temp.add(i, c);
			} else if (i < colors.size() && colors.get(i) != null) {
				temp.add(i, colors.get(i));
			} else {
				temp.add(i, new Color(1, 1, 1, 1));
			}
			i++;
		}
		colors.clear();
		colors.addAll(temp);
	}

	public Color[] getColorArray() {
		if (colors == null) {
			setDefaultColors();
		}
		Color[] c1 = new Color[colors.size()];
		for (int i = 0; i < colors.size(); i++) {
			c1[i] = colors.get(i);
		}
		return c1;
	}

	public void setDefaultColors() {
		if (colors == null) {
			colors = new ArrayList<Color>();
		}
		colors.clear();
		colors.add(new Color(Converter.convertRGBtoFloat(0), Converter.convertRGBtoFloat(0),
				Converter.convertRGBtoFloat(0), 1));
		colors.add(new Color(Converter.convertRGBtoFloat(255), Converter.convertRGBtoFloat(0),
				Converter.convertRGBtoFloat(0), 1));
		colors.add(new Color(Converter.convertRGBtoFloat(64), Converter.convertRGBtoFloat(64),
				Converter.convertRGBtoFloat(255), 1));
		colors.add(new Color(Converter.convertRGBtoFloat(0), Converter.convertRGBtoFloat(0),
				Converter.convertRGBtoFloat(0), 1));
		colors.add(new Color(Converter.convertRGBtoFloat(0), Converter.convertRGBtoFloat(0),
				Converter.convertRGBtoFloat(0), 1));
		colors.add(new Color(Converter.convertRGBtoFloat(64), Converter.convertRGBtoFloat(64),
				Converter.convertRGBtoFloat(255), 1));
	}

	public ObservableList<String> getColors() {
		if (colors == null) {
			setDefaultColors();
		}
		return FXCollections.observableList(Converter.colorListToStringList(colors));
	}

	public void clearOldColors() {
		if (colors == null) {
			colors = new ArrayList<Color>();
		} else {
			colors.clear();
		}
	}

	// PLAYER METHODS

	public ObservableList<String> getPlayerNames() {
		sortPlayers();
		return FXCollections.observableList(playerNames);
	}

	public ObservableList<String> getPlayerNamesUnsorted() {
		List<String> list = getPlayerNamesMinusDefault();
		list.add(0, resources.getString("default"));
		return (ObservableList<String>) list;
	}

	public ObservableList<String> getPlayerNamesMinusDefault() {
		List<String> names = new ArrayList<String>();
		if (playerNames != null) {
			names.addAll(playerNames);
		}
		Collections.sort(names);
		if (names.contains(resources.getString("default"))) {
			do {
				names.remove(resources.getString("default"));
			} while (names.contains(resources.getString("default")));
		}
		return FXCollections.observableList(names);
	}

	public void addPlayerName(String name) {
		playerNames.add(name);
	}

	public void setPlayerNames(List<String> names) {
		playerNames = names;
	}

	private void sortPlayers() {
		if (playerNames != null) {
			Collections.sort(playerNames);
			playerNames = getPlayerNamesMinusDefault();
		} else {
			playerNames = new ArrayList<String>();
		}
		playerNames.add(0, resources.getString("default"));
	}

	public void removePlayer(String name) {
		playerNames.remove(name);
	}

	public PelaajanNimi getActivePlayer() {
		if (activePlayer == null)
			activePlayer = new PelaajanNimi(resources.getString("default"));
		return activePlayer;
	}

	public void setActivePlayer(PelaajanNimi activePlayer) {
		this.activePlayer = activePlayer;
	}

	public void saveNotes(String text) {
		if (notes == null) {
			notes = new ArrayList<String>();
		}
		notes.clear();
		notes.add(0, text);
	}

	public String loadNotes() {
		StringBuilder text = new StringBuilder();
		if (notes == null || notes.size() == 0) {
			notes = FReader.getInstance().readFileIgnoreComments("notes");
		}
		if (notes != null) {
			Iterator<String> itr = notes.iterator();
			while (itr.hasNext()) {
				int i = 1;
				System.out.println("i: " + i);
				i++;
				text.append(itr.next());
				if (itr.hasNext()) {
					text.append(" \n");
				}
			}
			System.out.println("### notes:" + text.toString());
			return text.toString();
		} else
			return new String();
	}

	public ObservableList<String> getNotes() {
		if (notes == null) {
			notes = FReader.getInstance().readFileIgnoreComments("notes");
		}
		if (notes == null) {
			return null;
		} else {
			return FXCollections.observableList(notes);
		}
	}

	// MULTIPLAYER METHODS

	public void setClientServer(Client c, Server s) {
		if (s != null) {
			server = s;
		}
		client = c;
	}

	public Client getClient() {
		return client;
	}

	public Server getServer() {
		return server;
	}

	// AI DIFFICULTY METHODS
	public void setAidif(int aidif) {
		this.aidif = aidif;
	}

	public int getAidif() {
		return aidif;
	}

	public ObservableList<String> getDifficulty() {
		List<String> al = new ArrayList<String>();
		al.add(String.valueOf(getAidif()));
		return FXCollections.observableList(al);
	}

}
