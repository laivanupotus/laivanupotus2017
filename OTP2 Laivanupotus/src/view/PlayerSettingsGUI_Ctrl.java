package view;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.Name;
import model.PelaajanNimi;

/**
 *
 * Ohjelman PlayerSettingsGUI toiminnallisuus.
 *
 * @author Joonas Kallinen
 *
 */

public class PlayerSettingsGUI_Ctrl {
	private ResourceBundle resources = Language.getInstance().messages();

	@FXML
	ListView<String> listViewNames;
	@FXML
	private TextField fieldNewPlayer;
	@FXML
	public ComboBox<String> existingPlayerList;
	@FXML
	public ComboBox<String> removablePlayerList;
	@FXML
	Button bRemove;
	@FXML
	private TextArea eventLog;

	private static PlayerSettingsGUI_Ctrl instance;

	private List<String> templist;

	private PlayerSettingsGUI_Ctrl() {
	}

	/*
	 * Method to return instance of the class or create it before returning it
	 */
	static PlayerSettingsGUI_Ctrl getInstance() {
		if (instance == null) {
			instance = new PlayerSettingsGUI_Ctrl();
		}
		return instance;
	}

	public void initialize() {
		Name.getInstance();
		templist = new ArrayList<String>();
		templist.addAll(Settings_SavedValues.getInstance().getPlayerNames());
		updateListView();
		updateComboBoxes();
		fieldNewPlayer.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
            	Name.setName(s2);
        		if (!Name.isLengthAllowedSize()) {
        			fieldNewPlayer.setText(Name.getName());
        		} else {
        			Name.setName(fieldNewPlayer.getText());
        		}
            }
        });
	}

	/*
	 * Method to check if the class has already been instanced. Returns true if
	 * instance of the class already exists, false otherwise
	 */
	public static boolean isInstanced() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Method for Making a new player
	 */
	@FXML
	private void addNewPlayer(ActionEvent event) {
		boolean found = false;
		for (String name : Settings_SavedValues.getInstance().getPlayerNames()) {
			if (name.matches("(?i)" + fieldNewPlayer.getText())) {
				found = true;
			}
		}
		if (found) {
			eventLog.setText(resources.getString("pNotadded")+" ('" + fieldNewPlayer.getText() + "' "+resources.getString("pAlreadyexists")+") \n" + eventLog.getText());
		} else {
			if (fieldNewPlayer.getText().matches("^[a-zA-Z]+$") && Name.isLengthAllowedSize()) {
				Settings_SavedValues.getInstance().addPlayerName(fieldNewPlayer.getText());
				eventLog.setText(resources.getString("pAdded")+" '" + fieldNewPlayer.getText() + "'\n" + eventLog.getText());
				updateListView();
				updateComboBoxes();
			} else {
				eventLog.setText(resources.getString("pNotadded")+" ('" + fieldNewPlayer.getText() + "' "+resources.getString("pNac")+") \n"
						+ eventLog.getText());
			}
		}
		fieldNewPlayer.setText("");
	}

	private void updateComboBoxes() {
		// Existing players
		existingPlayerList.setItems(Settings_SavedValues.getInstance().getPlayerNamesUnsorted());
		existingPlayerList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> o, Object o1, Object o2) {
//				System.out.println("existingPlayerList:Old value: " + o1 + ", new value: " + o2);
			}
		});
		if (fieldNewPlayer.getText().equals("")) {
			existingPlayerList.setValue(Settings_SavedValues.getInstance().getPlayerNames().get(0));
		} else {
			existingPlayerList.setValue(fieldNewPlayer.getText());
		}
		// Removable players
		removablePlayerList.setItems(Settings_SavedValues.getInstance().getPlayerNamesMinusDefault());
		removablePlayerList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> o, Object o1, Object o2) {
				if (o2 != null && o2.toString().matches("^DEFAULT$")) {
					removablePlayerListSetValue();
				}
			}
		});
		removablePlayerListSetValue();
	}

	private void removablePlayerListSetValue() {
		if (Settings_SavedValues.getInstance().getPlayerNamesMinusDefault().size() < 1) {
			removablePlayerList.setDisable(true);
			bRemove.setDisable(true);
			bRemove.setStyle("-fx-background-color:  rgb(192,192,192);");
		} else {
			removablePlayerList.setDisable(false);
			bRemove.setDisable(false);
			bRemove.setStyle("-fx-background-color:  rgb(100,215,245);");
		}
		if (Settings_SavedValues.getInstance().getPlayerNamesMinusDefault().size() >= 1) {
			removablePlayerList.setValue(Settings_SavedValues.getInstance().getPlayerNamesMinusDefault().get(0));
		}
	}

	private void updateListView() {
		listViewNames.setItems(Settings_SavedValues.getInstance().getPlayerNamesUnsorted());
	}

	@FXML
	private void textFieldEnter(KeyEvent ke) {
		if (ke.getCode() == KeyCode.ENTER) {
			addNewPlayer(null);
		}
	}

	/*
	 * Method for changing the active player
	 */
	@FXML
	private void changeActivePlayer(ActionEvent event) {
		eventLog.setText(resources.getString("pChanged")+" '" + existingPlayerList.getValue() + "' "+resources.getString("pAsA")+". \n" + eventLog.getText());
		Settings_SavedValues.getInstance().setActivePlayer(new PelaajanNimi(existingPlayerList.getValue()));
	}

	/*
	 * Method for removing an existing player
	 */
	@FXML
	private void removeExistingPlayer(ActionEvent event) {
		eventLog.setText(resources.getString("pRemoved")+" '" + removablePlayerList.getValue() + "' "+resources.getString("pFplayers")+". \n" + eventLog.getText());
		Settings_SavedValues.getInstance().removePlayer(removablePlayerList.getValue());
		updateListView();
		updateComboBoxes();
	}

	/*
	 * Method saves (or cancels) the value changes made in the Settings
	 * permanently per a session so they work in the game as intended.
	 */
	@FXML
	private void apply(ActionEvent event) {
		Button b = (Button) event.getSource();
		Stage stage = (Stage) b.getScene().getWindow();
		templist.clear();
		destroyInstance();
		stage.close();
	}

	@FXML
	private void cancel(ActionEvent event) {
		Button b = (Button) event.getSource();
		Stage stage = (Stage) b.getScene().getWindow();
		Settings_SavedValues.getInstance().setPlayerNames(templist);
		destroyInstance();
		stage.close();
	}

	/*
	 * Method to reset class instance so it can be created again at later time.
	 */
	public void destroyInstance() {
		instance = null;
	}

}
