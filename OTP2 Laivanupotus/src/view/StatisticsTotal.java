package view;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * Luokkaa käytetään TableView näkymän populoimiseen.
 *
 * @author Joonas Kallinen
 *
 */

public class StatisticsTotal {
	private final SimpleStringProperty name;
	private final SimpleStringProperty statgames;
	private final SimpleStringProperty statwins;
	private final SimpleStringProperty statlosses;
	private final SimpleStringProperty statshots;
	private final SimpleStringProperty stathits;

	public StatisticsTotal(String name, String statgames, String statwins, String statlosses, String statshots,
			String stathits) {
		this.name = new SimpleStringProperty(name);
		this.statgames = new SimpleStringProperty(statgames);
		this.statwins = new SimpleStringProperty(statwins);
		this.statlosses = new SimpleStringProperty(statlosses);
		this.statshots = new SimpleStringProperty(statshots);
		this.stathits = new SimpleStringProperty(stathits);
	}

    public String getName() {
        return name.get();
    }

    public void setName(String fName) {
        name.set(fName);
    }

    public String getStatgames() {
        return statgames.get();
    }

    public void setStatgames(String fName) {
    	statgames.set(fName);
    }

    public String getStatwins() {
        return statwins.get();
    }

    public void setStatwins(String fName) {
    	statwins.set(fName);
    }

    public String getStatlosses() {
        return statlosses.get();
    }

    public void setStatlosses(String fName) {
    	statlosses.set(fName);
    }

    public String getStatshots() {
        return statshots.get();
    }

    public void setStatshots(String fName) {
    	statshots.set(fName);
    }

    public String getStathits() {
        return stathits.get();
    }

    public void setStathits(String fName) {
    	stathits.set(fName);
    }


}
