package view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.scene.paint.Color;

/**
 *
 * Luokka muuttujien muuntamista varten.
 *
 * @author Joonas Kallinen
 *
 */

public class Converter {

	public static int convertFloatToRGB(double d) {
		return (int) (d * 255);
	}

	public static float convertRGBtoFloat(int f) {
		return (float) f / 255;
	}

	public static List<String> colorListToStringList(List<Color> lc) {
		List<String> ls = new ArrayList<String>();
		if (lc != null) {
			for (int i = 0; i < lc.size(); i++) {
				if (lc.get(i) != null) {
					ls.add(String.valueOf(convertFloatToRGB(lc.get(i).getRed())));
					ls.add(String.valueOf(convertFloatToRGB(lc.get(i).getGreen())));
					ls.add(String.valueOf(convertFloatToRGB(lc.get(i).getBlue())));
				} else {
					for (int j = 0; j < 3; j++) {
						ls.add(String.valueOf(255));
					}
				}
			}
		} else {
			for (int i = 0; i < 18; i++) {
				System.out.println("DEFAULT COLOR WHITE");
				ls.add(String.valueOf(255));
			}
		}
		return ls;
	}

	public static List<Color> stringListToColorList(List<String> ls) {
		List<Color> lc = new ArrayList<Color>();
		Integer[][] ia = new Integer[6][3];
		if (ls != null) {
			Iterator<String> itr = ls.iterator();
			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 3; j++) {
					if (itr.hasNext()) {
						String s = itr.next();
						ia[i][j] = Integer.valueOf(s);
					}
				}
			}
			for (int i = 0; i < 6; i++) {
				lc.add(new Color(convertRGBtoFloat(ia[i][0]), convertRGBtoFloat(ia[i][1]), convertRGBtoFloat(ia[i][2]),1));
			}
		}
		return lc;
	}

}
