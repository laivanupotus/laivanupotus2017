package view;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import model.KoordinaattiPari;
import view.Converter;

/**
 *
 * Ohjelman peliruutujen arvoista ja ominaisuuksista vastaava luokka.
 *
 * @author Joonas Kallinen
 *
 */

public class GameButton extends Button {
	private Button source;
	private KoordinaattiPari koordinaatit;

	public GameButton(Button source) {
		this.source = source;
		koordinaatit = KoordinaattiPari.create(Integer.valueOf(source.getId().substring(4, 5)), Integer.valueOf(source.getId().substring(3, 4)));
	}

	public KoordinaattiPari getKoordinaatit() {
		return koordinaatit;
	}

	public void newStyle() {
		source.setStyle(null);
	}

	public char getPlayerType() {
		char c;
		if (source.getId().length() == 5) {
			c = 'p';
		} else {
			c = 'o';
		}
		return c;
	}

	public void changeColorShipBeingAdded() {
		source.setStyle("-fx-background-color: rgb("+0+","+255+","+0+");");
	}

	public void changeColorShipWasNotAdded() {
		source.setStyle("-fx-background-color: rgb("+0+","+255+","+0+");");
	}

	public void changeColorShipDefault() {
		source.setStyle("-fx-background-color: rgb("+100+","+215+","+245+");");
	}

	public void changeColorShipAddedPlayer() {
		Color pd = Settings_SavedValues.getInstance().getColorArray()[0];
		if (pd == null) {
			pd = new Color(Converter.convertRGBtoFloat(64),Converter.convertRGBtoFloat(64),Converter.convertRGBtoFloat(64),1);
		}
		source.setStyle("-fx-background-color: rgb("+Converter.convertFloatToRGB(pd.getRed())+","+Converter.convertFloatToRGB(pd.getGreen())+","+Converter.convertFloatToRGB(pd.getBlue())+");");
	}

	public void changeColorShipHitPlayer() {
		Color pd = Settings_SavedValues.getInstance().getColorArray()[1];
		if (pd == null) {
			pd = new Color(Converter.convertRGBtoFloat(192),Converter.convertRGBtoFloat(0),Converter.convertRGBtoFloat(0),1);
		}
		source.setStyle("-fx-background-color: rgb("+Converter.convertFloatToRGB(pd.getRed())+","+Converter.convertFloatToRGB(pd.getGreen())+","+Converter.convertFloatToRGB(pd.getBlue())+");");
	}

	public void changeColorShipMissPlayer() {
		Color pd = Settings_SavedValues.getInstance().getColorArray()[2];
		if (pd == null) {
			pd = new Color(Converter.convertRGBtoFloat(255),Converter.convertRGBtoFloat(255),Converter.convertRGBtoFloat(255),1);
		}
		source.setStyle("-fx-background-color: rgb("+Converter.convertFloatToRGB(pd.getRed())+","+Converter.convertFloatToRGB(pd.getGreen())+","+Converter.convertFloatToRGB(pd.getBlue())+");");
	}

	public void changeColorShipAddedOpponent() {
		Color vd = Settings_SavedValues.getInstance().getColorArray()[3];
		if (vd == null) {
			vd = new Color(Converter.convertRGBtoFloat(64),Converter.convertRGBtoFloat(64),Converter.convertRGBtoFloat(64),1);
		}
		source.setStyle("-fx-background-color: rgb("+Converter.convertFloatToRGB(vd.getRed())+","+Converter.convertFloatToRGB(vd.getGreen())+","+Converter.convertFloatToRGB(vd.getBlue())+");");
	}

	public void changeColorShipHitOpponent() {
		Color vd = Settings_SavedValues.getInstance().getColorArray()[4];
		if (vd == null) {
			vd = new Color(Converter.convertRGBtoFloat(0),Converter.convertRGBtoFloat(192),Converter.convertRGBtoFloat(0),1);
		}
		source.setStyle("-fx-background-color: rgb("+Converter.convertFloatToRGB(vd.getRed())+","+Converter.convertFloatToRGB(vd.getGreen())+","+Converter.convertFloatToRGB(vd.getBlue())+");");
	}

	public void changeColorShipMissOpponent() {
		Color vd = Settings_SavedValues.getInstance().getColorArray()[5];
		if (vd == null) {
			vd = new Color(Converter.convertRGBtoFloat(255),Converter.convertRGBtoFloat(255),Converter.convertRGBtoFloat(255),1);
		}
		source.setStyle("-fx-background-color: rgb("+Converter.convertFloatToRGB(vd.getRed())+","+Converter.convertFloatToRGB(vd.getGreen())+","+Converter.convertFloatToRGB(vd.getBlue())+");");
	}

	public void reverseDisabledState() {
		if(source.isDisabled()) {
			source.setDisable(false);
		} else {
			source.setDisable(true);
		}
	}

	public void resetColor() {
		changeColorShipDefault();
	}

	public void enableDisabledState() {
		source.setDisable(false);
	}

	public void disableDisabledState() {
		source.setDisable(true);
	}

}
