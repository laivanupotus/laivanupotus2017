package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server extends Thread implements Server_IF {
	private ServerSocket server;
	private int port;
	private boolean isGame = true;

	private Socket socket_1;
	private BufferedReader input_1;
	private BufferedWriter output_1;

	private Socket socket_2;
	private BufferedReader input_2;
	private BufferedWriter output_2;

	public Server(int port) {
		this.port = port;
	}

	public boolean createGame(int port) {
		try {
			System.out.println(port);
			server = new ServerSocket(port);
			System.out.println(server.getInetAddress());
			// Odotetaan molempien yhdistamista
			socket_1 = server.accept();
			socket_2 = server.accept();

			if (socket_1 != null && socket_2 != null) {

				input_1 = new BufferedReader(new InputStreamReader(socket_1.getInputStream()));
				output_1 = new BufferedWriter(new OutputStreamWriter(socket_1.getOutputStream()));

				input_2 = new BufferedReader(new InputStreamReader(socket_2.getInputStream()));
				output_2 = new BufferedWriter(new OutputStreamWriter(socket_2.getOutputStream()));

				System.out.println("Game created");
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean startPhase() {

		try {
			@SuppressWarnings("unused")
			String player1Ready;
			while (!(player1Ready = input_1.readLine()).startsWith("READY")) {
				Thread.sleep(100);
			}
			@SuppressWarnings("unused")
			String player2Ready;
			while (!(player2Ready = input_2.readLine()).startsWith("READY")) {
				Thread.sleep(100);
			}
			return true;

		} catch (NullPointerException se) {
			System.out.println("Game suddenly closed.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void run() {
		try {
			// Creates game and wait players to insert ships
			if (createGame(port) && startPhase()) {
				output_1.write("GAME\n");
				output_2.write("GAME\n");
				output_1.flush();
				output_2.flush();
				int turn = 0;
				while (isGame) {

					if (turn % 2 == 0) {
						turn++;
						output_1.write("TURN\n");
						output_1.flush();

						String player1Move = input_1.readLine();
						output_2.write(player1Move + "\n");
						output_2.flush();

						String osuiko = input_2.readLine();
						output_1.write(osuiko + "\n");
						output_1.flush();
					} else {
						turn++;
						output_2.write("TURN\n");
						output_2.flush();

						String player2Move = input_2.readLine();
						output_1.write(player2Move + "\n");
						output_1.flush();

						String osuiko = input_1.readLine();
						output_2.write(osuiko + "\n");
						output_2.flush();
					}
				}
			}
		} catch (SocketException se) {
			System.out.println("Game round ended.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (socket_1 != null)
					socket_1.close();
				if (socket_2 != null)
					socket_2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeServer() {
		try {
			if (socket_1 != null)
				socket_1.close();
			if (socket_2 != null)
				socket_2.close();
			if (server != null)
				server.close();
			setGame(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setGame(boolean isGame) {
		this.isGame = isGame;
	}
}