package model;


/**
* Laivanupotuspeli_IF-interface, pohja laivanupotuspelin luonnile.
*
* @author Antti Pasanen
*/
public interface LaivanupotusPeli_IF {


	/**
	 * Uuden pelaajan lisays. Default ruudukko.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @return True, jos lisays onnistui.
	 */
	boolean uusiPelaaja(PelaajanNimi nimi);

	/**
	 * Pelaajan lisays. Custom ruudukko.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param ruudunKoko
	 *            Ruudukon koko KoordinaattiPari-oliona.
	 * @return True, jos lisays onnistui.
	 */
	boolean uusiPelaaja(PelaajanNimi nimi, KoordinaattiPari ruudukonKoko);

	/**
	 * Poistaa pelaajan.
	 *
	 * @param nimi
	 *            Poistettavan pelaajan nimi.
	 * @return True, jos poisto onnistui.
	 */
	boolean poistaPelaaja(PelaajanNimi nimi);

	/**
	 * Poistaa kaikki pelaajat.
	 */
	void poistaKaikkiPelaajat();

	/**
	 * Laivan lisays.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param koordinaatit
	 *            KoordinaattiPari taulukko
	 * @return True, jos lisays onnistui.
	 */
	boolean lisaaLaiva(PelaajanNimi nimi, KoordinaattiPari[] koordinaatit);

	/**
	 * Hakee pelaajan ruudukon char[y][x] taulukkona.
	 * Testausta varten.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param fog_of_war
	 *            onko fog of war paalla.
	 * @return char[][] taulukko, tai null jos pelaajaa ei ollut.
	 */
	char[][] getPelaajanKartta(PelaajanNimi nimi, boolean fog_of_war);

	/**
	 * Kertoon onko annetun pelaajan laivasto uponnut.
	 *
	 * @param nimi
	 *            Pelaajan nimi
	 *
	 * @return True, jos pelaajan laivasto on uponnut
	 */
	boolean pelaajanLaivatUponnut(PelaajanNimi nimi);

	/**
	 * Ampuu annetun pelaajan ruudukkoon annettuihin koordinaatteihin.
	 *
	 * @param nimi
	 *            Pelaajan nimi
	 * @param koordinaatit
	 *            koordinaatit
	 * @return True, jos ampuminen onnistui
	 */
	boolean ammu(PelaajanNimi nimi, KoordinaattiPari koordinaatit);

	/**
	 * Kertoo onko annetulla pelaajalla annetuissa koordinaateissa laiva.
	 *
	 * @param nimi
	 *            Pelaajan nimi
	 * @param koordinaatit
	 *            koordinaatit
	 *
	 * @return True, jos laiva loytyi ruudusta
	 */
	boolean onkoRuudussaLaiva(PelaajanNimi nimi, KoordinaattiPari koordinaatit);

	/**
	 * Palauttaa annetun pelaajan laivojen yhteiskoon.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi
	 *
	 * @return laivojen yhteiskoko
	 */
	int getPelaajanLaivojenYhteiskoko(PelaajanNimi s);


	/**
	 * Palauttaa taulukon jossa on annetun pelaajan laivojen kayttamien ruutujen
	 * koordinaatit.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi
	 * @return KoordinaattiPari-taulukko
	 */
	KoordinaattiPari[] getPelaajanLaivojenKoordinaatit(PelaajanNimi pelaajanNimi);

	/**
	 * Kertoo onko pelissa pelaajaa annetulla nimella.
	 *
	 * @param nimi
	 *            Haettava nimi
	 * @return True, jos pelaaja on luotu.
	 */
	boolean onPelaaja(PelaajanNimi nimi);

	/**
	 * Lisaa pelaajalle mediatorin valvomaan ruudukon muutoksia.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param mediator
	 *            Mediaattori.
	 *
	 * @return True, jos lisays onnistui.
	 */
	boolean lisaaLaivapeliMediator(PelaajanNimi pelaajanNimi, LaivapeliMediator_IF mediator);

	/**
	 * Palauttaa annetun pelaajan ruudukon koon.
	 *
	 * @param pelaajanNimi Pelaajan nimi
	 * @return KoordinaattiPari ruudukon koko
	 */
	KoordinaattiPari getPelaajanRuudukonKoko(PelaajanNimi pelaajanNimi);

	/**
	 * Kertoo onko pelaajan ruutuun ammuttu.
	 *
	 * @param nimi Pelaajan nimi
	 * @param koordinaatit	Ruudun koordinaatit
	 * @return True jos ammuttu, false muulloin
	 */
	boolean onkoRuutuunAmmuttu(PelaajanNimi nimi, KoordinaattiPari koordinaatit);
}
