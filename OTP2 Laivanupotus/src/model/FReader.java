package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * Tiedostojen lukemisen luokka.
 *
 * @author Joonas Kallinen
 *
 */

public class FReader {

	private static FReader instance;

	public static FReader getInstance() {
		if (instance == null) {
			instance = new FReader();
		}
		return instance;
	}

	public List<String> readFileIgnoreComments(String file) {
		Scanner s;
		List<String> list = new ArrayList<String>();
		try {
			s = new Scanner(new File(file));
			while (s.hasNext()) {
				String line = s.nextLine();
				if (!line.startsWith("#"))
					list.add(line);
			}
			s.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found. Return null.");
			return null;
		}
		return list;
	}

	public List<String> readFile(String file) {
		Scanner s;
		List<String> list = new ArrayList<String>();
		try {
			s = new Scanner(new File(file));
			while (s.hasNext()) {
				list.add(s.nextLine());
			}
			s.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found. Return null.");
			return null;
		}
		return list;
	}

}
