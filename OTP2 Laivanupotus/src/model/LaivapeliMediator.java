package model;

/**
 * Mediaattori, reagoi muutoksiin pelitilanteissa.
 * Asennetaan yksittaiselle pelaajalle, kahden pelaajan tapauksessa
 * molemmilla on omat mediaattorit.
 *
 *
 * @author Antti Pasanen
 */


import tilastot.PelinPelaaja;
import tilastot.Tilastot_IF;

public class LaivapeliMediator implements LaivapeliMediator_IF {

	private PelinPelaaja pelinPelaaja;
	private Tilastot_IF tilastot;

	/**
	 * Konstruktori.
	 *
	 * @param tilastot Tilastot luokka tilastojen operointia varten.
	 */
	public LaivapeliMediator(Tilastot_IF tilastot) {
		this.tilastot = tilastot;
	}


	/**
	 * Reagoi muutoksiin pelaajan peliruudussa.
	 */
	@Override
	public void ruutuMuuttu(Ruutu ruutu) {
		if (pelinPelaaja != null) {
			// kasvattaa laukauksia tilastoissa
			PelinPelaaja vastaPelaaja = tilastot.getKaksinpelit().getKaksinpeliByID(pelinPelaaja.getId())
										.getToinenPelinPelaaja(pelinPelaaja.getPelaajanNimi());
			vastaPelaaja.setLaukaukset(vastaPelaaja.getLaukaukset() + 1);

			if (ruutu.onLaiva()) {
				 // kasvattaa osumia tilastoissa
				vastaPelaaja.setOsumat(vastaPelaaja.getOsumat() + 1);
				// tarkastaa upposiko koko laivasto, jos niin asettaa pelaajan haviajaksi
				if (ruutu.getLaiva().getLaivasto().uponnut())
					tilastot.getKaksinpelit().getKaksinpeliByID(pelinPelaaja.getId())
										.setHaviaja(pelinPelaaja.getPelaajanNimi());
			}
		}
	}

	/**
	 * @return the pelinPelaaja
	 */
	public PelinPelaaja getPelinPelaaja() {
		return pelinPelaaja;
	}

	/**
	 * @param pelinPelaaja the pelinPelaaja to set
	 */
	public void setPelinPelaaja(PelinPelaaja pelinPelaaja) {
		this.pelinPelaaja = pelinPelaaja;
	}
}
