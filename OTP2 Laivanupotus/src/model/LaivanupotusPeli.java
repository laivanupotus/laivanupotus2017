package model;

import java.util.HashMap;

/**
 * Laivanupotuspeli-luokka. Toteuttaa laivanupotuspelin ilman kayttoliittymaa.
 *
 * @author Antti Pasanen
 */

public class LaivanupotusPeli implements LaivanupotusPeli_IF {

	HashMap<PelaajanNimi, Pelaaja> pelaajat;

	/**
	 * Konstruktori.
	 *
	 */
	public LaivanupotusPeli() {
		pelaajat = new HashMap<PelaajanNimi, Pelaaja>();
	}

	/**
	 * Uuden pelaajan lisays. Default ruudukko.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @return True, jos lisays onnistui.
	 */
	@Override
	public boolean uusiPelaaja(PelaajanNimi nimi) {
		if (pelaajat.containsKey(nimi))
			return false;

		pelaajat.put(nimi, new Pelaaja());

		return true;
	}

	/**
	 * Pelaajan lisays. Custom ruudukko.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param ruudunKoko
	 *            Ruudukon koko KoordinaattiPari-oliona.
	 * @return True, jos lisays onnistui.
	 */
	@Override
	public boolean uusiPelaaja(PelaajanNimi nimi, KoordinaattiPari ruudukonKoko) {
		if (pelaajat.containsKey(nimi) || ruudukonKoko == null)
			return false;

		pelaajat.put(nimi, new Pelaaja());
		pelaajat.get(nimi).setKarttaKoko(ruudukonKoko);

		return true;
	}

	/**
	 * Poistaa pelaajan.
	 *
	 * @param nimi
	 *            Poistettavan pelaajan nimi.
	 * @return True, jos poisto onnistui.
	 */
	@Override
	public boolean poistaPelaaja(PelaajanNimi nimi) {
		if (pelaajat.containsKey(nimi)) {
			pelaajat.remove(nimi);
			return true;
		} else
			return false;
	}

	/**
	 * Kertoo onko pelissa pelaajaa annetulla nimella.
	 *
	 * @param nimi
	 *            Haettava nimi
	 * @return True, jos pelaaja on luotu.
	 */
	@Override
	public boolean onPelaaja(PelaajanNimi nimi) {
		return pelaajat.containsKey(nimi);
	}

	/**
	 * Poistaa kaikki pelaajat.
	 */
	@Override
	public void poistaKaikkiPelaajat() {
		pelaajat.clear();
	}

	/**
	 * Laivan lisays.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param koordinaatit
	 *            KoordinaattiPari taulukko
	 * @return True, jos lisays onnistui.
	 */
	@Override
	public boolean lisaaLaiva(PelaajanNimi nimi, KoordinaattiPari[] koordinaatit) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).lisaaLaiva(koordinaatit);
		else
			return false;
	}

	/**
	 * Ampuu annetun pelaajan ruudukkoon annettuihin koordinaatteihin.
	 *
	 * @param nimi
	 *            Pelaajan nimi
	 * @param koordinaatit
	 *            koordinaatit
	 * @return True, jos ampuminen onnistui
	 */
	@Override
	public boolean ammu(PelaajanNimi nimi, KoordinaattiPari koordinaatit) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).getRuudukko().ammu(koordinaatit);

		return false;
	}

	/**
	 * Hakee pelaajan ruudukon char[y][x] taulukkona.
 	 * Testausta varten.
 	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param fog_of_war
	 *            onko fog of war paalla.
	 * @return char[][] taulukko, tai null jos pelaajaa ei ollut.
	 */
	@Override
	public char[][] getPelaajanKartta(PelaajanNimi nimi, boolean fog_of_war) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).getRuudukko().getKartta(fog_of_war);
		else
			return null;
	}

	/**
	 * Palauttaa annetun pelaajan ruudukon koon.
	 *
	 * @param pelaajanNimi Pelaajan nimi
	 * @return KoordinaattiPari ruudukon koko
	 */
	@Override
	public KoordinaattiPari getPelaajanRuudukonKoko(PelaajanNimi pelaajanNimi) {
		if (pelaajat.containsKey(pelaajanNimi))
			return pelaajat.get(pelaajanNimi).getRuudukko().getRuudukonKoko();
		else
			return null;
	}


	/**
	 * Kertoon onko annetun pelaajan laivasto uponnut.
	 *
	 * @param nimi
	 *            Pelaajan nimi
	 *
	 * @return True, jos pelaajan laivasto on uponnut
	 */
	@Override
	public boolean pelaajanLaivatUponnut(PelaajanNimi nimi) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).getLaivasto().uponnut();
		else
			return false;
	}

	/**
	 * Kertoo onko annetulla pelaajalla annetuissa koordinaateissa laiva.
	 *
	 * @param nimi
	 *            Pelaajan nimi
	 * @param koordinaatit
	 *            koordinaatit
	 *
	 * @return True, jos laiva loytyi ruudusta
	 */
	@Override
	public boolean onkoRuudussaLaiva(PelaajanNimi nimi, KoordinaattiPari koordinaatit) {
		if (pelaajat.containsKey(nimi))
			try {
				return pelaajat.get(nimi).getRuudukko().getRuutu(koordinaatit).onLaiva();
			} catch (NullPointerException npe) {
				return false;
			}
		else
			return false;
	}

	/**
	 * Kertoo onko pelaajan ruutuun ammuttu.
	 *
	 * @param nimi Pelaajan nimi
	 * @param koordinaatit	Ruudun koordinaatit
	 * @return True jos ammuttu, false muulloin
	 */
	@Override
	public boolean onkoRuutuunAmmuttu(PelaajanNimi nimi, KoordinaattiPari koordinaatit) {
		if (pelaajat.containsKey(nimi))
			try {
				return pelaajat.get(nimi).getRuudukko().getRuutu(koordinaatit).ammuttu();
			} catch(NullPointerException e) {
				return false;
			}
		else
			return false;
	}

	/**
	 * Palauttaa taulukon jossa on annetun pelaajan laivojen kayttamien ruutujen
	 * koordinaatit.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi
	 * @return KoordinaattiPari-taulukko
	 */
	@Override
	public KoordinaattiPari[] getPelaajanLaivojenKoordinaatit(PelaajanNimi pelaajanNimi) {
		if (!pelaajat.containsKey(pelaajanNimi))
			return null;

		return pelaajat.get(pelaajanNimi).getLaivasto().getLaivojenKoordinaatit();
	}

	/**
	 * Palauttaa annetun pelaajan laivojen yhteiskoon.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi
	 *
	 * @return laivojen yhteiskoko
	 */
	@Override
	public int getPelaajanLaivojenYhteiskoko(PelaajanNimi pelaajanNimi) {
		if (!pelaajat.containsKey(pelaajanNimi))
			return -1;

		return pelaajat.get(pelaajanNimi).getLaivasto().getLaivojenYhteiskoko();
	}

	/**
	 * Lisaa pelaajalle mediatorin valvomaan ruudukon muutoksia.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param mediator
	 *            Mediaattori.
	 *
	 * @return True, jos lisays onnistui.
	 */
	@Override
	public boolean lisaaLaivapeliMediator(PelaajanNimi pelaajanNimi, LaivapeliMediator_IF mediator) {
		if (!pelaajat.containsKey(pelaajanNimi))
			return false;

		pelaajat.get(pelaajanNimi).getRuudukko().setLaivapeliMediator(mediator);
		pelaajat.get(pelaajanNimi).getLaivasto().setLaivapeliMediator(mediator);

		return true;
	}
}
