package model;

import java.util.Random;

/**
 *
 * LaivanupotusAI, tekoaly laivanupotuspeliin. Kokoelma komentoja jotka suorittavat
 * samoja tapahtumia kuin itse pelaaja: Laivan sijoittaminen ruudukoilla ja ampuminen.
 *
 *
 * @author Antti Pasanen
 *
 */


public class LaivanupotusAI implements LaivanupotusAI_IF {

	private final static int DEFAULT_VAIKEUSTASO = 2;
	private final static int DEFAULT_HUIJAUSPROSENTTI = 40;
	private LaivanupotusPeli_IF laivanupotusPeli;
	private KoordinaattiPari laukaus = KoordinaattiPari.create(0, 0);
	private KoordinaattiPari[] laivanKoordinaatit;
	private int vaikeusTaso;
	private int huijausProsentti;
	private Random random;


	/**
	 * Konstruktori.
	 *
	 * @param lup
	 *            LaivanupotusPeli mita AI pelaa.
	 */
	public LaivanupotusAI(LaivanupotusPeli_IF lup) {
		this.laivanupotusPeli = lup;
		vaikeusTaso = DEFAULT_VAIKEUSTASO;
		huijausProsentti = DEFAULT_HUIJAUSPROSENTTI;
		random = new Random();
	}

	/**
	 * Suorittaa ampumisen annetun pelaajan ruudukolle.
	 *
	 * @param pelaajanNimi
	 *            Kohdepelaajan nimi.
	 * @return True jos ampuminen onnistui.
	 */
	@Override
	public boolean ammu(PelaajanNimi pelaajanNimi) {
		switch(vaikeusTaso) {
			case 1:
				return normiAmmu(pelaajanNimi);
			case 2:
				return smartAmmu(pelaajanNimi);
			case 3:
				return superAmmu(pelaajanNimi);
			case 4:
				return huijausAmmu(pelaajanNimi);
			default:
				return normiAmmu(pelaajanNimi);
		}
	}

	/**
	 *	Suoritetaan taysin satunnainen ampuminen annetun pelaajan ruudukkoon.
	 *
	 * @param pelaajanNimi annetun pelaajan nimi
	 * @return True, jos ampuminen onnistui
	 */
	@Override
	public boolean normiAmmu(PelaajanNimi pelaajanNimi) {
		if (!laivanupotusPeli.onPelaaja(pelaajanNimi))
			return false;

		// kaikkiin ruutuihin ammuttu ??
		// TODO

		int ruudunKokoX = laivanupotusPeli.getPelaajanRuudukonKoko(pelaajanNimi).getX();
		int ruudunKokoY = laivanupotusPeli.getPelaajanRuudukonKoko(pelaajanNimi).getY();
		//KoordinaattiPari laukaus;

		// random-laukaus
		do {
			laukaus = KoordinaattiPari.create(random.nextInt(ruudunKokoX), random.nextInt(ruudunKokoY));
		} while (!laivanupotusPeli.ammu(pelaajanNimi, laukaus));

		return true;
	}

	/**
	 * Jos ruudulla on laiva johon on osuttu, suoritetaan ampuminen sen ymparillä oleviin
	 * ruutuihin 4-ilmansuunnan mukaan. Jos laivaa ei ole ruudulla nakyvissa, tai kaikkiin
	 * niiden ymparillä oleviin ruutuihin on jo ammuttu, siirrytaan normiAmmu metodiin
	 *
	 *  @param pelaajanNimi Ammuttavan pelaajan nimi
	 *  @return True jos ampuminen onnistui.
	 *
	 */
	@Override
	public boolean smartAmmu(PelaajanNimi pelaajanNimi) {
		if (!laivanupotusPeli.onPelaaja(pelaajanNimi))
			return false;

		for (int i = 0; i < laivanupotusPeli.getPelaajanRuudukonKoko(pelaajanNimi).getY(); i++) {
			for (int j = 0; j < laivanupotusPeli.getPelaajanRuudukonKoko(pelaajanNimi).getX(); j++) {
				KoordinaattiPari koordinaatit = KoordinaattiPari.create(j, i);

				if (laivanupotusPeli.onkoRuudussaLaiva(pelaajanNimi, koordinaatit)
						&& laivanupotusPeli.onkoRuutuunAmmuttu(pelaajanNimi, koordinaatit)) {

					for (int k = 0; k < 4; k++) {
						laukaus = KoordinaattiPari.create(
								((k & 1) == 0) ? ((k == 0) ? j + 1 : j - 1) : j,
								((k & 1) == 0) ? i : ((k == 1) ? i + 1 : i - 1));
						if (laivanupotusPeli.ammu(pelaajanNimi, laukaus))
							return true;
					}
				}
			}
		}
		return normiAmmu(pelaajanNimi);
	}

	/**
	 * Ampuminen jossa jokaisella laukauksella on mahdollisuus osua ammuttavan pelaajan laivaan.
	 * Jos osutaan laivaan johon on jo ammuttu, siirrytaan smartAmmu metodiin.
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui
	 *
	 */
	@Override
	public boolean superAmmu(PelaajanNimi pelaajanNimi) {
		KoordinaattiPari[] pelaajanKoordinaatit = laivanupotusPeli.getPelaajanLaivojenKoordinaatit(pelaajanNimi);

		if (pelaajanKoordinaatit.length > 0) {
			int target = random.nextInt(pelaajanKoordinaatit.length);

			if (laivanupotusPeli.ammu(pelaajanNimi, pelaajanKoordinaatit[target])) {
				laukaus = pelaajanKoordinaatit[target];

				return true;
			}
		}
		return smartAmmu(pelaajanNimi);
	}


	/**
	 * Suorittaa huijausampumisen. Ampumisella on huijausprosentin maaraama mahdollisuus
	 * osua annetun pelaajan laivaan. Jos laukaus ei osu laivaan, siirrytaan smartAmmu metodiin.
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui
	 */
	@Override
	public boolean huijausAmmu(PelaajanNimi pelaajanNimi) {
		int huijaus = random.nextInt(100) + 1;

		if (huijaus <= huijausProsentti) {
			KoordinaattiPari[] pelaajanKoordinaatit = laivanupotusPeli.getPelaajanLaivojenKoordinaatit(pelaajanNimi);

			int r = random.nextInt(pelaajanKoordinaatit.length);

			for (int i = 0; i < pelaajanKoordinaatit.length; i++) {
				if (r == pelaajanKoordinaatit.length)
					r = 0;

				if (laivanupotusPeli.ammu(pelaajanNimi, pelaajanKoordinaatit[r])) {
					laukaus = pelaajanKoordinaatit[r];

					return true;
				}
				r++;
			}
		}
		return smartAmmu(pelaajanNimi);
	}


	/**
	 * Yrittaa lisata laivan maaritellylle pelaajalle satunnaiseen paikkaan kartalla.
	 *
	 * @param pelaajanNimi Pelaajan nimi jolle laiva lisataan
	 * @param laivanPituus Lisattavan laivan pituus
	 *
	 * @return True, jos lisays onnistui
	 */
	@Override
	public boolean asetaLaiva(PelaajanNimi pelaajanNimi, int laivanPituus) {
		if (!laivanupotusPeli.onPelaaja(pelaajanNimi))
			return false;

		int suunnat = 4;
		int suunta = random.nextInt(suunnat);

		laivanKoordinaatit = new KoordinaattiPari[laivanPituus];
		laivanKoordinaatit[0] = KoordinaattiPari.create(
				random.nextInt(laivanupotusPeli.getPelaajanRuudukonKoko(pelaajanNimi).getX()),
				random.nextInt(laivanupotusPeli.getPelaajanRuudukonKoko(pelaajanNimi).getY()));

		if (!laivanupotusPeli.onkoRuudussaLaiva(pelaajanNimi, laivanKoordinaatit[0])
				&& !laivanupotusPeli.onkoRuutuunAmmuttu(pelaajanNimi, laivanKoordinaatit[0])) {
			for (int i = 0; i < suunnat; i++) {
				if (suunta == suunnat)
					suunta = 0;
				for (int j = 1; j < laivanPituus; j++) {
					tarkastaRuudut(pelaajanNimi, j, // suunta & 1 == 0 = even number
							((suunta & 1) == 0) ? 0 : (suunta == 1 ? j : -j),
							((suunta & 1) == 0) ? (suunta == 0 ? j : -j) : 0);
					if (laivanupotusPeli.lisaaLaiva(pelaajanNimi, laivanKoordinaatit))
						return true;
				}
				suunta++;
			}
		}
		return false;
	}

	/**
	 * Tarkastaa onko annetussa ruudussa merta vai ei, ja muokkaa laivanKoordinaatit taulukkoa
	 * sen perusteella.
	 * null arvoksi jos tarkasteltavaan kohtaan ei voi lisätä laivaa.
	 *
	 * @param nimi PelaajanNimi
	 * @param offset tarkastettavan taulukon kohdan muutos
	 * @param x x-koordinaatin muutos
	 * @param y y-koordinaatin muutos
	 */
	private void tarkastaRuudut(PelaajanNimi nimi, int offset, int x, int y) {
		try {
			KoordinaattiPari koordinaatit = KoordinaattiPari.create(laivanKoordinaatit[0].getX() + x, laivanKoordinaatit[0].getY() + y);
			if (!laivanupotusPeli.onkoRuudussaLaiva(nimi, koordinaatit)
					&& !laivanupotusPeli.onkoRuutuunAmmuttu(nimi, koordinaatit)) {
				laivanKoordinaatit[offset] = koordinaatit;
			}
			else {
				laivanKoordinaatit[offset] = null;
			}
		} catch (Exception e) {
			laivanKoordinaatit[offset] = null;
		}
	}

	/**
	 * Viimeisimman ampumisen koordinaatit.
	 *
	 * @return KoordinaattiPari koordinaatit
	 */
	@Override
	public KoordinaattiPari getLaukaus() {
		return laukaus;
	}

	/**
	 * Viimeisimman asennetun laivan koordinaatit
	 *
	 * @return KoordinaattiPari[] KoordinaattiPari-taulukko
	 */
	@Override
	public KoordinaattiPari[] getViimeisinAsennettuLaiva() {
		return laivanKoordinaatit;
	}


	/**
	 * Palauttaa vaikeustason.
	 *
	 * @return Vaikeustaso
	 */
	@Override
	public int getVaikeusTaso() {
		return vaikeusTaso;
	}

	/**
	 * Asettaa vaikeustason (1, 2, 3, 4, ...)
	 *
	 * @param vaikeusTaso Vaikeustaso
	 */
	@Override
	public void setVaikeusTaso(int vaikeusTaso) {
		this.vaikeusTaso = vaikeusTaso;
	}

	/**
	 * @return Palauttaa huijausprosentin.
	 */
	@Override
	public int getHuijausProsentti() {
		return huijausProsentti;
	}

	/**
	 * Asettaa huijausprosentin.
	 *
	 * @param prosentti Huijausprosentti
	 */
	@Override
	public void setHuijausProsentti(int prosentti) {
		huijausProsentti = prosentti;
	}
}
