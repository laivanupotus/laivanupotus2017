package model;

import java.util.ArrayList;

/**
 *
 * Laiva luokka, pelaajan yksi laiva joka sijaitsee n-maaralla ruutuja.
 *
 * @author Antti Pasanen
 *
 */

public class Laiva {

	private boolean uponnut;
	private ArrayList<Ruutu> ruudut;
	private Laivasto laivasto;

	/**
	 * Konstruktori.
	 *
	 * @param ruudut Ruudut joissa laiva sijaitsee.
	 * @param laivasto Laivasto, jonka mukana laiva seilailee.
	 */
	public Laiva(ArrayList<Ruutu> ruudut, Laivasto laivasto) {
		this.ruudut = ruudut;
		this.ruudut.forEach(ruutu->ruutu.setLaiva(this));
		this.uponnut = false;
		this.setLaivasto(laivasto);
	}

	/**
	 * Upottaa laivan jos kaikkiin ruutuihin joissa laiva on, on ammuttu.
	 *
	 *
	 */
	public void upota() {
		uponnut = true;
		ruudut.forEach(ruutu-> {
			if (!ruutu.ammuttu())
				uponnut = false;
		});
	}

	/**
	 * Kertoo onko laiva uponnut.
	 *
	 * @return True, jos laiva on uponnut.
	 */
	public boolean getUponnut() {
		return uponnut;
	}

	/**
	 * Asettaa laivan uponnut-statuksen.
	 *
	 * @param uponnut True jos laiva on uponnut.
	 */
	public void setUponnut(boolean uponnut) {
		this.uponnut = uponnut;
	}

	/**
	 * Palauttaa ruudut joissa laiva on.
	 *
	 * @return Laivan ruudut
	 */
	public ArrayList<Ruutu> getRuudut() {
		return ruudut;
	}

	/**
	 * Asettaa laivan ruudut annettuihin.
	 *
	 * @param ruudut Ruudut
	 */
	public void setRuudut(ArrayList<Ruutu> ruudut) {
		this.ruudut = ruudut;
		this.ruudut.forEach(ruutu->ruutu.setLaiva(this));
	}

	/**
	 * @return the laivasto
	 */
	public Laivasto getLaivasto() {
		return laivasto;
	}

	/**
	 * @param laivasto the laivasto to set
	 */
	public void setLaivasto(Laivasto laivasto) {
		this.laivasto = laivasto;
	}
}
