package model;

import java.util.ArrayList;

/**
 *
 * Laivastoluokka, joka sisaltaa pelaajan laivat.
 *
 *
 * @author Antti Pasanen
 *
 */

public class Laivasto {

	private ArrayList<Laiva> laivat;
	private LaivapeliMediator_IF laivapeliMediator;

	/**
	 * Konstruktori
	 */
	public Laivasto() {
		laivat = new ArrayList<Laiva>();
		setLaivapeliMediator(null);
	}

	/**
	 * Lisaa laivan laivastoon
	 *
	 * @return True, jos laivan lisays onnistui
	 */
	public boolean lisaaLaiva(ArrayList<Ruutu> ruudut) {
		return laivat.add(new Laiva(ruudut, this));
	}

	/**
	 * Palauttaa true jos laivaston laivat ovat uponneet, false jos kaikki eivat ole,
	 * tai laivastossa ei ole laivoja.
	 *
	 *
	 * @return True, jos laivaston kaikki laivat ovat uponneet
	 */
	public boolean uponnut() {
		if (laivat.isEmpty())
			return false;

		for (Laiva laiva: laivat) {
			if (!laiva.getUponnut())
				return false;
		}

		return true;
	}

	/**
	 * Palauttaa KoordinaattiPari arrayn jossa on laivojen kayttamien ruutujen koordinaatit.
	 *
	 *
	 * @return Koordinaatit
	 */
	public KoordinaattiPari[] getLaivojenKoordinaatit() {

		KoordinaattiPari[] koordinaatit = new KoordinaattiPari[getLaivojenYhteiskoko()];

		int counter = 0;

		for (Laiva laiva: laivat) {
			for (int i = 0; i < laiva.getRuudut().size(); i++) {
				koordinaatit[counter] = laiva.getRuudut().get(i).getKoordinaatit();
				counter++;
			}
		}

		return koordinaatit;
	}

	/**
	 * Palauttaa laivastossa olevien laivojen kayttamien ruutujen maaran.
	 *
	 * @return Laivojen yhteiskoko
	 */
	public int getLaivojenYhteiskoko() {
		int yhteiskoko = 0;

		for (Laiva laiva: laivat) {
			yhteiskoko += laiva.getRuudut().size();
		}

		return yhteiskoko;
	}

	/**
	 * @return the laivapeliMediator
	 */
	public LaivapeliMediator_IF getLaivapeliMediator() {
		return laivapeliMediator;
	}

	/**
	 * @param laivapeliMediator the laivapeliMediator to set
	 */
	public void setLaivapeliMediator(LaivapeliMediator_IF laivapeliMediator) {
		this.laivapeliMediator = laivapeliMediator;
	}
}
