package model;

import java.util.Objects;

/**
 *
 * Luokka pelaajan nimen tallentamista ja hallinnoitia varten pelimallissa.
 *
 * @author antti Pasanen
 *
 */

public class PelaajanNimi {

	private String nimi;
	private static int maxPituus = 50;

	/**
	 * Konstruktori
	 *
	 * @param nimi Nimi.
	 */
	public PelaajanNimi(String nimi) {
		setNimi(nimi);
	}


	/**
	 * @return the nimi
	 */
	public String getNimi() {
		return nimi;
	}

	/**
	 * @param nimi the nimi to set
	 */
	public void setNimi(String nimi) {
		if (nimi != null) {
			if (nimi.length() > maxPituus)
				this.nimi = nimi.substring(0, maxPituus);
			else
				this.nimi = nimi;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!PelaajanNimi.class.isAssignableFrom(o.getClass()))
			return false;
		final PelaajanNimi toinen = (PelaajanNimi) o;

		if ((this.nimi == null) ? (toinen.getNimi() != null) : !this.nimi.equals(toinen.getNimi()))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(nimi);
	}

}
