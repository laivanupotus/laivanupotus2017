package model;


/**
 *
 * Ruudukko-luokka, kuvaa yhden pelaajan pelialuetta.
 *
 * @author Antti Pasanen
 *
 */
public class Ruudukko {

	public static final char SYMBOL_LAIVA = 'S';
	public static final char SYMBOL_MERI = '~';
	public static final char SYMBOL_SUMU = '#';
	public static final char SYMBOL_LAUKAUS_OSUMA = 'X';
	public static final char SYMBOL_LAUKAUS_HUTI = 'O';

	// Ruudukon default koot.
	private static int DEFAULT_X_KOKO = 10;
	private static int DEFAULT_Y_KOKO = 10;

	private Ruutu[][] ruudut;
	private KoordinaattiPari ruudukonKoko;

	private LaivapeliMediator_IF laivapeliMediator;

	/**
	 * Konstruktori.
	 *
	 * @param koko Ruudukon koko KoordinaattiPari oliona
	 */
	public Ruudukko(KoordinaattiPari koko) {
		if (koko == null || koko.getX() < 1 || koko.getY() < 1)
			this.ruudukonKoko = KoordinaattiPari.create(DEFAULT_X_KOKO, DEFAULT_Y_KOKO);
		else
			this.ruudukonKoko = koko;

		ruudut = new Ruutu[ruudukonKoko.getY()][ruudukonKoko.getX()];

		for (int i = 0; i < ruudukonKoko.getY(); i++) {
			for (int j = 0; j < ruudukonKoko.getX(); j++) {
				ruudut[i][j] = new Ruutu(KoordinaattiPari.create(j, i));
			}
		}

		laivapeliMediator = null;
	}

	/**
	 * Kertoo jos koordinaatit ovat ruudukon sisalla.
	 *
	 *
	 * @param koordinaatit Koordinaatit
	 * @return True, jos ruutu on ruudukossa
	 */
	public boolean onRuudukossa(KoordinaattiPari koordinaatit) {
		if (koordinaatit == null)
			return false;
		return (!(koordinaatit.getX() >= ruudukonKoko.getX() || koordinaatit.getY() >= ruudukonKoko.getY()));
	}

	/**
	 * Kertoo onko ruudukossa laiva vai ei.
	 *
	 * @param koordinaatit Koordinaatit.
	 * @return True, jos ruudussa ei ole laivaa
	 */
	public boolean onTyhja(KoordinaattiPari koordinaatit) {
		if (onRuudukossa(koordinaatit))
			if (!ruudut[koordinaatit.getY()][koordinaatit.getX()].onLaiva())
				return true;

		return false;
	}

	/**
	 * Palauttaa ruudukon koon KoordinaattiPari oliona.
	 *
	 * @return ruudukonKoko
	 */
	public KoordinaattiPari getRuudukonKoko() {
		return ruudukonKoko;
	}

	/**
	 *	Palauttaa ruudun annetuista koordinaateista.
	 *
	 *
	 * @param koordinaatit KoordinaattiPari koordinaatit.
	 * @return Koordinaateissa oleva ruutu, tai null
	 */
	public Ruutu getRuutu(KoordinaattiPari koordinaatit) {
		if (onRuudukossa(koordinaatit))
			return ruudut[koordinaatit.getY()][koordinaatit.getX()];
		else
			return null;
	}

	/**
	 * Yrittaa ampua ruutuun jos ruutu on ruudukon sisalla.
	 *
	 *
	 * @param koordinaatit KoordinaattiPari koordinaatit joihin ammutaan.
	 * @return True, jos ampuminen onnistui
	 */
	public boolean ammu(KoordinaattiPari koordinaatit) {
		if (onRuudukossa(koordinaatit)) {
			if (ruudut[koordinaatit.getY()][koordinaatit.getX()].ammu()) {
				if (laivapeliMediator != null) // viesti mediaattorille
					laivapeliMediator.ruutuMuuttu(ruudut[koordinaatit.getY()][koordinaatit.getX()]);
				return true;
			}
		}

		return false;
	}

	/**
	 * Luo char[][] taulukon ruudukosta. Jos fog_of_war on paalla, piilotetaan ruudut joihin
	 * ei ole ammuttu viela. Testausta varten jne.
	 *
	 * Symbolit:
	 * S = laiva
	 * # = sumu
	 * ~ = meri
	 * X = osuma
	 * O = huti
	 *
	 * @param fog_of_war Ovatko ruudut sumun alla.
	 * @return char[][] taulukko
	 */
	public char[][] getKartta(boolean fog_of_war) {
		char ruudukko[][] = new char[ruudukonKoko.getY()][ruudukonKoko.getX()];

		for (int i = 0; i < ruudukonKoko.getY(); i++) {
			for (int j = 0; j < ruudukonKoko.getX(); j++) {
				if (ruudut[i][j].ammuttu()) {
					if (ruudut[i][j].onLaiva())
						ruudukko[i][j] = SYMBOL_LAUKAUS_OSUMA;
					else
						ruudukko[i][j] = SYMBOL_LAUKAUS_HUTI;
				} else {
					if (fog_of_war)
						ruudukko[i][j] = SYMBOL_SUMU;
					else if (ruudut[i][j].onLaiva())
						ruudukko[i][j] = SYMBOL_LAIVA;
					else
						ruudukko[i][j] = SYMBOL_MERI;
				}
			}
		}
		return ruudukko;
	}

	/**
	 * @return the laivapeliMediator
	 */
	public LaivapeliMediator_IF getLaivapeliMediator() {
		return laivapeliMediator;
	}

	/**
	 * @param laivapeliMediator the laivapeliMediator to set
	 */
	public void setLaivapeliMediator(LaivapeliMediator_IF laivapeliMediator) {
		this.laivapeliMediator = laivapeliMediator;
	}
}
