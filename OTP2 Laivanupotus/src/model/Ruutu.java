package model;


/**
 *
 * Ruutu-olio, pienin osa pelialuetta.
 *
 *
 * @author Antti Pasanen
 *
 */
public class Ruutu {

	private KoordinaattiPari koordinaatit;
    private Laiva laiva;
    private boolean ammuttu;

    /**
     * Konstruktori.
     *
     * @param koordinaatti_X Ruudun X-koordinaatti.
     * @param koordinaatti_Y Ruudun Y-koordinaatti.
     */
    public Ruutu(KoordinaattiPari koordinaatit) {
    	this.setKoordinaatit(koordinaatit);
        ammuttu = false;
        laiva = null;
    }


    /**
     * Asettaa ruudussa olevan laivan
     *
     * @param laiva Laiva
     */
    public void setLaiva(Laiva laiva) {
        this.laiva = laiva;
    }

    /**
     * Ampuu ruutuun.
     *
     * @return True, jos ruutuun ei ole aikaisemmin ammuttu
     */
    public boolean ammu() {
    	if (ammuttu)
    		return false;
    	else {
    		ammuttu = true;
    		if (laiva != null)
    			laiva.upota();
    	}

    	return true;
    }

    /**
     * Kertoo onko ruutuun ammuttu
     *
     * @return True, jos ruutuun on ammuttu
     */
    public boolean ammuttu() {
        return ammuttu;
    }

    /**
     * Kertoo onko ruudussa laiva
     *
     * @return True, jos ruudussa on laiva
     */
    public boolean onLaiva() {
    	if (laiva == null)
    		return false;
    	else
    		return true;
    }


    /**
     * Palauttaa ruudukossa olevan laivan
     *
     * @return Laiva, tai null jos ruudussa ei ole laivaa.
     */
    public Laiva getLaiva() {
        return laiva;
    }


	/**
	 * @return the koordinaatit
	 */
	public KoordinaattiPari getKoordinaatit() {
		return koordinaatit;
	}


	/**
	 * @param koordinaatit the koordinaatit to set
	 */
	public void setKoordinaatit(KoordinaattiPari koordinaatit) {
		this.koordinaatit = koordinaatit;
	}
}
