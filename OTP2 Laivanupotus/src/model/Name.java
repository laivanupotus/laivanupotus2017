package model;

public class Name {
	private static String name;
	private static int maxNameSize;
	private static Name instance;

	private Name() {
		maxNameSize = 40;
	}

	public static Name getInstance() {
		if (instance == null) {
			instance = new Name();
			name = new String();
		}
		return instance;
	}

	public static boolean isLengthAllowedSize() {
//		System.out.println("\n:: isAllowed, name.length: " + name.length() + " <= " + maxNameSize);
		if (name.length() <= maxNameSize) {
//			System.out.println("TRUE: isAllowed, name.length: " + name.length() + " <= " + maxNameSize);
			return true;
		} else {
			name = name.substring(0, maxNameSize);
//			System.out.println("FALSE: isAllowed, name.length: " + name.length() + " > " + maxNameSize);
			return false;
		}
	}

	public static void setName(String newname) {
		name = newname;
	}

	public static String getName() {
		return name;
	}

}
