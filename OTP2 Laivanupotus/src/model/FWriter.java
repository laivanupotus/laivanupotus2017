package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javafx.collections.ObservableList;
import view.Settings_SavedValues;

/**
 *
 * Tiedostoon kirjoittamisen toteuttava luokka.
 *
 * @author Joonas Kallinen
 *
 */
public class FWriter {

	private static FWriter instance;

	public static FWriter getInstance() {
		if (instance == null) {
			instance = new FWriter();
		}
		return instance;
	}

	public boolean saveOnExit() {
		writeFileIgnoreComments(Settings_SavedValues.getInstance().getPlayerNamesMinusDefault(), "localplayers");
		writeFileIgnoreComments(Settings_SavedValues.getInstance().getColors(), "colors");
		writeFileIgnoreComments(Settings_SavedValues.getInstance().getNotes(), "notes");
		writeFileIgnoreComments(Settings_SavedValues.getInstance().getDifficulty(), "difficulty");
		return true;
	}

	private boolean writeFileIgnoreComments(ObservableList<String> data, String fileName) {
		if (data != null) {
			List<String> rList = FReader.getInstance().readFile(fileName);
			ObservableList<String> wList = data;
			Iterator<String> rItr;
			try {
				rItr = rList.iterator();
				System.out.println("rItr not null");
			} catch (NullPointerException npe) {
				rItr = wList.iterator();
				System.out.println("rItr = null");
			}
			Iterator<String> wItr = wList.iterator();
			System.out.println("\nFWriter, (A) rList: " + rList);
			System.out.println("FWriter, (B) wList: " + wList);
			PrintWriter out;
			try {
				out = new PrintWriter(fileName);
				while (rItr.hasNext()) {
					if (rItr.hasNext()) {
						String rval = rItr.next();
						if (rval.startsWith("#")) {
							out.println(rval);
						} else {
							if (wItr.hasNext())
								out.println(wItr.next());
						}
					}
				}
				while (wItr.hasNext()) {
					out.println(wItr.next());
				}
				out.close();
				return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
