package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import view.GameButton;
import view.GameGrid_Ctrl;
import view.MainGUI_Ctrl;
import view.Settings_SavedValues;

public class Client extends Thread implements Client_IF {
	private Socket socket;
	private int port;
	private String ip;
	private boolean isGame = true;

	private int playerMoveXKoord;
	private int playerMoveYKoord;

	private int voittocounter = 0;
	private boolean isLose = false;
	BufferedReader input;
    BufferedWriter output;
	ArrayList<GameButton> playerShips;
	GameButton gb;

	public Client(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public Client(int port) {
		this.port = port;
	}

	public ArrayList<GameButton> getPlayerShips() {
		return GameGrid_Ctrl.getPlayerShipButtons();
	}

	public void createConnection() {
		try {
			if(ip == null) {
				socket = new Socket(InetAddress.getLocalHost().getHostAddress(), port);
				System.out.println(socket.getInetAddress());
			}
			else {
				socket = new Socket(ip, port);
			}
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			System.out.println("Connected");
			MainGUI_Ctrl.getInstance().getGameGridWrapper().setDisable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void run() {
		try {
			if (socket == null) {
				createConnection();
				startPhase();
				@SuppressWarnings("unused")
				String ready;
				while(!(ready = input.readLine()).startsWith("GAME")) {
					Thread.sleep(100);
				}
				GameGrid_Ctrl.getInstance().setFlagPlay(true);
			}
			while(isGame) {
				String move = input.readLine();
				if(move.startsWith("TURN")) {
					GameGrid_Ctrl.getInstance().setFlagIsTurn(true);
					while(gb==null) {
						Thread.sleep(100);
					}
					playerMoveXKoord = gb.getKoordinaatit().getX();
					playerMoveYKoord = gb.getKoordinaatit().getY();

					output.write("AMMU,"+playerMoveXKoord+","+playerMoveYKoord+",\n");
					output.flush();

					boolean osuiko;
					boolean voitto;
					//Check if hit and if opponent fleet is sinked.
					String hit = input.readLine();
					String[] hitsplit = hit.split(",");
					osuiko = Boolean.valueOf(hitsplit[0]);
					voitto = Boolean.valueOf(hitsplit[1]);

					if(osuiko) {
						GameGrid_Ctrl.getInstance().playerHitOpponentShip(gb);
						//gb.changeColorShipHitOpponent();

					}
					else {
						GameGrid_Ctrl.getInstance().playerMissedOpponentShip(gb);
						//gb.changeColorShipMissOpponent();
					}
					if(voitto) {
							GameGrid_Ctrl.getInstance().playerSinkedOpponentFleet();
							GameGrid_Ctrl.getInstance().gameRoundIsOverMP();
							isGame = false;
							if(Settings_SavedValues.getInstance().getServer() != null) {
								Settings_SavedValues.getInstance().getServer().setGame(isGame);
							}
					}
					GameGrid_Ctrl.getInstance().setFlagIsTurn(false);
					gb = null;
				}
				else if(move.startsWith("AMMU")) {

					String[] parts = move.split(",");

					int xKoord = Integer.valueOf(parts[1]);
					int yKoord = Integer.valueOf(parts[2]);
					KoordinaattiPari kp = KoordinaattiPari.create(xKoord, yKoord);
					boolean isHit = osuiko(kp);
					if(isHit) {
						voittocounter++;
					}
					if(voittocounter==playerShips.size()) {
						isLose = true;
					}
					//boolean isLose = GameGrid_Ctrl.getInstance().hasFleetSinked();
					output.write(String.valueOf(isHit)+","+String.valueOf(isLose)+"\n");
					output.flush();

					GameGrid_Ctrl.getInstance().multiPlayerGridUpdate(isHit, kp);

					if(isLose) {
						GameGrid_Ctrl.getInstance().setStatusPlayerLost();
						GameGrid_Ctrl.getInstance().gameRoundIsOverMP();
						isGame = false;
						if(Settings_SavedValues.getInstance().getServer() != null) {
							Settings_SavedValues.getInstance().getServer().setGame(isGame);
						}
					}
				}
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//get X and Y coordinates and check if hit
	public boolean osuiko(KoordinaattiPari kp) {
		for(GameButton gb : getPlayerShips()) {
			if(gb.getKoordinaatit().getX() == kp.getX() && gb.getKoordinaatit().getY() == kp.getY()) {
				return true;
			}
		}
		return false;
	}


	public GameButton getGb() {
		return gb;
	}

	public void setGb(GameButton gb) {
		this.gb = gb;
	}

	public void startPhase() {
		while(GameGrid_Ctrl.getInstance().isFlagAddingShips()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		playerShips = getPlayerShips();
		try {
			output.write("READY\n");
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void closeSocket() {
		try {
			socket.close();
			isGame = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}