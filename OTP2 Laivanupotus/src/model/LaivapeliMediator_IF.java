package model;

/**
 * Mediaattoripohja laivanupotuspelille.
 *
 * @author Antti Pasanen
 *
 */
public interface LaivapeliMediator_IF {

	/**
	 * Reagoi muutoksiin pelaajan peliruudussa.
	 */
	void ruutuMuuttu(Ruutu ruutu);
}
