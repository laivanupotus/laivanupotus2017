package model;

import java.util.Objects;

/**
 * KoordinaattiPari luokka, sailyttaa positiivisen X ja Y koordinaatin arvot.
 * Luonti: KoordinaattiPari.Create(x, y)
 *
 * @author Antti Pasanen
 *
 */

public class KoordinaattiPari {

	private int X;
	private int Y;

	private KoordinaattiPari(int x, int y) {
		setX(x);
		setY(y);
	}

	/**
	 * KoordinaattiParin luontitehdas.
	 *
	 * @param x x
	 * @param y y
	 * @return null / koordinaattiPari
	 */
	public static KoordinaattiPari create(int x, int y) {
		if (validate(x) && validate(y))
			return new KoordinaattiPari(x, y);
		else
			return null;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return Y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		if (validate(y))
			Y = y;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return X;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		if (validate(x))
			X = x;
	}

	/**
	 * Validoi numeron (>= 0)
	 *
	 * @param number validoitava numero
	 * @return True, jos validi
	 */
	private static boolean validate(int number) {
		if (number < 0)
			return false;

		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!KoordinaattiPari.class.isAssignableFrom(o.getClass()))
			return false;
		final KoordinaattiPari toinen = (KoordinaattiPari) o;

		if (this.X != toinen.X || this.Y != toinen.Y)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(X, Y);
	}
}
