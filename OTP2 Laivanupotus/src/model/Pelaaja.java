package model;

import java.util.ArrayList;

/**
 *
 * Yksi laivanupotuspelin pelaaja, jolla on laivasto ja pelialue(ruudukko).
 *
 *
 * @author Antti Pasanen
 *
 */

public class Pelaaja {

	private Ruudukko ruudukko;
	private Laivasto laivasto;

	/**
	 * Konstruktori. Luo ruudukon default koolla.
	 */
	public Pelaaja() {
		this.ruudukko = new Ruudukko(null);
		this.laivasto = new Laivasto();
	}

	/**
	 * Asettaa pelaajan ruudukon annettuun kokoon.
	 *
	 * @param koko Ruudukon koko
	 */
	public void setKarttaKoko(KoordinaattiPari koko) {
		this.ruudukko = new Ruudukko(koko);
	}


	/**
	 * Lisaa pelaajalle laivan annettuihin ruutuihin. Lisays epaonnistuu jos pelaajalla
	 * on jo ruuduissa laiva tai koordinaatit ovat ohi ruudukon
	 *
	 *
	 * @param koordinaatit KoordinaattiPari[] koordinaatit
	 * @return True, jos lisays onnistui
	 */
	public boolean lisaaLaiva(KoordinaattiPari[] koordinaatit) {
		// koordinaatit puuttuvat
		if (koordinaatit == null)
			return false;

		// koordinaattien tarkistus
		for (int i = 0; i < koordinaatit.length; i++) {
			if (!ruudukko.onTyhja(koordinaatit[i]))
				return false;
			for (int j = i + 1; j < koordinaatit.length; j++) {
				if (koordinaatit[i].equals(koordinaatit[j]))
					return false;
			}
		}

		ArrayList<Ruutu> ruudut = new ArrayList<Ruutu>();

		for (int i = 0; i < koordinaatit.length; i++) {
			ruudut.add(ruudukko.getRuutu(koordinaatit[i]));
		}

		return laivasto.lisaaLaiva(ruudut);
	}

	/**
	 * Palauttaa pelaajan laivaston.
	 *
	 * @return Laivasto-olio
	 */
	public Laivasto getLaivasto() {
		return laivasto;
	}

	/**
	 * Palauttaa pelaajan ruudukon.
	 *
	 * @return Ruudukko-olio
	 */
	public Ruudukko getRuudukko() {
		return ruudukko;
	}

}
