package model;

public interface LaivanupotusAI_IF {

	/**
	 * Yrittaa lisata laivan maaritellylle pelaajalle.
	 *
	 * @param pelaajanNimi Pelaajan nimi jolle laiva lisataan
	 * @param laivanPituus Lisattavan laivan pituus
	 *
	 * @return True, jos lisays onnistui
	 */
	boolean asetaLaiva(PelaajanNimi pelaajanNimi, int laivanPituus);

	/**
	 * Suorittaa ampumisen.
	 *
	 * @param pelaajanNimi
	 *            Kohdepelaajan nimi.
	 * @return True jos ampuminen onnistui.
	 */
	boolean ammu(PelaajanNimi pelaajanNimi);

	/**
	 * Suorittaa normiampumisen.
	 *
	 * @param pelaajanNimi annetun pelaajan nimi
	 * @return True, jos ampuminen onnistui
	 */
	boolean normiAmmu(PelaajanNimi pelaajanNimi);

	/**
	 * Suorittaa huijausampumisen.
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui
	 */
	boolean huijausAmmu(PelaajanNimi pelaajanNimi);

	/**
	 * Suorittaa superampumisen.
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui
	 *
	 */
	boolean superAmmu(PelaajanNimi pelaajanNimi);


	/**
	 * Suoritetaan smartampuminen.
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui.
	 *
	 */
	boolean smartAmmu(PelaajanNimi pelaajanNimi);

	/**
	 * Palauttaa vaikeustason.
	 *
	 * @return Vaikeustaso
	 */
	int getVaikeusTaso();

	/**
	 * Asettaa vaikeustason (1...n)
	 *
	 * @param vaikeusTaso Vaikeustaso
	 */
	void setVaikeusTaso(int vaikeusTaso);

	/**
	 * @return Palauttaa huijausprosentin.
	 */
	int getHuijausProsentti();

	/**
	 * Asettaa huijausprosentin.
	 *
	 * @param prosentti Huijausprosentti
	 */
	void setHuijausProsentti(int prosentti);

	/**
	 * Viimeisimman ampumisen koordinaatit.
	 *
	 * @return KoordinaattiPari koordinaatit
	 */
	KoordinaattiPari getLaukaus();

	/**
	 * Viimeisimman asennetun laivan koordinaatit
	 *
	 * @return KoordinaattiPari[] KoordinaattiPari-taulukko
	 */
	KoordinaattiPari[] getViimeisinAsennettuLaiva();

}
