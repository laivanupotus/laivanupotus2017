package controller;

import java.util.ArrayList;

import model.KoordinaattiPari;
import model.PelaajanNimi;
import view.GameButton;

public interface Controller_IF {
	public abstract boolean lisaaLaiva(PelaajanNimi nimi, ArrayList<GameButton> buttonPath);
	public abstract void poistaKaikkiPelaajat();
	public abstract void uusiPelaaja(PelaajanNimi pelaajanNimi);
	public abstract Boolean ammu(PelaajanNimi nimi, KoordinaattiPari koordinaatit);
	public abstract Boolean onkoRuudussaLaiva(PelaajanNimi nimi, KoordinaattiPari koordinaatit);
	public abstract Boolean pelaajanLaivatUponnut(PelaajanNimi nimi);
	public abstract boolean ammu_AI(PelaajanNimi pelaajanNimi);
	public abstract KoordinaattiPari aiCoord();
	public abstract void setAIvaikeus(int v);
	public abstract int getAIvaikeus();
	public abstract int getPelaajanLaivojenYhteiskoko(PelaajanNimi pelaajanNimi);
	public abstract void poistaPelaaja(PelaajanNimi pelaajanNimi);
	void roundEnd();
	void gameExit();

}
