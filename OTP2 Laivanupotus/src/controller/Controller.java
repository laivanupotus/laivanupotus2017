package controller;

import java.util.ArrayList;
import model.KoordinaattiPari;
import model.LaivanupotusAI;
import model.LaivanupotusAI_IF;
import model.LaivanupotusPeli;
import model.LaivanupotusPeli_IF;
import model.PelaajanNimi;
import tilastot.Tilastot;
import tilastot.Tilastot_IF;
import view.GameButton;
import view.GameGrid_Ctrl;

public class Controller implements Controller_IF {

	private LaivanupotusPeli_IF lup;
	private LaivanupotusAI_IF ai;
	private Tilastot_IF tilastot;

	private static Controller instance;

	private Controller() {
		this.lup = new LaivanupotusPeli();
		this.ai = new LaivanupotusAI(lup);
		tilastot = new Tilastot();
		tilastot.connectDAO();
	}

	public static Controller getInstance() {
		if (instance == null) {
			instance = new Controller();
		}
		return instance;
	}

	public Tilastot_IF getTilastot() {
		return tilastot;
	}

	/**
	 * Asettaa tekoälyn generoimaan laivastonsa
	 *
	 * @param AI
	 *            Teköälypelaajan nimi
	 */
	private void generateAIships(PelaajanNimi AI) {
		if (lup.onPelaaja(AI))
			lup.poistaPelaaja(AI);
		lup.uusiPelaaja(AI);
		int i = GameGrid_Ctrl.getShipcountdefault();
		while (1 < i) {
			int laivanPituus = (int) (Math.round(Math.random() * 3 + 2));
			if (0 <= (i - laivanPituus)) {
				if (ai.asetaLaiva(AI, laivanPituus)) {
					i = i - laivanPituus;
				}
			}
		}
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi
	 * @return Kts. LaivanupotusAI -luokka.
	 */
	@Override
	public boolean ammu_AI(PelaajanNimi pelaajanNimi) {
		return ai.ammu(pelaajanNimi);
	}

	/**
	 * @return Palauta x-koordinaatti.
	 */
	@Override
	public KoordinaattiPari aiCoord() {
		return ai.getLaukaus();
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param AI
	 *            Teköälypelaajan nimi.
	 */
	@Override
	public void uusiPelaaja(PelaajanNimi pelaajanNimi) {
		PelaajanNimi ai = new PelaajanNimi("AI");
		if (lup.onPelaaja(pelaajanNimi))
			lup.poistaPelaaja(pelaajanNimi);
		lup.uusiPelaaja(pelaajanNimi);
		lup.uusiPelaaja(ai);
		generateAIships(ai);
		tilastot.uusiKaksinpeli(pelaajanNimi, ai, lup);
	}

	/**
	 * @param ai2
	 *            Tekoälypelaajan nimi.
	 * @param x
	 *            X-koordinaatti.
	 * @param y
	 *            Y-koordinaatti.
	 * @return Palauttaa boolean tiedon ampumisesta.
	 */
	@Override
	public Boolean ammu(PelaajanNimi ai2, KoordinaattiPari koordinaatit) {
		return lup.ammu(ai2, koordinaatit);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param x
	 *            X-koordinaatti.
	 * @param y
	 *            Y-koordinaatti.
	 * @return Palauttaa boolean tiedon oliko ruudussa laiva.
	 */
	@Override
	public Boolean onkoRuudussaLaiva(PelaajanNimi pelaajanNimi, KoordinaattiPari koordinaatit) {
		return lup.onkoRuudussaLaiva(pelaajanNimi, koordinaatit);
	}

	/**
	 * @param ai2
	 *            Pelaajan nimi.
	 * @return Palauttaa tiedon onko pelaajan laivat uponneet.
	 */
	@Override
	public Boolean pelaajanLaivatUponnut(PelaajanNimi ai2) {
		return lup.pelaajanLaivatUponnut(ai2);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param buttonPath
	 * @param shipXCoordold
	 *            Edellisen valinnan x-koordinaatti.
	 * @param shipYCoordold
	 *            Edellisen valinnan y-koordinaatti.
	 * @param xCoord
	 *            Nykyisen valinnan x-koordinaatti.
	 * @param yCoord
	 *            Nykyisen valinnan x-koordinaatti.
	 * @return Palautetaan boolean tieto lisäyksen onnistumisesta.
	 */
	@Override
	public boolean lisaaLaiva(PelaajanNimi pelaajanNimi, ArrayList<GameButton> buttonPath) {
		KoordinaattiPari[] koordinaatit = new KoordinaattiPari[buttonPath.size()];
		for (int i = 0; i < buttonPath.size(); i++) {
			koordinaatit[i] = KoordinaattiPari.create(Integer.valueOf(buttonPath.get(i).getKoordinaatit().getX()),
					Integer.valueOf(buttonPath.get(i).getKoordinaatit().getY()));
		}
		return lup.lisaaLaiva(pelaajanNimi, koordinaatit);
	}

	/**
	 * Poistaa nykyisen pelisession paikalliset pelaajat.
	 */
	@Override
	public void poistaKaikkiPelaajat() {
		lup.poistaKaikkiPelaajat();
	}

	/**
	 * @param v
	 *            Tekoälypelaajan vaikeusaste.
	 */
	@Override
	public void setAIvaikeus(int v) {
		ai.setVaikeusTaso(v);
	}

	/**
	 * @return Palauta tekoälypelaan nykyinen vaikeusaste.
	 */
	@Override
	public int getAIvaikeus() {
		return ai.getVaikeusTaso();
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @return Palautetaan pelaajan laivojen yhteiskoon lukumäärä.
	 */
	@Override
	public int getPelaajanLaivojenYhteiskoko(PelaajanNimi pelaajanNimi) {
		return lup.getPelaajanLaivojenYhteiskoko(pelaajanNimi);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 */
	@Override
	public void poistaPelaaja(PelaajanNimi pelaajanNimi) {
		lup.poistaPelaaja(pelaajanNimi);
	}


	@Override
	public void roundEnd() {
		tilastot.uploadKaksinpelit();
	}

	@Override
	public void gameExit() {
		tilastot.closeDAO();
	}

}
