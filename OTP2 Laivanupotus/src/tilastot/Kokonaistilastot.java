package tilastot;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Luokka kaikkien pelaajien kokonaistilastojen sailytysta varten.
 *
 * @author Antti Pasanen
 *
 */
public class Kokonaistilastot {

	private Map<String, PelaajanKokonaistilastot> kokonaistilastot;

	public Kokonaistilastot() {
		kokonaistilastot = new LinkedHashMap<String, PelaajanKokonaistilastot>();
	}


	/**
	 * Palauttaa pelaajien nimet.
	 *
	 * @return Set<String> pelaajien nimet
	 */
	public Set<String> getPelaajienNimet(){
		return kokonaistilastot.keySet();
	}

	/**
	 * Palauttaa pelaajan kokonaistilastot annetulla nimella.
	 *
	 * @param nimi Pelaajan nimi, jonka tilastot halutaan.
	 * @return PelaajanKokonaistilastot tai null jos pelaaja ei loydy.
	 */
	public PelaajanKokonaistilastot getPelaajanKokonaistilastot(String nimi) {
		if (kokonaistilastot.containsKey(nimi))
			return kokonaistilastot.get(nimi);

		return null;
	}

	/**
	 * Lisaa pelajaat annetusta Kaksinpelista tilastoihin.
	 *
	 * @param kp Kaksinpeli mista pelaajat otetaan.
	 */
	public void addPelaajanKokonaistilastot(Kaksinpeli kp) {
		updateKokonaistilastot(kp.getPelinPelaaja1());
		updateKokonaistilastot(kp.getPelinPelaaja2());
	}

	/**
	 * Paivittaa tilastoja pelaajan tiedoilla.
	 *
	 * @param pelaaja Pelaajan jonka tiedoilla paivitetaan.
	 */
	private void updateKokonaistilastot(PelinPelaaja pelaaja) {
		kokonaistilastot.putIfAbsent(pelaaja.getPelaajanNimi(),
				new PelaajanKokonaistilastot(pelaaja.getPelaajanNimi()));

		kokonaistilastot.compute(pelaaja.getPelaajanNimi(), (k, v) -> {
			v.setLaukaukset(v.getLaukaukset() + pelaaja.getLaukaukset());
			v.setOsumat(v.getOsumat() + pelaaja.getOsumat());
			v.setPelit(v.getPelit() + 1);
			if (pelaaja.getVoitto() == -1)
				v.setTappiot(v.getTappiot() + 1);
			if (pelaaja.getVoitto() == 1)
				v.setVoitot(v.getVoitot() + 1);
			return v;});
	}

	/**
	 * @return the kokonaistilastot
	 */
	public Map<String, PelaajanKokonaistilastot> getKokonaistilastot() {
		return kokonaistilastot;
	}

	/**
	 * @param kokonaistilastot the kokonaistilastot to set
	 */
	public void setKokonaistilastot(Map<String, PelaajanKokonaistilastot> kokonaistilastot) {
		this.kokonaistilastot = kokonaistilastot;
	}
}
