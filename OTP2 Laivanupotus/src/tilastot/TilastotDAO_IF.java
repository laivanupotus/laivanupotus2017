package tilastot;

/**
 * Data Access Object - interface
 *
 * @author Antti Pasanen
 *
 */

public interface TilastotDAO_IF {

	/**
	 * Tallentaa kaksinpelit olion tiedot tietokantaan.
	 *
	 *@return boolean true jos yhteys tietokantaan onnistui
	 */
	boolean updateKaksinpelit(Kaksinpelit kaksinpelit);

	/**
	 * Lataa tietokannasta kaksinpelit olion
	 *
	 * @param kaksinpelit Kaksinpelit minne tiedot ladataan
	 * @return boolean true, jos toiminnon teko tietokantaan onnistui
	 */
	boolean loadKaksinpelit(Kaksinpelit kaksinpelit);

	/**
	 * DAO:n sulkeminen.
	 */
	void exit();
}
