package tilastot;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Tilastot data access object, suorittaa hibernate komennot tietokantaan.
 *   Yhteyden muodostus
 *   Tietojen tallennus tietokantaan
 *   Tietojen lataus tietokannasta
 *
 *
 *
 * @author Antti Pasanen
 *
 */

public class TilastotDAO implements TilastotDAO_IF, Runnable {

	SessionFactory sf;
	Tilastot tilasto;


	public TilastotDAO(Tilastot tilastot) {
		// piilota hibernate consolespam
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		this.tilasto = tilastot;
		sf = null;
	}

	/**
	 * Muodostaa yhteyden tietokantaan.
	 * Jos yhteys muodostui ok, muuttaa tilastoluokan
	 * tietokantayhteys muuttujan true asentoon.
	 *
	 */
	@Override
	public void run() {
		sf = null;
		try {
			sf = new Configuration().configure()
					.addAnnotatedClass(Kaksinpeli.class)
					.addAnnotatedClass(PelinPelaaja.class)
					.buildSessionFactory();
			tilasto.setStatusDAO(true);
		} catch (Exception e) {
			tilasto.setStatusDAO(false);
			//e.printStackTrace();
		}
	}


	/**
	 * Tallentaa kaksinpelit olion tiedot tietokantaan.
	 *
	 *@return boolean true jos yhteys tietokantaan onnistui
	 */
	@Override
	public boolean updateKaksinpelit(Kaksinpelit kaksinpelit) {

		Session session = sf.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Set<String> keys = kaksinpelit.getKaksinpelit().keySet();
			keys.forEach(key->{
				session.saveOrUpdate(kaksinpelit.getKaksinpeliByID(key));
				session.saveOrUpdate(kaksinpelit.getKaksinpeliByID(key).getPelinPelaaja1());
				session.saveOrUpdate(kaksinpelit.getKaksinpeliByID(key).getPelinPelaaja2());
			});

			transaction.commit();
			return true;
		}
		catch(Exception e) {
			if (transaction != null)
				transaction.rollback();
			//e.printStackTrace();
		}
		finally {
			session.close();
		}
		return false;
	}


	/**
	 * Lataa tietokannasta kaksinpelit olion
	 *   Kaksinpeli taulukko
	 *   Pelin_pelaaja taulukko
	 * Yhdistaa tiedot niin etta oliot ovat linkitetty ok.
	 *
	 * @param kaksinpelit Kaksinpelit minne tiedot ladataan
	 * @return boolean true, jos toiminnon teko tietokantaan onnistui
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean loadKaksinpelit(Kaksinpelit kaksinpelit) {
		Session session = sf.openSession();
		Transaction transaction = null;
		List<Kaksinpeli> pelit;
		List<PelinPelaaja> pelaajat;

		try {
			transaction = session.beginTransaction();
			pelit = session.createQuery("FROM " + Kaksinpeli.class.getName()).getResultList();
			pelaajat = session.createQuery("FROM " + PelinPelaaja.class.getName()).getResultList();
			pelit.forEach(key->{
				kaksinpelit.put(key);
			});
			pelaajat.forEach(pelaaja->{
				if (kaksinpelit.getKaksinpeliByID(pelaaja.getId()).getPelaaja1().equals(pelaaja.getPelaajanNimi()))
					kaksinpelit.getKaksinpeliByID(pelaaja.getId()).setPelinPelaaja1(pelaaja);
				else
					kaksinpelit.getKaksinpeliByID(pelaaja.getId()).setPelinPelaaja2(pelaaja);
			});
			transaction.commit();
			return true;
		}
		catch(Exception e) {
			if (transaction != null)
				transaction.rollback();
			//e.printStackTrace();
		}
		finally {
			session.close();
		}
		return false;
	}

	/**
	 * Sulkee yhteyden manuaalisesti.
	 */
	@Override
	public void exit() {
		if (sf != null)
			if (sf.isOpen())
				sf.close();
	}
}