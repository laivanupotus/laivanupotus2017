package tilastot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 *
 * Luokka kaksinpelissa olevan yksittaisen pelaajan tietojen tallentamista varten.
 *
 *
 * @author Antti Pasanen
 *
 */

@Entity
@Table(name="Pelin_pelaaja")
@IdClass(PelinPelaajaID.class)
public class PelinPelaaja {

	@Id
	@Column(name="pelaajan_nimi")
	private String pelaajanNimi;
	@Column(name="laukaukset")
	private int laukaukset;
	@Column(name="osumat")
	private int osumat;
	@Id
	@Column(name="pelin_id")
	private String id;
	@Column(name="voitto")
	private int voitto;

	/**
	 * Konstruktori.
	 *
	 * @param pelaajanNimi Luodun pelaajan nimi
	 * @param id Kaksinpelin tunnuskoodi
	 */

	public PelinPelaaja(String pelaajanNimi, String id) {
		this.id = id;
		this.pelaajanNimi = pelaajanNimi;
		laukaukset = 0;
		osumat = 0;
		setVoitto(0);
	}

	public PelinPelaaja() {}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the osumat
	 */
	public int getOsumat() {
		return osumat;
	}

	/**
	 * @param osumat the osumat to set
	 */
	public void setOsumat(int osumat) {
		this.osumat = osumat;
	}

	/**
	 * @return the laukaukset
	 */
	public int getLaukaukset() {
		return laukaukset;
	}

	/**
	 * @param laukaukset the laukaukset to set
	 */
	public void setLaukaukset(int laukaukset) {
		this.laukaukset = laukaukset;
	}

	/**
	 * @return the pelaajannimi
	 */
	public String getPelaajanNimi() {
		return pelaajanNimi;
	}

	/**
	 * @param pelaajannimi the pelaajannimi to set
	 */
	public void setPelaajanNimi(String pelaajanNimi) {
		this.pelaajanNimi = pelaajanNimi;
	}

	/**
	 * Asettaa pelaajan voittajaksi tai haviajaksi parametrista riippuen.
	 *
	 * @param voitto true jos pelaaja voitti, false jos havisi.
	 */
	public void setVoitto(boolean voitto) {
		if (voitto)
			this.voitto = 1;
		else
			this.voitto = -1;
	}

	/**
	 * @return the voitto
	 */
	public int getVoitto() {
		return voitto;
	}

	/**
	 * @param voitto the voitto to set
	 */
	public void setVoitto(int voitto) {
		this.voitto = voitto;
	}
}
