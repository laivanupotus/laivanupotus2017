package tilastot;

import model.LaivanupotusPeli_IF;
import model.LaivapeliMediator;
import model.PelaajanNimi;


/**
 * Luokka laivanupotuspelin tietokannan kayttoa varten.
 *
 *
 * @author Antti Pasanen
 *
 */
public class Tilastot implements Tilastot_IF {

	private TilastotDAO DAO; // data access object
	private boolean DAOStatus;
	private Thread DAOThread;
	Kaksinpelit kaksinpelit;


	public Tilastot() {
		DAOStatus = false;
		DAO = new TilastotDAO(this);
		connectDAO();
		kaksinpelit = new Kaksinpelit();
	}

	/**
	 * Yrittaa luoda yhteyden tietokantaan
	 */
	@Override
	public void connectDAO() {
		if (DAOThread == null) {
			DAOThread = new Thread(DAO);
			DAOThread.start();
		}
		else if (!DAOThread.isAlive()) {
			DAOThread = new Thread(DAO);
			DAOThread.start();
		}
	}

	/**
	 * Tietokantayhteyden status
	 *
	 * @return True, jos yhteys on paalla
	 */
	@Override
	public boolean isDAOConnected() {
		return DAOStatus;
	}

	/**
	 * Asettaa statuksen tietokantayhteydesta
	 *
	 * @param status True jos paalla
	 */
	protected void setStatusDAO(boolean status) {
		DAOStatus = status;
	}


	/**
	 * Luo uuden kaksinpelin.
	 * @param pelaajanNimi1 pelaajan 1 nimi
	 * @param pelaajanNimi2 pelaajan 2 nimi
	 * @param lp laivanupotuspeli
	 *
	 * @return String luodun pelin yksikasitteinen tunnus
	 *
	 */
	@Override
	public String uusiKaksinpeli(PelaajanNimi pelaajanNimi1, PelaajanNimi pelaajanNimi2, LaivanupotusPeli_IF lp) {

		String id = kaksinpelit.uusiKaksinpeli(pelaajanNimi1.getNimi(), pelaajanNimi2.getNimi());
		if (id == null)
			return null;

		LaivapeliMediator m1 = new LaivapeliMediator(this);
		LaivapeliMediator m2 = new LaivapeliMediator(this);
		m1.setPelinPelaaja(kaksinpelit.getKaksinpeliByID(id).getPelinPelaaja1());
		m2.setPelinPelaaja(kaksinpelit.getKaksinpeliByID(id).getPelinPelaaja2());
		lp.lisaaLaivapeliMediator(pelaajanNimi1, m1);
		lp.lisaaLaivapeliMediator(pelaajanNimi2, m2);

		return id;
	}

	/**
	 * get Kaksinpelit
	 *
	 * @return Kaksinpelit olio
	 */
	@Override
	public Kaksinpelit getKaksinpelit() {
		return kaksinpelit;
	}

	/**
	 * Paivittaa kaksinpelit tietokantaan jos yhteys on paalla.
	 *
	 * @return boolean false, jos yhteys ei ollut paalla.
	 */
	@Override
	public boolean uploadKaksinpelit() {
		if (DAOStatus)
			return DAO.updateKaksinpelit(kaksinpelit);
		else
			return false;
	}

	/**
	 * Lataa tiedot kaksinpeleista tietokannasta
	 * jos yhteys on paalla.
	 *
	 *
	 * @return boolean false, jos yhteys ei ollut paalla.
	 */
	@Override
	public boolean downloadKaksinpelit() {
		if (DAOStatus) {
			DAO.loadKaksinpelit(kaksinpelit);
			return true;
		}
		return false;
	}

	/**
	 * Manuaalisesti sulkee hibernate yhteyden etta ohjelma sammuu ok.
	 *
	 */
	@Override
	public void closeDAO() {
		DAO.exit();
	}
}
