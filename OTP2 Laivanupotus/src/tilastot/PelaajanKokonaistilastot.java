package tilastot;

/**
 *
 * Luokka yksittaisen pelaajan kokonaistilastoille.
 *
 * @author Antti Pasanen
 *
 */
public class PelaajanKokonaistilastot {

	private String nimi;
	private int voitot = 0;
	private int tappiot = 0;
	private int laukaukset = 0;
	private int osumat = 0;
	private int pelit = 0;

	public PelaajanKokonaistilastot(String nimi) {
		this.nimi = nimi;
	}

	/**
	 * @return the osumat
	 */
	public int getOsumat() {
		return osumat;
	}
	/**
	 * @param osumat the osumat to set
	 */
	public void setOsumat(int osumat) {
		this.osumat = osumat;
	}
	/**
	 * @return the laukaukset
	 */
	public int getLaukaukset() {
		return laukaukset;
	}
	/**
	 * @param laukaukset the laukaukset to set
	 */
	public void setLaukaukset(int laukaukset) {
		this.laukaukset = laukaukset;
	}
	/**
	 * @return the tappiot
	 */
	public int getTappiot() {
		return tappiot;
	}
	/**
	 * @param tappiot the tappiot to set
	 */
	public void setTappiot(int tappiot) {
		this.tappiot = tappiot;
	}
	/**
	 * @return the voitot
	 */
	public int getVoitot() {
		return voitot;
	}
	/**
	 * @param voitot the voitot to set
	 */
	public void setVoitot(int voitot) {
		this.voitot = voitot;
	}
	/**
	 * @return the nimi
	 */
	public String getNimi() {
		return nimi;
	}
	/**
	 * @param nimi the nimi to set
	 */
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	/**
	 * @return the pelit
	 */
	public int getPelit() {
		return pelit;
	}

	/**
	 * @param pelit the pelit to set
	 */
	public void setPelit(int pelit) {
		this.pelit = pelit;
	}
}
