package tilastot;

import java.io.Serializable;

/**
 *
 * Luokka avainparia varten hibernate tietokantayhteytta varten
 *
 * @author Antti Pasanen
 *
 */
@SuppressWarnings("serial")
public class PelinPelaajaID implements Serializable {

	private String pelaajanNimi;
	private String id;

	public PelinPelaajaID() {}

	/**
	 * @return the pelaajanNimi
	 */
	public String getPelaajanNimi() {
		return pelaajanNimi;
	}

	/**
	 * @param pelaajanNimi the pelaajanNimi to set
	 */
	public void setPelaajanNimi(String pelaajanNimi) {
		this.pelaajanNimi = pelaajanNimi;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
