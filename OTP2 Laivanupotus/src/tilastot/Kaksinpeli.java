package tilastot;

import java.util.UUID;

import javax.persistence.*;

/**
 * Kaksinpeli luokka kuvaa tilastoja kahden pelaajan valisesta pelista.
 * Pelaajat talletettu tietoineen PelinPelaaja luokkiin.
 * Pelin tunnus id generoitu uniikkina.
 *
 * CREATE TABLE Kaksinpeli (
 * pelin_id varchar(40),
 * pelaaja1 varchar(50),
 * pelaaja2 varchar(50),
 * PRIMARY KEY(pelin_id)
 * ); *
 *
 * @author Antti Pasanen
 *
 */

@Entity
@Table(name="Kaksinpeli")
public class Kaksinpeli {

	@Id
	@Column(name="pelin_id", length=40)
	private String id;

	@Column(name="pelaaja1", length=50, nullable=false)
	private String pelaaja1;

	@Column(name="pelaaja2", length=50, nullable=false)
	private String pelaaja2;

	@Transient
	private PelinPelaaja pelinPelaaja1;
	@Transient
	private PelinPelaaja pelinPelaaja2;

	public Kaksinpeli() {}

	public Kaksinpeli(String pelaaja1, String pelaaja2) {
		this.pelaaja1 = pelaaja1;
		this.pelaaja2 = pelaaja2;
		id = UUID.randomUUID().toString();

		pelinPelaaja1 = new PelinPelaaja(pelaaja1, id);
		pelinPelaaja2 = new PelinPelaaja(pelaaja2, id);
	}

	/**
	 * Asettaa pelaajan annetulla nimella voittajaksi,
	 * ja toisen pelaajan haviaksi.
	 *
	 * @param nimi Voittavan pelaajan nimi.
	 */
	public void setVoittaja(String nimi) {
		if (nimi.equals(this.pelaaja1)) {
			pelinPelaaja1.setVoitto(true);
			pelinPelaaja2.setVoitto(false);
		}
		else if (nimi.equals(this.pelaaja2)) {
			pelinPelaaja1.setVoitto(false);
			pelinPelaaja2.setVoitto(true);
		}
	}

	/**
	 * Asettaa pelaajan annetulla nimella haviajaksi,
	 * ja toisen pelaajan voittajaksi.
	 *
	 * @param nimi Haviavan pelaajan nimi
	 */
	public void setHaviaja(String nimi) {
		if (nimi.equals(this.pelaaja1)) {
			pelinPelaaja1.setVoitto(false);
			pelinPelaaja2.setVoitto(true);
		}
		else if (nimi.equals(this.pelaaja2)) {
			pelinPelaaja1.setVoitto(true);
			pelinPelaaja2.setVoitto(false);
		}
	}

	/**
	 * Palauttaa vastapelaajan annetulla nimella kysytylle pelaajalle,
	 * tai null jos pelissa ei ole nimella varustettua pelaajaa.
	 *
	 * @param nimi Pelaajan, jonka vastapelaaja haetaan, nimi
	 * @return PelinPelaaja vastapelaaja, tai null
	 */
	public PelinPelaaja getToinenPelinPelaaja(String nimi) {
		if (nimi.equals(pelaaja1))
			return pelinPelaaja2;
		else if (nimi.equals(pelaaja2))
			return pelinPelaaja1;
		else
			return null;
	}


	/**
	 * @return the pelaaja2
	 */
	public String getPelaaja2() {
		return pelaaja2;
	}

	/**
	 * @param pelaaja2 the pelaaja2 to set
	 */
	public void setPelaaja2(String pelaaja2) {
		this.pelaaja2 = pelaaja2;
	}

	/**
	 * @return the pelaaja1
	 */
	public String getPelaaja1() {
		return pelaaja1;
	}

	/**
	 * @param pelaaja1 the pelaaja1 to set
	 */
	public void setPelaaja1(String pelaaja1) {
		this.pelaaja1 = pelaaja1;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the pelinPelaaja2
	 */
	public PelinPelaaja getPelinPelaaja2() {
		return pelinPelaaja2;
	}

	/**
	 * @param pelinPelaaja2 the pelinPelaaja2 to set
	 */
	public void setPelinPelaaja2(PelinPelaaja pelinPelaaja2) {
		this.pelinPelaaja2 = pelinPelaaja2;
	}

	/**
	 * @return the pelinPelaaja1
	 */
	public PelinPelaaja getPelinPelaaja1() {
		return pelinPelaaja1;
	}

	/**
	 * @param pelinPelaaja1 the pelinPelaaja1 to set
	 */
	public void setPelinPelaaja1(PelinPelaaja pelinPelaaja1) {
		this.pelinPelaaja1 = pelinPelaaja1;
	}
}
