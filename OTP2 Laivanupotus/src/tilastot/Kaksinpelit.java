package tilastot;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Kaksinpelit luokka sisaltaa kaksinpeli-oliot.
 *
 *
 * @author Antti Pasanen
 *
 */
public class Kaksinpelit {

	private Map<String, Kaksinpeli> kaksinpelit;

	public Kaksinpelit() {
		setKaksinpelit(new LinkedHashMap<String, Kaksinpeli>());
	}

	/**
	 *
	 * Luo uusi peli kahden pelaajan valille.
	 *
	 * @param pelaajanNimi1 Pelaajan 1 nimi
	 * @param pelaajanNimi2 Pelaajan 2 nimi
	 * @return String pelin tunnus.
	 */
	public String uusiKaksinpeli(String pelaajanNimi1, String pelaajanNimi2) {
		if (pelaajanNimi1 == null || pelaajanNimi2 == null)
			return null;

		Kaksinpeli kaksinpeli = new Kaksinpeli(pelaajanNimi1, pelaajanNimi2);
		put(kaksinpeli);

		return kaksinpeli.getId();
	}

	/**
	 * Lisaa uuden kaksinpelin.
	 *
	 * @param id tunnus
	 * @param kaksinpeli kaksinpeli
	 */
	public void put(Kaksinpeli kaksinpeli) {
		kaksinpelit.put(kaksinpeli.getId(), kaksinpeli);
	}

	/**
	 * Palauttaa kaksinpelin annetulla tunnuksella.
	 *
	 * @param id tunnus
	 * @return Kaksinpeli
	 */
	public Kaksinpeli getKaksinpeliByID(String id) {
		if (!kaksinpelit.containsKey(id))
			return null;

		return kaksinpelit.get(id);
	}

	/**
	 * Palauttaa pelaajien kokonaistilastotiedot Kokonaistilastot-oliona.
	 *
	 * @return Kokonaistilastot kokonaistilastot
	 */
	public Kokonaistilastot getKokonaistilastot() {
		Kokonaistilastot kokonaistilastot = new Kokonaistilastot();

		kaksinpelit.forEach((nimi, peli)->{
			kokonaistilastot.addPelaajanKokonaistilastot(peli);
		});

		return kokonaistilastot;
	}

	/**
	 * @return the kaksinpelit
	 */
	public Map<String, Kaksinpeli> getKaksinpelit() {
		return kaksinpelit;
	}

	/**
	 * @param kaksinpelit the kaksinpelit to set
	 */
	public void setKaksinpelit(Map<String, Kaksinpeli> kaksinpelit) {
		this.kaksinpelit = kaksinpelit;
	}
}
