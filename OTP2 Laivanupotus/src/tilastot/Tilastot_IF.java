package tilastot;

import model.LaivanupotusPeli_IF;
import model.PelaajanNimi;

/**
 * Tilastot interface
 *
 * @author Antti Pasanen
 *
 */

public interface Tilastot_IF {

	/**
	 * Tietokantayhteyden status
	 *
	 * @return True, jos yhteys on paalla
	 */
	boolean isDAOConnected();

	/**
	 * Yrittaa luoda yhteyden tietokantaan
	 */
	void connectDAO();

	/**
	 * DAO-yhteyden sulkemismetodi.
	 */
	void closeDAO();

	/**
	 * Luo uuden kaksinpelin.
	 * @param pelaajanNimi1 pelaajan 1 nimi
	 * @param pelaajanNimi2 pelaajan 2 nimi
	 * @param lp laivanupotuspeli
	 *
	 * @return String luodun pelin yksikasitteinen tunnus
	 *
	 */
	String uusiKaksinpeli(PelaajanNimi pelaajanNimi1, PelaajanNimi pelaajanNimi2, LaivanupotusPeli_IF lp);

	/**
	 * Tietojen paivitys tietokantaan.
	 *
	 * @return boolean false, jos ei onnistunut
	 */
	boolean uploadKaksinpelit();

	/**
	 * Tietojen lataus tietokannasta.
	 *
	 * @return boolean false, jos ei onnistunut
	 */
	boolean downloadKaksinpelit();

	/**
	 * get Kaksinpelit
	 *
	 * @return Kaksinpelit olio
	 */
	Kaksinpelit getKaksinpelit();
}