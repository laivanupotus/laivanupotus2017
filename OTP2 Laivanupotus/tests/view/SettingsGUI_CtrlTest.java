package view;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javafx.scene.paint.Color;

public class SettingsGUI_CtrlTest {
	SettingsGUI_Ctrl settings;

	@Before
	public void Aloitus(){
		settings = settings.getInstance();
	}

	/*
	 * Testi isInstanced ja getIstance metodeille
	 */
	@Test
	public void TestgetInstance(){
		settings.destroyInstance();
		assertFalse(settings.isInstanced());
		settings = settings.getInstance();
		assertTrue(settings.isInstanced());
		settings = settings.getInstance();

	}


	@After
	public void Nollaa(){
		settings.destroyInstance();
	}
}
