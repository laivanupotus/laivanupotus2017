package view;
import static org.junit.Assert.*;

import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class LanguageTest {
	Language language;
	Locale locale;
	ResourceBundle messages;
	
	
	@Test
	public void TestgetInstance(){
		language = language.getInstance();
		assertEquals(language, language.getInstance());
	}

	@Test
	public void TestsetLanguage(){
		language = language.getInstance();
		String lan = "fi";
		String cou = "FI";
		language.setLanguage(lan, cou);
		locale = new Locale(lan, cou);
		assertEquals(language.messages(), ResourceBundle.getBundle("properties.MessagesBundle_fi_FI", locale));
		
		lan = "en";
		cou = "EN";
		language.setLanguage(lan, cou);
		locale = new Locale(lan, cou);
		assertEquals(language.messages(), ResourceBundle.getBundle("properties.MessagesBundle_en_EN", locale));
		
		lan = "ja";
		cou = "JA";
		language.setLanguage(lan, cou);
		locale = new Locale(lan, cou);
		assertEquals(language.messages(), ResourceBundle.getBundle("properties.MessagesBundle_ja_JA", locale));
		
		lan = "muu";
		cou = "MUU";
		language.setLanguage(lan, cou);
		locale = new Locale(lan, cou);
		assertEquals(language.messages(), ResourceBundle.getBundle("properties.MessagesBundle_en_EN", locale));
		
	}


}
