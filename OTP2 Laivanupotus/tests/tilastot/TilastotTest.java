package tilastot;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import model.KoordinaattiPari;
import model.LaivanupotusAI;
import model.LaivanupotusPeli;
import model.LaivapeliMediator;
import model.PelaajanNimi;

/**
 *
 * Testit tilastoluokan toimintaa varten.
 * Vaatii toimivan pelin ja tekoalyn.
 *
 *
 * @author Antti Pasanen
 *
 */
@SuppressWarnings({"unused"})
public class TilastotTest {

	/**
	 *	DAO yhteystesti
	 */
	@Test
	public void testAccess() {
		Tilastot tilastot = new Tilastot();
		try {
			Thread.sleep(5000);// odota yhteyden muodostamista
		}
		catch(Exception e) {}

		if (tilastot.isDAOConnected())
			assertTrue("Tunneling off ?", tilastot.isDAOConnected());
		else
			assertFalse("Tunneling on ?", tilastot.isDAOConnected());
	}

	/**
	 * Mediator test, tilastojen paivitys mediaattorin kautta.
	 */
	@Test
	public void testMediator() {
		PelaajanNimi nimi1 = new PelaajanNimi("keke");
		PelaajanNimi nimi2 = new PelaajanNimi("joke");
		Tilastot tilastot = new Tilastot();
		LaivapeliMediator mediator = new LaivapeliMediator(tilastot);

		LaivanupotusPeli lp = new LaivanupotusPeli();
		lp.uusiPelaaja(nimi1);
		lp.uusiPelaaja(nimi2);
		lp.lisaaLaivapeliMediator(nimi1, mediator);

		String id = tilastot.getKaksinpelit().uusiKaksinpeli(nimi1.getNimi(), nimi2.getNimi());
		mediator.setPelinPelaaja(tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja1());

		// tilastot ennen laukauksia
		int laukaukset = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getLaukaukset();
		int osumat = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getOsumat();

		assertEquals(0, laukaukset);
		assertEquals(0, osumat);

		// laukaus mereen
		lp.ammu(nimi1, KoordinaattiPari.create(3, 4));
		laukaukset = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getLaukaukset();
		osumat = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getOsumat();

		assertEquals(1, laukaukset);
		assertEquals(0, osumat);

		// lisaa laiva, ja ammu osuma
		int[] x = {0,0,0};
		int[] y = {1,2,3};
		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(0, 1),
				KoordinaattiPari.create(0, 2),
				KoordinaattiPari.create(0, 3)
		};
		lp.lisaaLaiva(nimi1, koordinaatit);
		lp.ammu(nimi1, koordinaatit[1]);
		laukaukset = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getLaukaukset();
		osumat = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getOsumat();

		assertEquals(2, laukaukset);
		assertEquals(1, osumat);
		}

	/**
	 * Mediator test, pelin voittaminen upottamalla pelaajan koko laivasto
	 */
	@Test
	public void testMediatorPelinLopetus() {
		PelaajanNimi nimi1 = new PelaajanNimi("keke");
		PelaajanNimi nimi2 = new PelaajanNimi("joke");
		Tilastot tilastot = new Tilastot();
		LaivapeliMediator mediator = new LaivapeliMediator(tilastot);

		LaivanupotusPeli lp = new LaivanupotusPeli();
		lp.uusiPelaaja(nimi1);
		lp.uusiPelaaja(nimi2);
		lp.lisaaLaivapeliMediator(nimi1, mediator);

		String id = tilastot.getKaksinpelit().uusiKaksinpeli(nimi1.getNimi(), nimi2.getNimi());
		mediator.setPelinPelaaja(tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja1());

		// lisaa laiva, ja upota
		int[] x = {0,0,0};
		int[] y = {1,2,3};
		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(0, 1),
				KoordinaattiPari.create(0, 2),
				KoordinaattiPari.create(0, 3)
		};
		lp.lisaaLaiva(nimi1, koordinaatit);
		lp.ammu(nimi1, koordinaatit[0]);
		lp.ammu(nimi1, koordinaatit[1]);
		lp.ammu(nimi1, koordinaatit[2]);
		int laukaukset = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getLaukaukset();
		int osumat = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getOsumat();

		// kaikki laukaukset osumia
		assertEquals(3, laukaukset);
		assertEquals(3, osumat);

		// pelaaja1 laivasto upotettu, pelaaja2 voitti
		int voittoPelaaja1 = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja1().getVoitto();
		int voittoPelaaja2 = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getVoitto();

		assertEquals(-1, voittoPelaaja1);
		assertEquals(1, voittoPelaaja2);

		try {
			Thread.sleep(5000);// odota yhteyden muodostamista
		}
		catch(Exception e) {}

		tilastot.uploadKaksinpelit(); // tietokantapaivitys
		}


	/**
	 * Testipeli, tekoaly vs tekoaly
	 *
	 */
	@Test
	public void testAIPeli() {
		PelaajanNimi nimi1 = new PelaajanNimi("apina");
		PelaajanNimi nimi2 = new PelaajanNimi("sika");
		Tilastot tilastot = new Tilastot();

		LaivanupotusPeli lp = new LaivanupotusPeli();
		LaivanupotusAI ai = new LaivanupotusAI(lp);
		lp.uusiPelaaja(nimi1);
		lp.uusiPelaaja(nimi2);

		String id = tilastot.uusiKaksinpeli(nimi1, nimi2, lp);

		boolean ins = false;
		// aseta laivastot, 5 laivaa molemmille pelaajille
		for (int i = 0; i < 5; i++) {
			do {
				ins = ai.asetaLaiva(nimi1, 4);
			}
			while(!ins);
			do {
				ins = ai.asetaLaiva(nimi2, 4);
			}
			while(!ins);
		}

		// pelaa kunnes (vuoron jalkeen) toisen pelaajan laivat uponneet
		while(!lp.pelaajanLaivatUponnut(nimi1) && !lp.pelaajanLaivatUponnut(nimi2)) {
			ai.huijausAmmu(nimi1); // pelaaja 2 huijaa voittaakseen varmasti
			ai.normiAmmu(nimi2);
		}

		int pelaajan2laukaukset = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getLaukaukset();
		int pelaajan2osumat = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getOsumat();
		int pelaajan1laukaukset = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja1().getLaukaukset();
		int pelaajan1osumat = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja1().getOsumat();
		int voittoPelaaja1 = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja1().getVoitto();
		int voittoPelaaja2 = tilastot.getKaksinpelit().getKaksinpeliByID(id).getPelinPelaaja2().getVoitto();

		assertEquals(pelaajan2laukaukset, pelaajan1laukaukset); // sama maara laukauksia
		assertTrue(pelaajan2osumat > pelaajan1osumat); // huijari osuu useammin
		assertEquals(1, voittoPelaaja2); // huijari voittaa
		assertEquals(-1, voittoPelaaja1);

		// tulostus tietokantavertailua varten
		System.out.println("Pelaaja1: " + nimi1.getNimi());
		System.out.println("Laukaukset: " + pelaajan1laukaukset);
		System.out.println("Osumat: " + pelaajan1osumat);
		System.out.println("Voitto: " + voittoPelaaja1);
		System.out.println("-------------------");
		System.out.println("Pelaaja2: " + nimi2.getNimi());
		System.out.println("Laukaukset: " + pelaajan2laukaukset);
		System.out.println("Osumat: " + pelaajan2osumat);
		System.out.println("Voitto: " + voittoPelaaja2);

		try {
			Thread.sleep(5000);// odota yhteyden muodostamista
		}
		catch(Exception e) {}

		tilastot.uploadKaksinpelit(); // tietokantapaivitys
	}

	/**
	 * Lataustesti, lataa tietokannasta Pelin_pelaaja ja Kaksinpeli taulut,
	 * ja lisaa ne omaan taulukkoon. Tarkastaa etta kaikki taulut tuli olioihin
	 * linkitettya oikein.
	 *
	 */
	@Test
	public void testTietokantaLataus() {
		Tilastot tilastot = new Tilastot();

		try {
			Thread.sleep(5000);// odota yhteyden muodostamista
		}
		catch(Exception e) {}

		if (tilastot.downloadKaksinpelit()) {
			Map<String, Kaksinpeli> kaksinpelit = (LinkedHashMap<String, Kaksinpeli>) tilastot.getKaksinpelit().getKaksinpelit();

			kaksinpelit.forEach((id, kpeli)->{
				assertEquals("Kaksinpelin id vastaa mapin id:ta",id , kpeli.getId());
				assertEquals("Pelaajan 1 id vastaa pelin id:ta",
						kpeli.getId(), kpeli.getPelinPelaaja1().getId());
				assertEquals("Pelaajan 1 nimi vastaa kaksinpelin pelaajan 1 nimea",
						kpeli.getPelaaja1(), kpeli.getPelinPelaaja1().getPelaajanNimi());
				assertEquals("Pelaajan 2 id vastaa pelin id:ta",
						kpeli.getId(), kpeli.getPelinPelaaja2().getId());
				assertEquals("Pelaajan 2 nimi vastaa kaksinpelin pelaajan 2 nimea",
						kpeli.getPelaaja2(), kpeli.getPelinPelaaja2().getPelaajanNimi());
			});
		}
	}

	/**
	 * Hakee kokonaistilastot tietokannasta ja tulostaa.
	 */
	@Ignore
	@Test
	public void KokonaistilastotTest() {

		Tilastot tilastot = new Tilastot();

		try {
			Thread.sleep(5000);// odota yhteyden muodostamista
		}
		catch(Exception e) {}

		if (tilastot.isDAOConnected()) {
			if (tilastot.downloadKaksinpelit()) {
				Map<String, PelaajanKokonaistilastot>kokonaistilastot = tilastot.getKaksinpelit().getKokonaistilastot().getKokonaistilastot();

				System.out.println("--- Kokonaistilastotest BEGIN ---");
				kokonaistilastot.forEach((nimi, pelaaja)->{
					System.out.println("nimi: " + pelaaja.getNimi());
					System.out.println("  laukaukset: " + pelaaja.getLaukaukset());
					System.out.println("  osumat: " + pelaaja.getOsumat());
					System.out.println("  tappiot: " + pelaaja.getTappiot());
					System.out.println("  voitot: " + pelaaja.getVoitot());
					System.out.println("  pelit: " + pelaaja.getPelit());
				});
				System.out.println("--- Kokonaistilastotest END ---");
			}
		}
	}


	/**
	 * Testaa kokonaistilasto-luokan toiminnan.
	 * Pelaa 3 pelia ja tarkastaa pelien paatteeksi,
	 * etta tilastoihin on tullut oikeat tiedot.
	 */
	@Test
	public void KokonaistilastotLocalTest() {

		Tilastot tilastot = new Tilastot();
		LaivanupotusPeli lp = new LaivanupotusPeli();
		LaivanupotusAI ai = new LaivanupotusAI(lp);
		ai.setHuijausProsentti(100);

		PelaajanNimi nimi1 = new PelaajanNimi("lepakkomies");
		PelaajanNimi nimi2 = new PelaajanNimi("tarzan");

		lp.uusiPelaaja(nimi1);
		lp.uusiPelaaja(nimi2);

		tilastot.uusiKaksinpeli(nimi1, nimi2, lp);

		// aseta laivastot, 5 laivaa molemmille pelaajille
		asetaLaivat(ai, nimi1, nimi2);

		// pelaa kunnes (vuoron jalkeen) toisen pelaajan laivat uponneet
		while(!lp.pelaajanLaivatUponnut(nimi1) && !lp.pelaajanLaivatUponnut(nimi2)) {
			ai.huijausAmmu(nimi1); // pelaaja 2 huijaa voittaakseen varmasti
			ai.normiAmmu(nimi2);
		}

		Kokonaistilastot kt = tilastot.getKaksinpelit().getKokonaistilastot();
		PelaajanKokonaistilastot pkt = kt.getPelaajanKokonaistilastot(nimi2.getNimi());

		assertEquals("Pelaaja 2 ei osunut 20 kertaa vaikka piti", 20, pkt.getOsumat());
		assertEquals("Pelaaja 2 ei osunut joka laukauksella vaikka piti", pkt.getOsumat(), pkt.getLaukaukset());

		Set<String> nimet = kt.getPelaajienNimet();
		assertTrue("Pelaaja 1 on tilastoissa", nimet.contains(nimi1.getNimi()));
		assertTrue("Pelaaja 2 on tilastoissa", nimet.contains(nimi2.getNimi()));
		assertEquals("Tilastoissa ei ole muita pelaajia", 2, nimet.size());

		// uusi kierros, mutta pelaaja 1 huijaa talla kertaa
		lp.poistaKaikkiPelaajat();
		lp.uusiPelaaja(nimi1);
		lp.uusiPelaaja(nimi2);
		tilastot.uusiKaksinpeli(nimi1, nimi2, lp);
		asetaLaivat(ai, nimi1, nimi2);
		while(!lp.pelaajanLaivatUponnut(nimi1) && !lp.pelaajanLaivatUponnut(nimi2)) {
			ai.huijausAmmu(nimi2);
			ai.normiAmmu(nimi1);
		}

		kt = tilastot.getKaksinpelit().getKokonaistilastot();
		pkt = kt.getPelaajanKokonaistilastot(nimi1.getNimi());

		assertEquals("Pelaaja 1 ei voittanut 1 pelia.", 1, pkt.getVoitot());
		assertEquals("Pelaaja 1 ei havinnyt 1 pelia.", 1, pkt.getTappiot());
		assertEquals("Pelaaja 1 ei pelannut 2 pelia.", 2, pkt.getPelit());

		// kierros 3, peli keskeytetaan 10 kierroksen jalkeen
		lp.poistaKaikkiPelaajat();
		lp.uusiPelaaja(nimi1);
		lp.uusiPelaaja(nimi2);
		tilastot.uusiKaksinpeli(nimi1, nimi2, lp);
		asetaLaivat(ai, nimi1, nimi2);
		for (int i = 0; i < 10; i++) {
			ai.huijausAmmu(nimi2);
			ai.normiAmmu(nimi1);
		}

		kt = tilastot.getKaksinpelit().getKokonaistilastot();
		pkt = kt.getPelaajanKokonaistilastot(nimi1.getNimi());

		assertEquals("Pelaaja 1 ei pelannu 3 pelia.", 3, pkt.getPelit());
		assertEquals("Pelaaja 1 ei ampunut yhteensa 50 laukausta.", 50, pkt.getLaukaukset());

		Map<String, PelaajanKokonaistilastot>kokonaistilastot = kt.getKokonaistilastot();

		System.out.println("--- Kokonaistilastotest BEGIN ---");
		kokonaistilastot.forEach((nimi, pelaaja)->{
			System.out.println("nimi: " + pelaaja.getNimi());
			System.out.println("  laukaukset: " + pelaaja.getLaukaukset());
			System.out.println("  osumat: " + pelaaja.getOsumat());
			System.out.println("  tappiot: " + pelaaja.getTappiot());
			System.out.println("  voitot: " + pelaaja.getVoitot());
			System.out.println("  pelit: " + pelaaja.getPelit());
		});
		System.out.println("--- Kokonaistilastotest END ---");
	}


	/**
	 * Asettaa 5 4-ruudun pituista laivaa annetuille pelaajille.
	 *
	 * @param ai Tekoalyluokka joka asettaa laivat
	 * @param nimi1 Pelaaja 1
	 * @param nimi2 Pelaaja 2
	 */
	private void asetaLaivat(LaivanupotusAI ai, PelaajanNimi nimi1, PelaajanNimi nimi2) {
		boolean ins = false;

		for (int i = 0; i < 5; i++) {
			do {
				ins = ai.asetaLaiva(nimi1, 4);
			}
			while(!ins);
			do {
				ins = ai.asetaLaiva(nimi2, 4);
			}
			while(!ins);
		}
	}
}
