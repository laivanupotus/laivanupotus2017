package model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * JUnit testeja Laivanupotuspeli-mallin testausta varten.
 *
 * @author Antti Pasanen
 *
 */

@SuppressWarnings("unused")
public class LaivanupotusPeliTest {

	private static LaivanupotusPeli lp = new LaivanupotusPeli();
	private static PelaajanNimi nimi = new PelaajanNimi("apina");
	private static PelaajanNimi nimi2 = new PelaajanNimi("sika");


	/**
	 * Pelaajien poisto testin suorituksen jälkeen.
	 */
	@After
	public void tyhjennaPelaajat() {
		lp.poistaKaikkiPelaajat();
	}


	/**
	 * Uuden pelaajan lisaystesti.
	 */
	@Test
	public void testLisaaPelaaja() {
		boolean uusi1 = lp.uusiPelaaja(nimi);
		boolean uusi2 = lp.uusiPelaaja(nimi);
		assertTrue("pelaajan lisays", uusi1);
		assertFalse("pelaajan lisays toistamiseen", uusi2);
	}


	/**
	 * Testi pelaajan poistamista varten.
	 */
	@Test
	public void testPoistaPelaaja() {
		boolean lisays = lp.uusiPelaaja(nimi);
		boolean poisto = lp.poistaPelaaja(nimi);
		boolean poisto2 = lp.poistaPelaaja(nimi);
		assertTrue("pelaajan lisays", lisays);
		assertTrue("pelaajan poisto", poisto);
		assertFalse("pelaajan poisto toistamiseen", poisto2);
	}


	/**
	 * Testi uuden pelaajan lisaamista eri kartan koolla.
	 */
	@Test
	public void testLisaaPelaaja2() {
		boolean uusi1 = lp.uusiPelaaja(nimi, KoordinaattiPari.create(4, 5));
		boolean uusi2 = lp.uusiPelaaja(nimi, KoordinaattiPari.create(3, 5));
		assertTrue("pelaajan lisays", uusi1);
		assertFalse("pelaajan lisays toistamiseen", uusi2);
	}


	/**
	 * Testi pelaajan lisaamistä varten epakelvolla kartan koolla.
	 */
	@Test
	public void testLisaaPelaaja3() {
		boolean uusi1 = lp.uusiPelaaja(nimi, KoordinaattiPari.create(-1, -1));
		assertFalse("pelaajan lisays huonot koordinaatit", uusi1);
	}

	/**
	 * Testi laivan lisaykselle pelaajalle.
	 */
	@Test
	public void testLisaaLaiva1() {
		lp.uusiPelaaja(nimi);
		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(2, 2),
				KoordinaattiPari.create(3, 3)};
		boolean lisays1 = lp.lisaaLaiva(nimi2, koordinaatit); // tuntematon pelaaja
		boolean lisays2 = lp.lisaaLaiva(nimi, koordinaatit);

		assertFalse("lisays olemattomalle pelaajalle", lisays1);
		assertTrue("Lisays", lisays2);
	}


	/**
	 * Testit laivan lisaamiselle erilaisilla koordinaateilla.
	 */
	@Test
	public void testLisaaLaiva2() {
		lp.uusiPelaaja(nimi);
		KoordinaattiPari[] koordinaatit1 = {
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(-1, 1),
		};
		KoordinaattiPari[] koordinaatit2 = {
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(11, 1),
		};
		KoordinaattiPari[] koordinaatit3 = {
				KoordinaattiPari.create(3, 1),
				KoordinaattiPari.create(3, 1),
		};
		KoordinaattiPari[] koordinaatit4 = {
		};
		boolean lisays1 = lp.lisaaLaiva(nimi, koordinaatit1); // huonot koordinaatit
		boolean lisays2 = lp.lisaaLaiva(nimi, koordinaatit2); // liian suuret koordinaatit
		boolean lisays3 = lp.lisaaLaiva(nimi, koordinaatit3); // sama koordinaatti
		boolean lisays4 = lp.lisaaLaiva(nimi, koordinaatit4); // ei koordinaatteja
		assertFalse("huonot koordinaatit", lisays1);
		assertFalse("liian suuret koordinaatit", lisays2);
		assertFalse("sama koordinaatti", lisays3);
		assertFalse("ei koordinaatteja", lisays3);
	}


	/**
	 * Testi kartanhakemista char taulukkona.
	 *
	 */
	@Test
	public void testKartta1() {
		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		lp.uusiPelaaja(nimi);
		char[][] kartta2 = lp.getPelaajanKartta(nimi, false);
		char[][] vertailuKartta2 = new char[10][10];

		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++) {
				vertailuKartta2[i][j] = '~';
			}

		int x = 3;
		int y = 5;

		lp.uusiPelaaja(nimi2, KoordinaattiPari.create(x, y));
		char[][] kartta3 = lp.getPelaajanKartta(nimi2, false);
		char[][] vertailuKartta3 = new char[y][x];

		for (int i = 0; i < y; i++)
			for (int j = 0; j < x; j++) {
				vertailuKartta3[i][j] = '~';
			}

		assertArrayEquals("kartta olemattomalta pelaajalta", null, kartta1);
		assertArrayEquals("default kartta", vertailuKartta2, kartta2);
		assertArrayEquals("custom kartta", vertailuKartta3, kartta3);
	}


	/**
	 * Visuaalinen testi laivan sijoittamiselle.
	 */
	@Test
	public void testKartta2() {
		lp.uusiPelaaja(nimi, KoordinaattiPari.create(4, 4));
		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(0, 0),
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(2, 2),
				KoordinaattiPari.create(3, 3)};
		lp.lisaaLaiva(nimi, koordinaatit);

		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}
	}


	/**
	 * Testit ampumista varten.
	 *
	 */
	@Test
	public void testAmmu() {
		lp.uusiPelaaja(nimi, KoordinaattiPari.create(4, 4));
		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(0, 0),
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(2, 2),
				KoordinaattiPari.create(3, 3)};
		lp.lisaaLaiva(nimi, koordinaatit);

		boolean ammu1 = lp.ammu(nimi, KoordinaattiPari.create(0, 0));
		boolean ammu2 = lp.ammu(nimi, KoordinaattiPari.create(1, 0));
		boolean ammu3 = lp.ammu(nimi, KoordinaattiPari.create(1, 0));
		boolean ammu4 = lp.ammu(nimi, KoordinaattiPari.create(5, 0));

		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}

		assertTrue("ampuminen laivaan", ammu1);
		assertTrue("ampuminen veteen", ammu2);
		assertFalse("ampuminen uudestaan samaan", ammu3);
		assertFalse("ampuminen ohi kartan", ammu4);
	}


	/**
	 * Testeja laivan upottamisen osalta.
	 *
	 */
	@Test
	public void testUpotus() {
		lp.uusiPelaaja(nimi, KoordinaattiPari.create(4, 4));

		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(0, 0),
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(2, 2),
				KoordinaattiPari.create(3, 3)};

		boolean uponnut1 = lp.pelaajanLaivatUponnut(nimi);

		lp.lisaaLaiva(nimi, koordinaatit);
		lp.ammu(nimi, koordinaatit[0]);
		lp.ammu(nimi, koordinaatit[1]);
		lp.ammu(nimi, koordinaatit[2]);

		boolean uponnut2 = lp.pelaajanLaivatUponnut(nimi);
		lp.ammu(nimi, koordinaatit[3]);
		boolean uponnut3 = lp.pelaajanLaivatUponnut(nimi);

		koordinaatit = new KoordinaattiPari[]{
				KoordinaattiPari.create(2, 0),
				KoordinaattiPari.create(3, 0)};
		lp.lisaaLaiva(nimi, koordinaatit);

		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}

		boolean uponnut4 = lp.pelaajanLaivatUponnut(nimi);

		assertFalse("ei laivoja laivastossa", uponnut1);
		assertFalse("laiva ei taysin ammuttu", uponnut2);
		assertTrue("uponnut", uponnut3);
		assertFalse("toine laiva ehja", uponnut4);
	}


	/**
	 * Validointi etta laiva on ruudussa lisayksen jalkeen.
	 */
	@Test
	public void onkoRuudussaLaivaTest() {
		lp.uusiPelaaja(nimi);

		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(0, 0),
				KoordinaattiPari.create(1, 1),
				KoordinaattiPari.create(2, 2),
				KoordinaattiPari.create(3, 3)};

		lp.lisaaLaiva(nimi, koordinaatit);

		assertTrue(lp.onkoRuudussaLaiva(nimi, koordinaatit[0]));
		assertTrue(lp.onkoRuudussaLaiva(nimi, koordinaatit[1]));
		assertTrue(lp.onkoRuudussaLaiva(nimi, koordinaatit[2]));
		assertTrue(lp.onkoRuudussaLaiva(nimi, koordinaatit[3]));
		assertFalse(lp.onkoRuudussaLaiva(nimi, KoordinaattiPari.create(1, 2)));

		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}
	}


	/**
	 * PelaajanNimi - luokan testit
	 */
	@Test
	public void pelaajanNimiTest() {

		PelaajanNimi nimi1 = new PelaajanNimi("keke");
		assertTrue(nimi1.equals(nimi1));
		assertFalse(nimi1.equals(null));

		PelaajanNimi nimi2 = new PelaajanNimi("keke");
		assertTrue(nimi1.equals(nimi2));
		assertTrue(nimi1.hashCode() == nimi2.hashCode());

		PelaajanNimi nimi3 = new PelaajanNimi("falseus");
		assertFalse(nimi1.equals(nimi3));
		assertFalse(nimi1.hashCode() == nimi3.hashCode());

		PelaajanNimi nimi4 = new PelaajanNimi(null);
		assertFalse(nimi1.equals(nimi4));
		assertTrue(nimi4.equals(nimi4));
		assertFalse(nimi4.equals(null));
		assertFalse(nimi4.hashCode() == nimi1.hashCode());

		nimi4.setNimi("keke");
		assertTrue(nimi1.equals(nimi4));
		assertTrue(nimi1.hashCode() == nimi4.hashCode());
	}
}
