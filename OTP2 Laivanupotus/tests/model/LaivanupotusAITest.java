package model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * Testeja laivanupotustekoalya varten.
 *
 * @author Antti Pasanen
 *
 */

public class LaivanupotusAITest {

	private static LaivanupotusPeli lp = new LaivanupotusPeli();
	private static LaivanupotusAI ai = new LaivanupotusAI(lp);
	private static PelaajanNimi nimi = new PelaajanNimi("apina");

	/**
	 * Luodaan pelaaja ennen testia.
	 */
	@Before
	public void setupTests() {
		lp.uusiPelaaja(nimi);
	}

	/**
	 * Poistetaan pelaaja testin jalkeen.
	 */
	@After
	public void resetTests() {
		lp.poistaKaikkiPelaajat();
	}


	/**
	 * Testi tekoalyn ampumista varten.
	 */
	@Test
	public void laivanupotusAILaukausTest() {
		// kartta ennen ampumista
		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		ai.ammu(nimi);

		// laukauksen koordinaatit
		KoordinaattiPari laukaus = ai.getLaukaus();

		// kartta ampumisen jalkeen
		char[][] kartta2 = lp.getPelaajanKartta(nimi, false);

		boolean laukausTesti = false;

		// ammutulla kartalla laukauksen merkki ampumisen kohdalla
		if (kartta2[laukaus.getY()][laukaus.getX()] == 'O')
			laukausTesti = true;

		boolean eiAmmuttu = false;

		// ampumattomalla kartalla ampumisen kohdalla vetta
		if (kartta1[laukaus.getY()][laukaus.getX()] == '~')
			eiAmmuttu = true;

		assertTrue("laukaustesti", laukausTesti);
		assertTrue("laukaustesti2", eiAmmuttu);
	}


	/**
	 * Testi alykkaamman ampumisen osalta.
	 */
	@Test
	public void laivanupotusAILaukausTest2() {
		KoordinaattiPari[] koordinaatit = {
				KoordinaattiPari.create(2, 1),
				KoordinaattiPari.create(3, 1),
				KoordinaattiPari.create(4, 1)
		};

		lp.lisaaLaiva(nimi, koordinaatit);

		// ammutaan 1 osuma, sitten ammutaan osuman ymparille

		lp.ammu(nimi, koordinaatit[0]);
		ai.smartAmmu(nimi);
		ai.smartAmmu(nimi);
		ai.smartAmmu(nimi);
		ai.smartAmmu(nimi);

		char[][] kartta2 = lp.getPelaajanKartta(nimi, false);

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.print(kartta2[i][j]);
			}
			System.out.print("\n");
		}

		boolean laukausTesti1 = false;
		boolean laukausTesti2 = false;
		boolean laukausTesti3 = false;
		boolean laukausTesti4 = false;

		// x = 2, y = 1
		if (kartta2[1][1] == 'O')
			laukausTesti1 = true;
		if (kartta2[1][3] == 'X')
			laukausTesti2 = true;
		if (kartta2[0][2] == 'O')
			laukausTesti3 = true;
		if (kartta2[2][2] == 'O')
			laukausTesti4 = true;

		assertTrue("testi1", laukausTesti1);
		assertTrue("testi2", laukausTesti2);
		assertTrue("testi3", laukausTesti3);
		assertTrue("testi4", laukausTesti4);
	}


	/**
	 * Testi huijaavan ampumisen osalta.
	 */
	@Test
	public void laivanupotusAILaukausTest3() {

		KoordinaattiPari[] koordinaatit = {
			KoordinaattiPari.create(1, 4)
		};

		ai.superAmmu(nimi);
		lp.lisaaLaiva(nimi, koordinaatit);
		ai.superAmmu(nimi);

		char[][] kartta2 = lp.getPelaajanKartta(nimi, false);

		boolean osuma = false;

		if (kartta2[4][1] == 'X')
			osuma = true;

		assertTrue("osui ekalla", osuma);
	}


	/**
	 * Visuaalinen testi laivojen lisaysta varten.
	 */
	@Ignore
	@Test
	public void laivanupotusAILaivanLisaysTestB() {

		char[][] kartta1 = lp.getPelaajanKartta(nimi, false);

		ai.asetaLaiva(nimi, 1);
		ai.asetaLaiva(nimi, 2);
		ai.asetaLaiva(nimi, 3);
		ai.asetaLaiva(nimi, 4);
		ai.asetaLaiva(nimi, 5);
		ai.asetaLaiva(nimi, 6);
		ai.asetaLaiva(nimi, 7);
		ai.asetaLaiva(nimi, 8);

		char[][] kartta2 = lp.getPelaajanKartta(nimi, false);

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.print("\n");
		}
		System.out.println("\n");

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.print(kartta2[i][j]);
			}
			System.out.print("\n");
		}
	}

	/**
	 * Laivan automaagisen lisaamisen testi.
	 */
	@Test
	public void laivanupotusAILaivanLisaysTest() {

		ai.asetaLaiva(nimi, 3);
		KoordinaattiPari[] koordinaatit = ai.getViimeisinAsennettuLaiva();
		char[][] kartta2 = lp.getPelaajanKartta(nimi, false);

		boolean laiva = false;
		boolean laivaVaarissaRuuduissa = false;
		boolean laivaOikeissaRuuduissa = true;

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				for (int k = 0; k < 3; k++) {
					if ((koordinaatit[k].getX() == j) && (koordinaatit[k].getY() == i)) {
						if (kartta2[i][j] != Ruudukko.SYMBOL_LAIVA) {
							laivaOikeissaRuuduissa = false;
						}
						laiva = true;
					}
				}
				if (laiva)
					laiva = false;
				else
					if (kartta2[i][j] != Ruudukko.SYMBOL_MERI)
						laivaVaarissaRuuduissa = true;
			}
		}

		assertFalse("1", laivaVaarissaRuuduissa);
		assertTrue("2", laivaOikeissaRuuduissa);
	}
}
