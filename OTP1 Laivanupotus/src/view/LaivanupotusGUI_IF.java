package view;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.FontWeight;

public interface LaivanupotusGUI_IF {

	public abstract Label setLabel(String labelText, String font, int size, FontWeight weight);
	public abstract Label setLabel(String labelText, String font, int size);
	public abstract void setButton(Button button, String buttonText, String font, String bgColor, String textColor, int textSize, String borderColor);
	public abstract Node mpgamePrepare();
	public abstract Node mpgamePrepareBottom();
	public abstract void laivaanOsui(Button gb, int xCoord, int yCoord);
	public abstract void laivaanEiOsunut(Button gb, int xCoord, int yCoord);
	public abstract void laivastoUpposi();
	public abstract void setFlagIsTurn(boolean flagIsTurn);

}
