package view;

import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import controller.Controller;
import controller.Controller_IF;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Tilastot;
import model.Tilastot_IF;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class LaivanupotusGUI extends Application implements LaivanupotusGUI_IF {

	private static final int gridSize = 10;
	private static final double gridSizePixel = 32;
	private static final int shipSizeDefault = 2;
	private static final int shipCountDefault = 24;
	private static final String AI = "AI";
	private static final String OSUI = "OSUI";
	private static final String EIOSUNUT = "EIOSUNUT";
	private static final String UPPOSI = "UPPOSI";
	private static final String AIVAIKEUSDEFAULT = "AI: helppo";
	private static final String OSUMA = "osuma";
	private static final String HUTI = "huti";
	private static final String VOITTO = "voitto";
	private static final String HAVIO = "häviö";

	private static String pelaajanNimi;

	private Stage primaryStage;

	private Controller_IF ctrl;

	/*
	 * Panes
	 */
	private StackPane root;

	private BorderPane bpMain;
	private BorderPane bpNewGame;
	private BorderPane bpSPGame;
	private BorderPane bpMPGamePrepare;

	private HBox hboxSPgameBottom;
	private HBox hboxNewGameBottom;

	private VBox vbMPGamePrepare;

	/*
	 * Buttons
	 */
	private Button buttonNewGame;
	private Button buttonSettings;
	private Button buttonInstructions;
	private Button buttonQuit;
	private Button buttonSinglePG;
	private Button buttonMultiPG;
	private Button buttonToMainMenu;
	private Button buttonCreateNewGame;
	private Button buttonConnectToGame;
	private Button tilastot;
	private Button luoUusiPelaaja;
	private Button poistaPelaaja;

	/*
	 * ChoiceBoxes
	 */
	private ChoiceBox<String> cbShipSize;

	/*
	 * RadioButtons
	 */
	private RadioButton rbAIvaikeus1;
	private RadioButton rbAIvaikeus2;
	private RadioButton rbAIvaikeus3;
	private RadioButton rbAIvaikeus4;

	/*
	 * TextArea
	 */
	private TextArea tfStatus;

	/*
	 * Labels
	 */
	private Label labelShipsAvailable;
	private Label labelAIvaikeus;
	private Label labelOsumat1;
	private Label labelOsumat2;

	/*
	 * Values
	 */
	private int mainWindowMinWidth = 960;
	private int mainWindowMaxWidth = 1920;
	private int mainWindowDefaultWidth = 960;
	private int mainWindowMinHeight = 540;
	private int mainWindowMaxHeight = 1080;
	private int mainWindowDefaultHeight = 540;

	private int subWindowMinWidth = 360;
	private int subWindowMaxWidth = 960;
	private int subWindowDefaultWidth = 344;
	private int subWindowMinHeight = 240;
	private int subWindowMaxHeight = 540;
	private int subWindowDefaultHeight = 480;

	// Yksittäisen laivan koko
	private int shipSize = shipSizeDefault;
	// Pelierän alussa käytettävissä olevien ruutujen määrä laivastolle
	private int shipCount = shipCountDefault;
	private int shipXCoordold = 0;
	private int shipYCoordold = 0;

	private boolean flagAddingShips;
	private boolean flagShipInProgress;
	private boolean flagPlay;
	private boolean flagMulti;
	private boolean flagIsTurn;
	private boolean flagPlayerWins;
	private Stage stageSettings;
	private Label labelOsumatA;
	private Label labelOsumatB;

	@Override
	public void init() {
		ctrl = new Controller(this, AI);
		// ctrl.uusiPelaaja(pelaajanNimi, AI);
		flagAddingShips = true;
		flagShipInProgress = false;
		flagMulti = false;
		flagIsTurn = false;
		createButtons();
		buttonActions();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			this.primaryStage = primaryStage;
			this.primaryStage.setTitle(ctrl.getText('l', 0) + ", IP: " + InetAddress.getLocalHost());
			this.primaryStage.setMinWidth(mainWindowMinWidth);
			this.primaryStage.setMinHeight(mainWindowMinHeight);
			this.primaryStage.setMaxWidth(mainWindowMaxWidth);
			this.primaryStage.setMaxHeight(mainWindowMaxHeight);
			createPanes();
			Scene scene = new Scene(root, mainWindowDefaultWidth, mainWindowDefaultHeight);
			this.primaryStage.setScene(scene);
			this.primaryStage.show();
			this.primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent event) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							ctrl.closeSessionFactory();
							if (stageSettings != null)
								stageSettings.close();
							Platform.exit();
						}
					});
				}
			});
			postStartActions();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * start() metodin jälkitoimet suoritetaan täällä.
	 */
	private void postStartActions() {
		Region region = (Region) tfStatus.lookup(".content");
		region.setBackground(
				new Background(new BackgroundFill(Color.rgb(192, 192, 254, 0.5), CornerRadii.EMPTY, Insets.EMPTY)));
		labelAIvaikeus = setLabel("AI: " + rbAIvaikeus1.getText(), "Arial", 30);
		ctrl.setAIvaikeus(1);
	}

	/**
	 * Luo Pane -nodet ja aseta niille alinodet.
	 */
	private void createPanes() {
		root = new StackPane();

		// Main menu
		bpMain = new BorderPane();
		bpMain.setTop(mainTop());
		bpMain.setCenter(mainCenter());
		bpMain.setBottom(mainBottom());
		bpMain.setLeft(mainLeft());
		bpMain.setRight(mainRight());
		bpMain.setStyle(ctrl.getText('s', 0));

		// New Game
		bpNewGame = new BorderPane();
		bpNewGame.setTop(newgameTop());
		bpNewGame.setCenter(newgameCenter());
		bpNewGame.setBottom(newgameBottom());
		bpNewGame.setLeft(newgameLeft());
		bpNewGame.setRight(newgameRight());
		bpNewGame.setStyle(ctrl.getText('s', 1));

		// SinglePlayer Game
		bpSPGame = new BorderPane();
		bpSPGame.setTop(spgameTop());
		bpSPGame.setCenter(spgameCenter());
		bpSPGame.setBottom(spgameBottom());
		bpSPGame.setLeft(spgameLeft());
		bpSPGame.setRight(spgameRight());
		bpSPGame.setStyle(ctrl.getText('s', 1));

		// Multiplayer Game - Preparation Screen
		bpMPGamePrepare = new BorderPane();
		bpMPGamePrepare.setCenter(mpgamePrepare());
		bpMPGamePrepare.setBottom(mpgamePrepareBottom());
		bpMPGamePrepare.setStyle(ctrl.getText('s', 1));

		root.getChildren().addAll(bpMPGamePrepare, bpSPGame, bpNewGame, bpMain);
	}

	/*
	 * Aseta alanodet MAIN BORDER PANEn sisään Metodit mainTop(), mainCenter(),
	 * mainBottom(), mainLeft(), mainRight()
	 */

	/**
	 * Asettaa sisällön mainTop() haaraan.
	 *
	 * @return Palauttaa mainTop() haaraan sisältämän hbox-noden.
	 */
	private Node mainTop() {
		HBox hbox = new HBox();
		hbox.setAlignment(Pos.TOP_LEFT);
		hbox.setSpacing(25);
		hbox.setPadding(new Insets(20, 25, 20, 25));
		hbox.getChildren().addAll(setLabel(ctrl.getText('l', 1), "Arial", 30));
		return hbox;
	}

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node mainCenter() {
		return null;
	}

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node mainBottom() {
		return null;
	}

	/**
	 * Asettaa sisällön mainLeft() haaraan.
	 *
	 * @return Palauttaa mainLeft() haaraan sisältämän vbox-noden.
	 */
	private Node mainLeft() {
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_LEFT);
		vbox.setSpacing(25);
		vbox.setPadding(new Insets(20, 25, 20, 25));
		vbox.getChildren().addAll(buttonNewGame, buttonSettings, buttonInstructions, buttonQuit);
		return vbox;
	}

	/**
	 * Asettaa sisällön mainRight() haaraan.
	 *
	 * @return Palauttaa mainRight() haaraan sisältämän vbox-noden.
	 */
	private Node mainRight() {
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(20, 25, 20, 25));
		vbox.setSpacing(25);
		vbox.getChildren().addAll(tilastot, luoUusiPelaaja, poistaPelaaja);
		return vbox;
	}

	/*
	 * Aseta alanodet NEW GAME BORDER PANEn sisään Metodit newgameTop(),
	 * newgameCenter(), newgameBottom(), newgameLeft(), newgameRight()
	 */

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node newgameTop() {
		return null;
	}

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node newgameCenter() {
		return null;
	}

	/**
	 * Asettaa sisällön newgameBottom() haaraan.
	 *
	 * @return Palauttaa newgameBottom() haaraan sisältämän hbox-noden.
	 */
	private Node newgameBottom() {
		hboxNewGameBottom = new HBox();
		hboxNewGameBottom.setAlignment(Pos.BOTTOM_LEFT);
		hboxNewGameBottom.setSpacing(25);
		hboxNewGameBottom.setPadding(new Insets(20, 25, 20, 25));
		hboxNewGameBottom.getChildren().addAll(buttonToMainMenu);
		return hboxNewGameBottom;
	}

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node newgameLeft() {
		return null;
	}

	/**
	 * Asettaa sisällön newgameRight() haaraan.
	 *
	 * @return Palauttaa newgameRight() haaraan sisältämän vbox-noden.
	 */
	private Node newgameRight() {
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_RIGHT);
		vbox.setSpacing(25);
		vbox.setPadding(new Insets(20, 25, 20, 25));
		vbox.getChildren().addAll(buttonSinglePG, buttonMultiPG);
		return vbox;
	}

	/*
	 * Aseta alanodet SINGLE PLAYER GAME BORDER PANEn sisään Metodit
	 * spgameTop(), spgameCenter(), spgameBottom(), spgameLeft(), spgameRight()
	 */

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node spgameTop() {
		return null;
	}

	/**
	 * Asettaa sisällön spgameCenter() haaraan.
	 *
	 * @return Palauttaa spgameCenter() haaraan sisältämän hbox-noden.
	 */
	private Node spgameCenter() {
		VBox vbox1 = new VBox();
		VBox vbox2 = new VBox();
		HBox hbox = new HBox();
		hbox.setAlignment(Pos.CENTER);

		GridPane grid1 = new GridPane(); // Pelaajan ruudukko
		GridPane grid2 = new GridPane(); // Vastapelaajan ruudukko

		grid1.setPadding(new Insets(32, 32, 32, 32));
		grid1.setGridLinesVisible(true);
		grid2.setPadding(new Insets(32, 32, 32, 32));
		grid2.setGridLinesVisible(true);

		// Buttonien luominen ruudukkoon
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				grid1.add(gridButton(new Button(), i, j, 'p'), i, j);
			}
		}
		// Buttonien luominen ruudukkoon
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				grid2.add(gridButton(new Button(), i, j, 'o'), i, j);
			}
		}

		labelOsumat1 = setLabel("0", "Arial", 36);
		labelOsumat2 = setLabel("0", "Arial", 36);
		labelOsumatA = setLabel(" / " + ctrl.getPelaajanLaivojenYhteiskoko(pelaajanNimi) + " osuttu", "Arial", 36);
		labelOsumatB = setLabel(" / " + ctrl.getPelaajanLaivojenYhteiskoko(AI) + " osuttu", "Arial", 36);

		HBox hboxLabel1 = new HBox();
		hboxLabel1.setSpacing(10);
		hboxLabel1.getChildren().addAll(labelOsumat1, labelOsumatA);
		HBox hboxLabel2 = new HBox();
		hboxLabel2.setSpacing(10);
		hboxLabel2.getChildren().addAll(labelOsumat2, labelOsumatB);

		hboxLabel1.setAlignment(Pos.TOP_CENTER);
		hboxLabel2.setAlignment(Pos.TOP_CENTER);
		vbox1.getChildren().addAll(grid1, hboxLabel1);
		vbox2.getChildren().addAll(grid2, hboxLabel2);
		hbox.getChildren().addAll(vbox1, vbox2);
		return hbox;
	}

	/**
	 * Pelialueen buttoneiden toiminnallisuuden kuvaaminen.
	 *
	 * @param b
	 *            Pelialueen gridin yksittäinen painike.
	 * @param xCoord
	 *            Valitun painikkeen x-koordinaatti.
	 * @param yCoord
	 *            Valitun painikkeen y-koordinaatti.
	 * @param c
	 *            Valittu ruudukko, 'p' (player) tai 'o' (opponent).
	 * @return Palauttaa nappulan itsensä.
	 */
	private Button gridButton(Button b, int xCoord, int yCoord, char c) {
		Button gb = b;
		gb.setStyle("-fx-background-color: rgba(145,200,255,0.5);");
		gb.setMinHeight(gridSizePixel);
		gb.setMaxHeight(gridSizePixel);
		gb.setMinWidth(gridSizePixel);
		gb.setMaxWidth(gridSizePixel);
		gb.setId(yCoord + "" + xCoord);
		gb.setOpacity(0.7);
		ctrl.setHashButton(gb, xCoord, yCoord, c);
		// Yksittäisen buttonin toiminta ruudukossa:
		gb.setOnAction((ActionEvent event) -> {
			if (flagAddingShips && shipCount > 0) {
				int x1 = xCoord - shipSize + 1;
				int x2 = xCoord + shipSize - 1;
				int y1 = yCoord - shipSize + 1;
				int y2 = yCoord + shipSize - 1;
				// Laivojen asettaminen vierekkäin
				if (!flagShipInProgress && !ctrl.onkoRuudussaLaiva(pelaajanNimi, xCoord, yCoord)) {
					// Ensimmäisen laivaruudun asetus
					flagShipInProgress = true;
					gb.setStyle("-fx-background-color: white;");
					cbShipSize.setDisable(true);
					shipXCoordold = xCoord;
					shipYCoordold = yCoord;
				} else {
					if (shipXCoordold == xCoord && shipYCoordold == yCoord) {
						gb.setStyle("-fx-background-color: rgba(145,200,255,0.5);");
						cbShipSize.setDisable(false);
						flagShipInProgress = false;
						/*
						 * Alla oleva ehto tarkistaa onko laivoja asetettu
						 * aluelle, jolle uutta laivaa yritetään asettaa,
						 * lisäksi se tarkistaa että laivan lopetusruutua
						 * yritetään asettaa laivan pituuden etäisyydelle
						 * aloitusruudusta.
						 */
					} else if (!shipsOnArea(shipSize, shipXCoordold, xCoord, shipYCoordold, yCoord)
							&& (shipCount - shipSize) >= 0 && (shipXCoordold == xCoord || shipYCoordold == yCoord)
							&& ((Math.abs(shipXCoordold - xCoord) == (shipSize - 1))
									|| Math.abs(shipYCoordold - yCoord) == (shipSize - 1))) {
						boolean laivaLisätty = ctrl.lisaaLaiva(pelaajanNimi, shipSize, shipXCoordold, shipYCoordold,
								xCoord, yCoord);
						if (laivaLisätty) {
							tfStatus.setText(ctrl.getText('t', 11) + "(" + shipXCoordold + "," + shipYCoordold + ")("
									+ xCoord + "," + yCoord + ")," + ctrl.getText('t', 12)
									+ (shipCountDefault - shipCount + shipSize) + " \n " + tfStatus.getText());
						}
						// X-akselin suunnassa
						if (shipYCoordold == yCoord && (shipXCoordold == x1 || shipXCoordold == x2)
								&& (x1 >= 0 || x2 >= 0) && (x1 <= (gridSize - 1) || x2 <= (gridSize - 1))) {
							// Vasemmalta oikealle
							if (shipXCoordold < xCoord) {
								for (int k = shipXCoordold; k < (shipXCoordold + shipSize); k++) {
									setShip(k, yCoord, c);
								}
								// Oikealta vasemmalle
							} else if (shipXCoordold > xCoord) {
								for (int k = shipXCoordold; k > (shipXCoordold - shipSize); k--) {
									setShip(k, yCoord, c);
								}
							}
							flagShipInProgress = false;
							cbShipSize.setDisable(false);
							shipCount = shipCount - shipSize;
							// Y-akselin suunnassa
						} else if (shipXCoordold == xCoord && (shipYCoordold == y1 || shipYCoordold == y2)
								&& (y1 >= 0 || y2 >= 0) && (y1 <= (gridSize - 1) || y2 <= (gridSize - 1))) {
							// Ylhäältä alas
							if (shipYCoordold < yCoord) {
								for (int k = shipYCoordold; k < (shipYCoordold + shipSize); k++) {
									setShip(xCoord, k, c);
								}
								// Alhaalta yläs
							} else if (shipYCoordold > yCoord) {
								for (int k = shipYCoordold; k > (shipYCoordold - shipSize); k--) {
									setShip(xCoord, k, c);
								}
							}
							flagShipInProgress = false;
							cbShipSize.setDisable(false);
							shipCount = shipCount - shipSize;
						}
					}
					labelShipsAvailable.setText(String.valueOf(shipCount));
				}
			}
			if (!flagPlay && shipCount < shipSizeDefault) {
				labelOsumatA.setText(" / " + ctrl.getPelaajanLaivojenYhteiskoko(pelaajanNimi) + " osuttu");
				tfStatus.setText(ctrl.getText('t', 13) + "\n " + tfStatus.getText());
				flagAddingShips = false;
				cbShipSize.setDisable(true);
				buttonToMainMenu.setDisable(false);
				flagIsTurn = true;
				reverseGridButtonsDisabledState('p');
				reverseGridButtonsDisabledState('o');
				flagPlay = true;
				alertLaivatLisätty();

			} else if (flagPlay) {
				// SINGLE PLAYER
				if (!flagMulti && ctrl.ammu(AI, xCoord, yCoord)) {
					// Pelaajan vuoro
					if (ctrl.onkoRuudussaLaiva(AI, xCoord, yCoord)) {
						ctrl.setTilastot(OSUMA, pelaajanNimi);
						laivaanOsui(gb, xCoord, yCoord);
						labelOsumat2.setText(String.valueOf(Integer.valueOf(labelOsumat2.getText()) + 1));
						if (ctrl.pelaajanLaivatUponnut(AI)) {
							ctrl.setTilastot(VOITTO, pelaajanNimi);
							pelieräpäättyi();
							laivastoUpposi();
							flagPlay = false;
						}
					} else {
						ctrl.setTilastot(HUTI, pelaajanNimi);
						laivaanEiOsunut(gb, xCoord, yCoord);
					}
					// Tekoälyn vuoro
					if (!flagPlayerWins && ctrl.ammu_AI(pelaajanNimi)) {
						int aix = ctrl.aiCoordX();
						int aiy = ctrl.aiCoordY();
						Button pb = ctrl.getHashButton(Integer.valueOf(aiy + "" + aix), 'p');
						if (ctrl.onkoRuudussaLaiva(pelaajanNimi, aix, aiy)) {
							tfStatus.setText(ctrl.getText('t', 14) + "(" + aix + "," + aiy + ")\n " + tfStatus.getText());
							labelOsumat1.setText(String.valueOf(Integer.valueOf(labelOsumat1.getText()) + 1));
							pb.setStyle("-fx-background-color: red;");
							pb.setText("X");
							if (ctrl.pelaajanLaivatUponnut(pelaajanNimi)) {

								ctrl.setTilastot(HAVIO, pelaajanNimi);
								tfStatus.setText(ctrl.getText('t', 15) + "\n " + tfStatus.getText());
								pelieräpäättyi();
								alertHävisit();
								reverseGridButtonsDisabledState('o');
								flagPlay = false;
							}
						} else {
							tfStatus.setText(ctrl.getText('t', 16) + "(" + aix + "," + aiy + ")\n " + tfStatus.getText());
							pb.setStyle("-fx-background-color: black;");
						}
					}
				} else if (!flagMulti && !ctrl.ammu(AI, xCoord, yCoord)) {
					tfStatus.setText(ctrl.getText('t', 17) + "(" + xCoord + "," + yCoord + ")\n " + tfStatus.getText());
					alertOnAmmuttu();
				}
				// MULTIPLAYER
				// Oma vuoro:
				if (flagMulti && flagIsTurn) {
					// TODO
					String osuiko = ctrl.sendDataToClient(xCoord + "" + yCoord);
					if (osuiko.equals(OSUI)) {
						laivaanOsui(gb, xCoord, yCoord);
					} else if (osuiko.equals(EIOSUNUT)) {
						laivaanEiOsunut(gb, xCoord, yCoord);
					} else if (osuiko.equals(UPPOSI)) {
						laivaanOsui(gb, xCoord, yCoord);
						laivastoUpposi();
					}
					flagIsTurn = false;
					// Vastustajan vuoro:
				} else if (flagMulti && !flagIsTurn) {
					// TODO
					alertVastapelaajanVuoro();
				}
			}
		});
		gb.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				gb.setOpacity(1);
			}
		});
		gb.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				gb.setOpacity(0.7);
			}
		});
		return gb;
	}

	/**
	 * @param gb
	 *            Button, johon osuttiin.
	 * @param xCoord
	 *            Buttonin x-koordinaatti.
	 * @param yCoord
	 *            Buttonin y-koordinaatti.
	 */
	@Override
	public void laivaanOsui(Button gb, int xCoord, int yCoord) {
		tfStatus.setText(ctrl.getText('t', 18) + "(" + xCoord + "," + yCoord + ")\n " + tfStatus.getText());
		gb.setStyle("-fx-background-color: green;");
		gb.setText("X");
	}

	/**
	 * Toimenpiteet kun laivasto uppoaa.
	 */
	@Override
	public void laivastoUpposi() {
		flagPlayerWins = true;
		tfStatus.setText(ctrl.getText('t', 19) + "\n " + tfStatus.getText());
		alertVoitit();
		reverseGridButtonsDisabledState('o');
	}

	/**
	 * Toimenpiteet kun laivaan ei osuttu.
	 */
	@Override
	public void laivaanEiOsunut(Button gb, int xCoord, int yCoord) {
		tfStatus.setText(ctrl.getText('t', 20) + "(" + xCoord + "," + yCoord + ")\n " + tfStatus.getText());
		gb.setStyle("-fx-background-color: white;");
	}

	/**
	 * Kääntää kaikkien buttoneiden disabled-tilan päinvastaiseksi.
	 *
	 * @param c
	 *            Määrittää kumpaa ruudukkoa muutokset koskevat.
	 */
	private void reverseGridButtonsDisabledState(char c) {
		Map<Integer, Button> hmb = ctrl.getHashMap(c);
		for (Entry<Integer, Button> entry : hmb.entrySet()) {
			if (entry.getValue().isDisabled()) {
				entry.getValue().setDisable(false);
				entry.getValue().setOpacity(0.9);
			} else {
				entry.getValue().setDisable(true);
				entry.getValue().setOpacity(0.7);
			}
		}
	}

	/**
	 * Pelierän päättymisen toimenpiteet.
	 */
	private void pelieräpäättyi() {
		if (ctrl.updateTilastotDAO(pelaajanNimi)) {
			alertPäivitysOnnistui();
		} else {
			alertPäivitysEpäonnistui();
		}
	}

	/**
	 *
	 * @param size
	 *            Valitun laivan koko.
	 * @param x1
	 *            Ensimmäiseksi valitun ruudun x-koordinaatti.
	 * @param x2
	 *            Ensimmäiseksi valitun ruudun y-koordinaatti.
	 * @param y1
	 *            Toiseksi valitun ruudun x-koordinaatti.
	 * @param y2
	 *            Toiseksi valitun ruudun y-koordinaatti.
	 * @return Palauttaa tiedon siitä yritetäänkö uutta laivaa asettaa aiemmin
	 *         asetetun laivan päälle.
	 */
	private boolean shipsOnArea(int size, int x1, int x2, int y1, int y2) {
		boolean foundShip = false;
		if (x1 > x2 || y1 > y2) {
			int tmp = x1;
			x1 = x2;
			x2 = tmp;
			tmp = y1;
			y1 = y2;
			y2 = tmp;
		}
		int y = y1;
		int x = x1;
		for (int i = 0; i < size; i++) {
			if (x1 == x2) {
				if (!foundShip)
					foundShip = ctrl.onkoRuudussaLaiva(pelaajanNimi, x2, y);
				y++;
			} else if (y1 == y2) {
				if (!foundShip)
					foundShip = ctrl.onkoRuudussaLaiva(pelaajanNimi, x, y2);
				x++;
			}
		}
		return foundShip;
	}

	/**
	 * Asettaa painetun nappulan tyylit.
	 *
	 * @param x
	 *            Ruudukon x-koordinaatti.
	 * @param y
	 *            Ruudukon y-koordinaatti.
	 * @param c
	 *            Ruudukon tunnus.
	 */
	private void setShip(int x, int y, char c) {
		ctrl.getHashButton(Integer.valueOf(y + "" + x), c).setText(String.valueOf(shipSize));
		ctrl.getHashButton(Integer.valueOf(y + "" + x), c).setStyle("-fx-background-color: white;");
	}

	/**
	 * Asettaa sisällön spgameBottom() haaraan.
	 *
	 * @return Palauttaa spgameBottom() haaraan sisältämän hbox-noden.
	 */
	private Node spgameBottom() {
		final int TEXTAREAWIDTH = 340;
		HBox hbox = new HBox();
		tfStatus = new TextArea();
		tfStatus.setMinWidth(TEXTAREAWIDTH);
		tfStatus.setMaxWidth(TEXTAREAWIDTH);
		tfStatus.setPrefRowCount(3);
		tfStatus.setEditable(false);
		hbox.getChildren().addAll(tfStatus);

		hboxSPgameBottom = new HBox();
		hboxSPgameBottom.setAlignment(Pos.BOTTOM_LEFT);
		hboxSPgameBottom.setSpacing(25);
		hboxSPgameBottom.setPadding(new Insets(20, 25, 20, 25));
		hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus);
		return hboxSPgameBottom;
	}

	/**
	 * Asettaa sisällön spgameLeft() haaraan.
	 *
	 * @return Palauttaa spgameLeft() haaraan sisältämän vbox-noden.
	 */
	private Node spgameLeft() {
		HBox hbox = new HBox();
		String[] shipSizeOptions = { "2", "3", "4", "5" };
		cbShipSize = new ChoiceBox<String>();
		cbShipSize.setItems(FXCollections.observableArrayList(shipSizeOptions));
		cbShipSize.setValue(String.valueOf(shipSizeDefault));
		cbShipSize.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> o, Object o1, Object o2) {
				shipSize = Integer.valueOf(o2.toString());
			}
		});

		hbox.getChildren().addAll(setLabel(ctrl.getText('l', 3), "Arial", 14, FontWeight.NORMAL), cbShipSize);

		labelShipsAvailable = setLabel(String.valueOf(shipCount), "Arial", 48);

		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_RIGHT);
		vbox.getChildren().addAll(hbox, setLabel(ctrl.getText('l', 6), "Arial", 14, FontWeight.NORMAL),
				labelShipsAvailable);
		return vbox;
	}

	/**
	 * Varalla oleva metodi
	 *
	 * @return Palauttaa noden.
	 */
	private Node spgameRight() {
		return null;
	}

	/**
	 * Asettaa sisällön mpgamePrepare() haaraan.
	 *
	 * @return Palauttaa mpgamePrepare() haaraan sisältämän vbox-noden.
	 */
	@Override
	public Node mpgamePrepare() {
		VBox vbMPGamePrepare = new VBox();
		vbMPGamePrepare.setAlignment(Pos.CENTER);
		vbMPGamePrepare.setSpacing(25);
		vbMPGamePrepare.setPadding(new Insets(20, 25, 20, 25));
		vbMPGamePrepare.getChildren().addAll(buttonCreateNewGame, buttonConnectToGame);
		return vbMPGamePrepare;
	}

	/**
	 * Asettaa sisällön mpgamePrepareBottom() haaraan.
	 *
	 * @return Palauttaa mpgamePrepareBottom() haaraan sisältämän vbox-noden.
	 */
	@Override
	public Node mpgamePrepareBottom() {
		vbMPGamePrepare = new VBox();
		vbMPGamePrepare.setAlignment(Pos.CENTER);
		vbMPGamePrepare.setSpacing(25);
		vbMPGamePrepare.setPadding(new Insets(20, 25, 20, 25));
		vbMPGamePrepare.getChildren().addAll(buttonToMainMenu);
		return vbMPGamePrepare;
	}

	// Kaikki pelin olennaiset alertit löytyvät alta:

	/**
	 * Ilmoitus kun pelaaja on lisännyt kaikki laivansa.
	 */
	private void alertLaivatLisätty() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 0));
		alert.setHeaderText(ctrl.getText('a', 1));
		alert.setContentText(ctrl.getText('a', 2));
		alert.show();
	}

	/**
	 * Ilmoitus kun pelaaja yrittää ampua samaan ruutuun toistamiseen.
	 */
	private void alertOnAmmuttu() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 3));
		alert.setHeaderText(ctrl.getText('a', 4));
		alert.setContentText(ctrl.getText('a', 5));
		alert.show();
	}

	/**
	 * Ilmoitus kun pelaaja on voittanut erän.
	 */
	private void alertVoitit() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 6));
		alert.setHeaderText(ctrl.getText('a', 7));
		alert.setContentText(ctrl.getText('a', 8));
		alert.show();
	}

	/**
	 * Ilmoitus kun pelaaja on hävinnyt erän.
	 */
	private void alertHävisit() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 9));
		alert.setHeaderText(ctrl.getText('a', 10));
		alert.setContentText(ctrl.getText('a', 11));
		alert.show();
	}

	/**
	 * Ilmoitus kun pelaaja on luovuttanut erän.
	 */
	private void alertLuovutit() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 12));
		alert.setHeaderText(ctrl.getText('a', 13));
		alert.setContentText(ctrl.getText('a', 11));
		alert.show();
	}

	/**
	 * Ilmoitus kun on vastapelaajan vuoro.
	 */
	private void alertVastapelaajanVuoro() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 14));
		alert.setHeaderText(ctrl.getText('a', 15));
		alert.setContentText(ctrl.getText('a', 16));
		alert.show();
	}

	/**
	 * Varoitus virheellisestä portista.
	 */
	private void alertLuoUusiPeliWARNING() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(ctrl.getText('a', 17));
		alert.setHeaderText(ctrl.getText('a', 18));
		alert.setContentText(ctrl.getText('a', 19));
		alert.show();
	}

	/**
	 * Varoitus tuntemattomasta virheestä.
	 */
	private void alertLiityPeliinWARNING() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(ctrl.getText('a', 20));
		alert.setHeaderText(ctrl.getText('a', 21));
		alert.setContentText(ctrl.getText('a', 19));
		alert.show();
	}

	/**
	 * Ilmoitus kun tietokannan päivitys onnistui.
	 */
	private void alertPäivitysOnnistui() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 22));
		alert.setHeaderText(ctrl.getText('a', 23));
		alert.setContentText(ctrl.getText('a', 24));
		alert.show();
	}

	/**
	 * Ilmoitus kun tietokannan päivitys epäonnistui.
	 */
	private void alertPäivitysEpäonnistui() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(ctrl.getText('a', 22));
		alert.setHeaderText(ctrl.getText('a', 25));
		alert.setContentText(ctrl.getText('a', 26));
		alert.show();
	}

	/**
	 * Luodaan nappulat ja asetetaan niiden arvot.
	 */
	private void createButtons() {
		// NEW BUTTONS
		buttonNewGame = new Button();
		buttonSettings = new Button();
		buttonInstructions = new Button();
		tilastot = new Button();
		luoUusiPelaaja = new Button();
		poistaPelaaja = new Button();
		buttonQuit = new Button();

		buttonSinglePG = new Button();
		buttonMultiPG = new Button();
		buttonToMainMenu = new Button();

		buttonCreateNewGame = new Button();
		buttonConnectToGame = new Button();

		// NEW RADIO BUTTONS
		rbAIvaikeus1 = new RadioButton("Helppo");
		rbAIvaikeus2 = new RadioButton("Normaali");
		rbAIvaikeus3 = new RadioButton("Vaikea");
		rbAIvaikeus4 = new RadioButton("Huijari");

		// SET BUTTON VALUES
		setButton(buttonNewGame, ctrl.getText('b', 0), "Arial", "#729fcf", "#000", 20, "black");
		setButton(buttonSettings, ctrl.getText('b', 1), "Arial", "#729fcf", "#000", 20, "black");
		setButton(buttonInstructions, ctrl.getText('b', 2), "Arial", "#729fcf", "#000", 20, "black");
		setButton(tilastot, ctrl.getText('b', 9), "Arial", "#729fcf", "#000", 20, "black");
		setButton(luoUusiPelaaja, ctrl.getText('b', 10), "Arial", "#729fcf", "#000", 16, "black");
		setButton(poistaPelaaja, ctrl.getText('b', 11), "Arial", "#729fcf", "#000", 16, "black");
		setButton(buttonQuit, ctrl.getText('b', 8), "Arial", "#729fcf", "#000", 20, "black");

		setButton(buttonSinglePG, ctrl.getText('b', 3), "Arial", "#729fcf", "#000", 20, "black");
		setButton(buttonMultiPG, ctrl.getText('b', 4), "Arial", "#729fcf", "#000", 20, "black");
		setButton(buttonToMainMenu, ctrl.getText('b', 5), "Arial", "#729fcf", "#000", 20, "black");

		setButton(buttonCreateNewGame, ctrl.getText('b', 6), "Arial", "#729fcf", "#000", 20, "black");
		setButton(buttonConnectToGame, ctrl.getText('b', 7), "Arial", "#729fcf", "#000", 20, "black");
	}

	/**
	 * Asetetaan luotujen nappuloiden toiminnallisuus.
	 */
	private void buttonActions() {
		// Uusi peli
		buttonNewGame.setOnAction((ActionEvent event) -> {
			// Tyhjennetään Pelaaja -luokan oliot
			ctrl.poistaKaikkiPelaajat();
			// Tarkistetaan onko Tilastot -luokan olioita olemassa
			if (ctrl.getTilastot().size() == 0) {
				// Pyydetään luomaaan uusi pelaaja
				Optional<String> r = uusiPelaaja();
				if (r.isPresent()) {
					// Jos pelaaja luotiin niin aloitetaan uuden pelin
					// alkutoimet
					uudenPelinAlkuToimet();
				}
			} else {
				// Jos Tilastot -luokan olioita on olemassa niin aloitetaan
				// uuden pelin alkutoimet
				if (pelaajanNimi == null) {
					pelaajanNimi = ctrl.getTilastot(0).getPelaajanNimi();
				}
				uudenPelinAlkuToimet();
			}
		});
		// Päävalikkoon
		buttonToMainMenu.setOnAction((ActionEvent event) -> {
			if (flagPlay) {
				// Pelaaja häviää, mikäli pelin aikana poistutaan pelistä
				ctrl.setTilastot(HAVIO, pelaajanNimi);
				pelieräpäättyi();
				alertLuovutit();
			}
			bpMain.toFront();
			flagMulti = false;
			if (!hboxNewGameBottom.getChildren().contains(buttonToMainMenu)) {
				hboxNewGameBottom.getChildren().addAll(buttonToMainMenu);
			}
		});
		// Yksinpeli
		buttonSinglePG.setOnAction((ActionEvent event) -> {
			bpSPGame.toFront();
			if (!hboxSPgameBottom.getChildren().contains(buttonToMainMenu)
					&& !hboxSPgameBottom.getChildren().contains(tfStatus)) {
				hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus, labelAIvaikeus);
			} else {
				hboxSPgameBottom.getChildren().clear();
				hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus, labelAIvaikeus);
			}
			buttonToMainMenu.setDisable(true);
			if (!flagAddingShips)
				buttonToMainMenu.setDisable(false);
		});
		// Moninpeli
		buttonMultiPG.setOnAction((ActionEvent event) -> {
			bpMPGamePrepare.toFront();
			flagMulti = true;
			if (!vbMPGamePrepare.getChildren().contains(buttonToMainMenu)) {
				vbMPGamePrepare.getChildren().addAll(buttonToMainMenu);
			}
		});
		// Moninpeli - Luo peli
		buttonCreateNewGame.setOnAction((ActionEvent event) -> {
			TextInputDialog tid = new TextInputDialog();
			tid.setTitle(ctrl.getText('t', 21));
			tid.setHeaderText(ctrl.getText('t', 22));
			tid.setContentText(ctrl.getText('t', 23));
			Optional<String> result = tid.showAndWait();
			result.ifPresent(portti -> {
				try {
					ctrl.createNewMP(InetAddress.getLocalHost().getHostAddress(), Integer.valueOf(portti));
					bpSPGame.toFront();
					if (!hboxSPgameBottom.getChildren().contains(buttonToMainMenu)
							&& !hboxSPgameBottom.getChildren().contains(tfStatus)) {
						hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus);
					} else {
						hboxSPgameBottom.getChildren().clear();
						hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus);
					}
					buttonToMainMenu.setDisable(true);
					if (!flagAddingShips)
						buttonToMainMenu.setDisable(false);
				} catch (NumberFormatException nfe) {
					alertLuoUusiPeliWARNING();
				} catch (UnknownHostException uhe) {
					System.out.println("\n\n! Encountered unknown host. More details:");
					uhe.printStackTrace();
				}
			});
		});
		// Moninpeli - Liity peliin
		buttonConnectToGame.setOnAction((ActionEvent event) -> {
			TextInputDialog tid = new TextInputDialog();
			tid.setTitle(ctrl.getText('t', 24));
			tid.setHeaderText(ctrl.getText('t', 25));
			tid.setContentText(ctrl.getText('t', 26));
			Optional<String> result = tid.showAndWait();
			result.ifPresent(ip -> {
				TextInputDialog tid2 = new TextInputDialog();
				tid2.setTitle(ctrl.getText('t', 24));
				tid2.setHeaderText(ctrl.getText('t', 27));
				tid2.setContentText(ctrl.getText('t', 23));
				Optional<String> result2 = tid2.showAndWait();
				result2.ifPresent(portti -> {
					try {
						ctrl.joinNewMP(String.valueOf(ip), Integer.valueOf(portti));
						bpSPGame.toFront();
						if (!hboxSPgameBottom.getChildren().contains(buttonToMainMenu)
								&& !hboxSPgameBottom.getChildren().contains(tfStatus)) {
							hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus);
						} else {
							hboxSPgameBottom.getChildren().clear();
							hboxSPgameBottom.getChildren().addAll(buttonToMainMenu, tfStatus);
						}
						buttonToMainMenu.setDisable(true);
						if (!flagAddingShips)
							buttonToMainMenu.setDisable(false);
					} catch (Exception e) {
						alertLiityPeliinWARNING();
					}
				});
			});
		});
		// Asetukset
		buttonSettings.setOnAction((ActionEvent event) -> {
			// Uuden ikkunan valmistelut
			stageSettings = new Stage();
			stageSettings.setMinWidth(360);
			stageSettings.setMinHeight(240);
			stageSettings.setTitle(ctrl.getText('t', 28));
			VBox vbSettingsRoot = new VBox();
			vbSettingsRoot.setAlignment(Pos.TOP_LEFT);
			vbSettingsRoot.setSpacing(5);
			vbSettingsRoot.setPadding(new Insets(15, 20, 15, 20));
			Scene scene = new Scene(vbSettingsRoot, 360, 240);
			stageSettings.setScene(scene);
			stageSettings.show();

			// Labels:
			Label labelVaikeusaste = new Label(ctrl.getText('l', 4));

			// HBox vaikeusasteelle
			HBox hboxAIvaikeus = new HBox();
			hboxAIvaikeus.setSpacing(25);
			ToggleGroup groupAIvaikeus = new ToggleGroup();
			rbAIvaikeus1.setToggleGroup(groupAIvaikeus);
			rbAIvaikeus2.setToggleGroup(groupAIvaikeus);
			rbAIvaikeus3.setToggleGroup(groupAIvaikeus);
			rbAIvaikeus4.setToggleGroup(groupAIvaikeus);
			hboxAIvaikeus.getChildren().addAll(rbAIvaikeus1, rbAIvaikeus2, rbAIvaikeus3, rbAIvaikeus4);

			// Valitaan se vaikeusaste, joka on sillä hetkellä AI:lla
			if (ctrl.getAIvaikeus() == 1) {
				rbAIvaikeus1.setSelected(true);
			} else if (ctrl.getAIvaikeus() == 2) {
				rbAIvaikeus2.setSelected(true);
			} else if (ctrl.getAIvaikeus() == 3) {
				rbAIvaikeus3.setSelected(true);
			} else if (ctrl.getAIvaikeus() == 4) {
				rbAIvaikeus4.setSelected(true);
			}

			// Kuuntelija radionappien ryhmälle
			groupAIvaikeus.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
				public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
					if (new_toggle == null)
						new_toggle = old_toggle;
					if (new_toggle.equals(rbAIvaikeus1)) {
						ctrl.setAIvaikeus(1);
						labelAIvaikeus.setText("AI: " + rbAIvaikeus1.getText());
					} else if (new_toggle.equals(rbAIvaikeus2)) {
						ctrl.setAIvaikeus(2);
						labelAIvaikeus.setText("AI: " + rbAIvaikeus2.getText());
					} else if (new_toggle.equals(rbAIvaikeus3)) {
						ctrl.setAIvaikeus(3);
						labelAIvaikeus.setText("AI: " + rbAIvaikeus3.getText());
					} else if (new_toggle.equals(rbAIvaikeus4)) {
						ctrl.setAIvaikeus(4);
						labelAIvaikeus.setText("AI: " + rbAIvaikeus4.getText());
					}
				}
			});

			// Asetetaan elementit ikkunaan
			vbSettingsRoot.getChildren().addAll(labelVaikeusaste, hboxAIvaikeus);
		});
		// Ohje
		buttonInstructions.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				final Stage stage = new Stage();
				stage.setTitle(ctrl.getText('b', 2));
				stage.setMinWidth(subWindowMinWidth);
				stage.setMinHeight(subWindowMinHeight);
				stage.setMaxWidth(subWindowMaxWidth);
				stage.setMaxHeight(subWindowMaxHeight);
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(primaryStage);

				VBox dialogVbox = new VBox(20);
				dialogVbox.setStyle(ctrl.getText('s', 0));
				dialogVbox.setSpacing(25);
				dialogVbox.setPadding(new Insets(20, 25, 20, 25));
				dialogVbox.getChildren().addAll(setLabel(ctrl.getText('l', 2), "Arial", 14, FontWeight.NORMAL));

				ScrollPane scrollPane = new ScrollPane(dialogVbox);
				scrollPane.setFitToWidth(true);

				Scene dialogScene = new Scene(scrollPane, subWindowDefaultWidth, subWindowDefaultHeight);
				stage.setScene(dialogScene);
				stage.show();
			}
		});
		// Tilastot
		tilastot.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				// Jos pelaajia ei ole
				if (ctrl.getTilastot().size() == 0) {
					// Pyydetään luomaan uusi pelaaja
					Optional<String> r = uusiPelaaja();
					// Jos uusi pelaaja luotiin
					if (r.isPresent()) {
						// niin näytetään tilastot
						näytäTilastot();
					}

					// Mikäli olemassa olevia pelaajia löytyy, niin näytetään
					// tilastot
				} else {
					näytäTilastot();
				}
			}
		});
		// Luo uusi pelaaja
		luoUusiPelaaja.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				// Pyydetään luomaan uusi pelaaja
				uusiPelaaja();
			}
		});
		// Poista pelaaja
		poistaPelaaja.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				// Pyydetään poistettavan pelaajan nimeä
				TextInputDialog dialog = new TextInputDialog(pelaajanNimi);
				dialog.setTitle(ctrl.getText('l', 1));
				dialog.setHeaderText(ctrl.getText('l', 5));
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					ctrl.poistaPelaaja(result.get());
				}
			}
		});
		// Lopeta
		buttonQuit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				ctrl.closeSessionFactory();
				if (stageSettings != null)
					stageSettings.close();
				Platform.exit();
			}
		});
	}

	/**
	 * Toimet, jotka suoritetaan kun uusi pelierä alkaa.
	 */
	private void uudenPelinAlkuToimet() {
		// Metodia kutsutaan täällä: buttonActions() >
		// buttonNewGame.setOnAction()
		ctrl.uusiPelaaja(pelaajanNimi, AI);
		bpNewGame.toFront();
		ctrl.generateAIships(AI);
		bpSPGame.setCenter(spgameCenter());
		flagAddingShips = true;
		flagShipInProgress = false;
		flagPlay = false;
		flagPlayerWins = false;
		cbShipSize.setDisable(false);
		buttonToMainMenu.setDisable(false);
		shipCount = shipCountDefault;
		cbShipSize.setValue(String.valueOf(shipSizeDefault));
		labelShipsAvailable.setText(String.valueOf(shipCount));
		tfStatus.setText("");
		reverseGridButtonsDisabledState('o');
		if (!hboxNewGameBottom.getChildren().contains(buttonToMainMenu)) {
			hboxNewGameBottom.getChildren().addAll(buttonToMainMenu);
		}
	}

	/**
	 * Tilastot-ikkunan visuaalinen esitys.
	 */
	private void näytäTilastot() {
		// // Metodia kutsutaan täällä: buttonActions() > tilastot.setOnAction()
		// Luodaan visuaalinen esitys tilastoille
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);

		// Haetaan kaikkien pelaajien tilastot
		ArrayList<Tilastot> pelaajat = ctrl.getTilastot();
		StringBuilder sb = new StringBuilder();
		if (pelaajanNimi == null) {
			sb.append(ctrl.getText('t', 2));
		} else {
			sb.append(ctrl.getText('t', 3) + pelaajanNimi + ".");
		}
		sb.append(ctrl.getText('t', 0));

		for (int i = 0; i < pelaajat.size(); i++) {
			Tilastot_IF t = ctrl.getTilastot(i);
			Double[] d = t.getTilastot();

			sb.append(t.getPelaajanNimi() + ": \n");
			sb.append(ctrl.getText('t', 4) + d[0].intValue() + ".\n");
			sb.append(ctrl.getText('t', 5) + d[1].intValue() + ".\n");
			sb.append(ctrl.getText('t', 6) + df.format(d[2] * 100) + "%.\n\n");

			sb.append(ctrl.getText('t', 7) + d[3].intValue() + ".\n");
			sb.append(ctrl.getText('t', 8) + d[4].intValue() + ".\n");
			sb.append(ctrl.getText('t', 9) + df.format(d[5] * 100) + "%.");
			if (1 < pelaajat.size() && ctrl.getTilastotIndex(t.getPelaajanNimi()) < (pelaajat.size() - 1)) {
				sb.append(ctrl.getText('t', 1));
			}
		}
		;

		final Stage stage = new Stage();
		stage.setTitle(ctrl.getText('b', 9));
		stage.setMinWidth(subWindowMinWidth);
		stage.setMinHeight(subWindowMinHeight);
		stage.setMaxWidth(subWindowMaxWidth);
		stage.setMaxHeight(subWindowMaxHeight);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initOwner(primaryStage);

		VBox dialogVbox = new VBox(20);
		dialogVbox.setStyle(ctrl.getText('s', 0));
		dialogVbox.setSpacing(25);
		dialogVbox.setPadding(new Insets(20, 25, 20, 25));
		dialogVbox.getChildren().addAll(setLabel(sb.toString(), "Arial", 14, FontWeight.NORMAL));

		ScrollPane scrollPane = new ScrollPane(dialogVbox);
		scrollPane.setFitToWidth(true);

		Scene dialogScene = new Scene(scrollPane);
		stage.setScene(dialogScene);
		stage.setMaxWidth(1);
		stage.setMaxHeight(1);
		stage.show();
		if (dialogVbox.getHeight() < 640) {
			stage.setMaxHeight(dialogVbox.getHeight());
			stage.setHeight(dialogVbox.getHeight());
		} else {
			stage.setMaxHeight(640);
			stage.setHeight(640);
		}
	}

	/**
	 * Kehoite uuden pelaajan luomiseen
	 *
	 * @return Palautetaan tieto dialogi-ikkunan tuloksesta
	 */
	private Optional<String> uusiPelaaja() {
		TextInputDialog dialog = new TextInputDialog(pelaajanNimi);
		dialog.setTitle(ctrl.getText('l', 1));
		dialog.setHeaderText(ctrl.getText('l', 5));
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			this.pelaajanNimi = result.get();
			ctrl.uusiPelaaja(this.pelaajanNimi, AI);
		}
		return result;
	}

	/**
	 * @param labelText
	 *            Uuden labelin teksti.
	 * @param font
	 *            Labelin fontti.
	 * @param size
	 *            Fontin koko.
	 * @param weight
	 *            FontWeight -ominaisuus.
	 * @return Palauttaa luodun labelin.
	 */
	@Override
	public Label setLabel(String labelText, String font, int size, FontWeight weight) {
		Label label = new Label();
		label.setStyle("-fx-background-color: rgba(255,255,255,0.6); -fx-padding: 8 8 8 8;");
		label.setText(labelText);
		label.setFont(Font.font(font, weight, size));
		label.setWrapText(true);
		return label;
	}

	/**
	 * @param labelText
	 *            Uuden labelin teksti.
	 * @param font
	 *            Labelin fontti.
	 * @param size
	 *            Fontin koko.
	 * @return Palauttaa luodun labelin.
	 */
	@Override
	public Label setLabel(String labelText, String font, int size) {
		Label label = new Label();
		label.setText(labelText);
		label.setFont(Font.font(font, size));
		label.setWrapText(true);
		return label;
	}

	/*
	 * Aseta nappulan arvot parametreista
	 */
	/**
	 * @param button
	 *            Asetettava button.
	 * @param buttonText
	 *            Buttonin teksti.
	 * @param font
	 *            Buttonin fontti.
	 * @param bgColor
	 *            Buttonin taustaväri.
	 * @param textColor
	 *            Buttonin tekstin väri.
	 * @param textSize
	 *            Buttonin tekstin koko.
	 */
	@Override
	public void setButton(Button button, String buttonText, String font, String bgColor, String textColor, int textSize,
			String borderColor) {
		button.setText(buttonText);
		button.setStyle("-fx-font: " + textSize + " " + font + "; -fx-text-fill: " + textColor
				+ "; -fx-background-color: " + bgColor + "; -fx-border-color: '" + borderColor + "';");
		button.setPadding(new Insets(20, 45, 20, 45));
		button.setMaxWidth(Double.MAX_VALUE);
	}

	/**
	 * @param flagIsTurn
	 *            Ilmaisee onko pelaajan vuoro.
	 */
	@Override
	public void setFlagIsTurn(boolean flagIsTurn) {
		this.flagIsTurn = flagIsTurn;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
