package controller;

import java.util.ArrayList;
import java.util.Map;

import javafx.scene.control.Button;
import model.Client;
import model.Client_IF;
import model.Data;
import model.Data_IF;
import model.LaivanupotusAI;
import model.LaivanupotusAI_IF;
import model.LaivanupotusPeli;
import model.LaivanupotusPeli_IF;
import model.Server;
import model.Server_IF;
import model.Tilastot;
import model.TilastotDAO;
import model.TilastotDAO_IF;
import model.Tilastot_IF;
import view.LaivanupotusGUI_IF;

public class Controller implements Controller_IF {

	private LaivanupotusGUI_IF gui;
	private Data_IF data;
	private LaivanupotusPeli_IF lup;
	private LaivanupotusAI_IF ai;
	private Server_IF server;
	private Client_IF client;
	private TilastotDAO_IF tdao;

	public Controller(LaivanupotusGUI_IF laivanupotusGUI, String AI) {
		this.tdao = new TilastotDAO();
		this.data = new Data(tdao);
		this.lup = new LaivanupotusPeli();
		this.ai = new LaivanupotusAI(lup);
		this.gui = laivanupotusGUI;
	}

	/**
	 * Asettaa tekoälyn generoimaan laivastonsa
	 *
	 * @param AI
	 *            Teköälypelaajan nimi
	 */
	@Override
	public void generateAIships(String AI) {
		int i = 24;
		while (1 < i) {
			int laivanPituus = (int) (Math.round(Math.random() * 3 + 2));
			if (0 <= (i - laivanPituus)) {
				if (ai.asetaLaiva(AI, laivanPituus)) {
					i = i - laivanPituus;
				}
			}
		}
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi
	 * @return Kts. LaivanupotusAI -luokka.
	 */
	@Override
	public boolean ammu_AI(String pelaajanNimi) {
		return ai.ammu(pelaajanNimi);
	}

	/**
	 * @return Palauta x-koordinaatti.
	 */
	@Override
	public int aiCoordX() {
		return ai.getLaukausX();
	}

	/**
	 * @return Palauta y-koordinaatti.
	 */
	@Override
	public int aiCoordY() {
		return ai.getLaukausY();
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param AI
	 *            Teköälypelaajan nimi.
	 */
	@Override
	public void uusiPelaaja(String pelaajanNimi, String AI) {
		lup.uusiPelaaja(pelaajanNimi, 10, 10);
		lup.uusiPelaaja(AI, 10, 10);
		data.addPlayer(pelaajanNimi);
	}

	/**
	 * @param c
	 *            Palautettavan merkkijonon ryhmän tunnus.
	 * @param i
	 *            Palautettavan merkkijonon indeksi ryhmässään.
	 * @return Palautetaan merkkijono Data -luokasta.
	 */
	@Override
	public String getText(char c, int i) {
		return data.getString(c, i);
	}

	/**
	 * @param c
	 *            Valittavan Mapin tunnus.
	 * @return Palautetaan HashMap, jossa on Integer,Button arvopareja.
	 */
	@Override
	public Map<Integer, Button> getHashMap(char c) {
		return data.getHashMap(c);
	}

	/**
	 * @param h
	 *            Buttonin tunnus.
	 * @param c
	 *            Buttonin ryhmä.
	 * @return Palautetaan peliruudukon button.
	 */
	@Override
	public Button getHashButton(int h, char c) {
		return data.getHashButton(h, c);
	}

	/**
	 * @param gb
	 *            Buttonin Data-luokan HashMappiin vietävä button.
	 * @param x
	 *            Buttonin x-koordinaatti.
	 * @param y
	 *            Buttonin y-koordinaatti.
	 * @param c
	 *            Buttonin ryhmä.
	 */
	@Override
	public void setHashButton(Button gb, int x, int y, char c) {
		data.setHashButton(gb, x, y, c);
	}

	/**
	 * @param ai
	 *            Tekoälypelaajan nimi.
	 * @param x
	 *            X-koordinaatti.
	 * @param y
	 *            Y-koordinaatti.
	 * @return Palauttaa boolean tiedon ampumisesta.
	 */
	@Override
	public Boolean ammu(String ai, int x, int y) {
		return lup.ammu(ai, x, y);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param x
	 *            X-koordinaatti.
	 * @param y
	 *            Y-koordinaatti.
	 * @return Palauttaa boolean tiedon oliko ruudussa laiva.
	 */
	@Override
	public Boolean onkoRuudussaLaiva(String pelaajanNimi, int x, int y) {
		return lup.onkoRuudussaLaiva(pelaajanNimi, x, y);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @return Palauttaa tiedon onko pelaajan laivat uponneet.
	 */
	@Override
	public Boolean pelaajanLaivatUponnut(String pelaajanNimi) {
		return lup.pelaajanLaivatUponnut(pelaajanNimi);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @param shipSize
	 *            Valitun laivan koko.
	 * @param shipXCoordold
	 *            Edellisen valinnan x-koordinaatti.
	 * @param shipYCoordold
	 *            Edellisen valinnan y-koordinaatti.
	 * @param xCoord
	 *            Nykyisen valinnan x-koordinaatti.
	 * @param yCoord
	 *            Nykyisen valinnan x-koordinaatti.
	 * @return Palautetaan boolean tieto lisäyksen onnistumisesta.
	 */
	@Override
	public boolean lisaaLaiva(String pelaajanNimi, int shipSize, int shipXCoordold, int shipYCoordold, int xCoord,
			int yCoord) {
		int[] x = new int[shipSize];
		int[] y = new int[shipSize];
		if (shipXCoordold != xCoord) {
			for (int i = 0; i < shipSize; i++) {
				if (shipXCoordold < xCoord) {
					x[i] = shipXCoordold + i;
					y[i] = yCoord;
				} else {
					x[i] = xCoord + i;
					y[i] = yCoord;
				}
			}
		} else if (shipYCoordold != yCoord) {
			for (int i = 0; i < shipSize; i++) {
				if (shipYCoordold < yCoord) {
					x[i] = xCoord;
					y[i] = shipYCoordold + i;
				} else {
					x[i] = xCoord;
					y[i] = yCoord + i;
				}
			}
		}
		return lup.lisaaLaiva(pelaajanNimi, x, y);
	}

	/**
	 * Multiplayer -ominaisuus, jota ei ole kokonaan implementoitu.
	 *
	 * @param ip IP-osoite.
	 * @param portti Portti.
	 * @deprecated
	 */
	@Override
	public void createNewMP(String ip, int portti) {
		server = new Server(portti);
		server.start();
		joinNewMP(ip, portti);
	}

	/**
	 * Multiplayer -ominaisuus, jota ei ole kokonaan implementoitu.
	 *
	 * @param ip IP-osoite.
	 * @param portti Portti.
	 * @deprecated
	 */
	@Override
	public void joinNewMP(String ip, int portti) {
		client = new Client(this, ip, portti);
		client.start();
	}

	/**
	 * Poistaa nykyisen pelisession paikalliset pelaajat.
	 */
	@Override
	public void poistaKaikkiPelaajat() {
		lup.poistaKaikkiPelaajat();
	}

	/**
	 * Multiplayer -ominaisuus, jota ei ole kokonaan implementoitu. Lähettää GUI
	 * "AMMU" tiedon clientille, vastaanottaa tiedon osumasta.
	 *
	 * @param string Lähetettävä data.
	 * @return Palautetaan dataa.
	 * @deprecated
	 */
	@Override
	public String sendDataToClient(String string) {
		String osuiko = client.sendData(string);
		return osuiko;
	}

	/**
	 * Multiplayer -ominaisuus, jota ei ole kokonaan implementoitu.
	 *
	 * @param osuiko
	 *            Koordinaattitieto merkkijonona.
	 * @return Palautetaan tieto tuloksesta.
	 * @deprecated
	 */
	@Override
	public String mpOsuiko(String osuiko) {
		gui.setFlagIsTurn(true);
		int x = Integer.valueOf(osuiko.substring(0, 1));
		int y = Integer.valueOf(osuiko.substring(1, 2));
		Button gb = getHashButton(Integer.valueOf(String.valueOf(y) + "" + String.valueOf(x)), 'p');
		if (onkoRuudussaLaiva("pelaajan_nimi", x, y)) {
			if (pelaajanLaivatUponnut("pelaajan_nimi")) {
				// LAIVASTO UPPOSI
				gui.laivastoUpposi();
				return "UPPOSI";
			} else {
				// LAIVAAN OSUI
				gui.laivaanOsui(gb, x, y);
				return "OSUI";
			}
		} else {
			// LAIVAAN EI OSUNUT
			gui.laivaanEiOsunut(gb, x, y);
			return "EIOSUNUT";
		}
	}

	/**
	 * @param v
	 *            Tekoälypelaajan vaikeusaste.
	 */
	@Override
	public void setAIvaikeus(int v) {
		ai.setVaikeusTaso(v);
	}

	/**
	 * @return Palauta tekoälypelaan nykyinen vaikeusaste.
	 */
	@Override
	public int getAIvaikeus() {
		return ai.getVaikeusTaso();
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @return Palautetaan pelaajan laivojen yhteiskoon lukumäärä.
	 */
	@Override
	public int getPelaajanLaivojenYhteiskoko(String pelaajanNimi) {
		return lup.getPelaajanLaivojenYhteiskoko(pelaajanNimi);
	}

	/**
	 * @param s
	 *            Pelierän loppputuloksesta riippuva tunnus.
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 */
	@Override
	public void setTilastot(String s, String pelaajanNimi) {
		data.setTilastot(s, pelaajanNimi);
	}

	/**
	 * Palautetaan pelaajan tilastot pelaajan nimen perusteella.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @return Palautetaan yksittäisen pelaajan tilastot.
	 */
	@Override
	public Tilastot_IF getTilastot(String pelaajanNimi) {
		return data.getTilastot(pelaajanNimi);
	}

	/**
	 * Palautetaan pelaajan tilaston indeksi pelaajan nimen perusteella.
	 *
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @return Palautetaan indeksi pelaajan tilastoista.
	 */
	@Override
	public int getTilastotIndex(String pelaajanNimi) {
		return data.getTilastotIndex(pelaajanNimi);
	}

	/**
	 * Palautetaan pelaajan tilastot indeksin perusteella.
	 *
	 * @param index
	 *            Palautettavan tilaston indeksi.
	 * @return Palautetaan yksittäisen pelaajan tilastot.
	 */
	@Override
	public Tilastot_IF getTilastot(int index) {
		return data.getTilastot(index);
	}

	/**
	 * @return Palautetaan ArrayList pelaajien tilastoista.
	 */
	@Override
	public ArrayList<Tilastot> getTilastot() {
		return data.getTilastot();
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 * @return Palautetaan boolean tieto onnistuiko tietokannan päivitys.
	 */
	@Override
	public boolean updateTilastotDAO(String pelaajanNimi) {
		return data.updateTilastotDAO(pelaajanNimi);
	}

	/**
	 * @param pelaajanNimi
	 *            Pelaajan nimi.
	 */
	@Override
	public void poistaPelaaja(String pelaajanNimi) {
		tdao.deleteTilastot(pelaajanNimi);
	}

	/**
	 * @return Palautetaan boolean tieto onko SessionFactory olemassa.
	 */
	@Override
	public boolean isSessionFactory() {
		return tdao.isSessionFactory();
	}

	/**
	 * Pyydetään sulkemaan SessionFactory.
	 */
	@Override
	public void closeSessionFactory() {
		tdao.closeSessionFactory();
	}
}
