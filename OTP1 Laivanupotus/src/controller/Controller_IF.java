package controller;

import java.util.ArrayList;
import java.util.Map;

import javafx.scene.control.Button;
import model.Tilastot;
import model.Tilastot_IF;

public interface Controller_IF {

	public abstract String getText(char c, int i);
	public abstract void generateAIships(String AI);
	public abstract Map<Integer, Button> getHashMap(char c);
	public abstract Button getHashButton(int h, char c);
	public abstract void setHashButton(Button gb, int i, int j, char c);
	public abstract boolean lisaaLaiva(String nimi, int shipSize, int shipXCoordold, int shipYCoordold, int xCoord, int yCoord);
	public abstract void createNewMP(String ip, int portti);
	public abstract void joinNewMP(String ip, int portti);
	public abstract void poistaKaikkiPelaajat();
	public abstract void uusiPelaaja(String pelaajanNimi, String AI);
	public abstract Boolean ammu(String nimi, int x, int y);
	public abstract Boolean onkoRuudussaLaiva(String nimi, int x, int y);
	public abstract Boolean pelaajanLaivatUponnut(String nimi);
	public abstract boolean ammu_AI(String pelaajanNimi);
	public abstract int aiCoordX();
	public abstract int aiCoordY();
	public abstract String sendDataToClient(String string);
	public abstract String mpOsuiko(String ammu);
	public abstract void setAIvaikeus(int v);
	public abstract int getAIvaikeus();
	public abstract int getPelaajanLaivojenYhteiskoko(String s);
	public abstract void setTilastot(String s, String pelaajanNimi);
	public abstract Tilastot_IF getTilastot(String pelaajanNimi);
	public abstract Tilastot_IF getTilastot(int index);
	public abstract int getTilastotIndex(String pelaajanNimi);
	public abstract ArrayList<Tilastot> getTilastot();
	public abstract boolean updateTilastotDAO(String pelaajanNimi);
	public abstract void poistaPelaaja(String pelaajanNimi);
	public abstract boolean isSessionFactory();
	public abstract void closeSessionFactory();

}
