package model;

import org.hibernate.SessionFactory;

public interface TilastotDAO_IF {
	public abstract boolean createTilastot(Tilastot tilastot);
	public abstract Tilastot readTilastot(String pelaajanNimi);
	public abstract Tilastot[] readTilastot();
	public abstract boolean updateTilastot(Tilastot tilastot);
	public abstract boolean deleteTilastot(String pelaajanNimi);
	public abstract boolean isSessionFactory();
	public abstract void closeSessionFactory();

}
