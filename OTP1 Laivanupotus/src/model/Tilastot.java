package model;

import javax.persistence.*;

@Entity
@Table(name="TILASTOT")
public class Tilastot implements Tilastot_IF, Comparable<Object> {


	/*
	 * Tunnelointi portti 3300 -> 10.114.32.36:3306
	 *
	 *
	 * CREATE DATABASE laivanupotus;
	 *
	 * //use laivanupotus;
	 *
	 * CREATE TABLE TILASTOT (
	 * 	pelaajanNimi varchar(50),
	 * 	tilastot BLOB,
	 * 	PRIMARY KEY(pelaajanNimi)
	 * );
	 *
	 */


	@Id
	@Column(name="pelaajanNimi")
	private String pelaajanNimi = null;

	@Lob
	@Column(name="tilastot", columnDefinition = "BLOB")
	private Double[] tilastot = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	Tilastot (String pelaajanNimi, Double[] tilastot) {
		this.pelaajanNimi = pelaajanNimi;
		this.tilastot = tilastot;
	}

	Tilastot (String pelaajanNimi) {
		this.pelaajanNimi = pelaajanNimi;
	}

	Tilastot () {}

	@Override
	public String getPelaajanNimi() {
		return pelaajanNimi;
	}

	@Override
	public void setPelaajanNimi(String pelaajanNimi) {
		this.pelaajanNimi = pelaajanNimi;
	}

	@Override
	public Double[] getTilastot() {
		return tilastot;
	}

	@Override
	public void setTilastot(Double[] tilastot) {
		this.tilastot = tilastot;
	}

	@Override
	public void setTilastot(Double d, int j) {
		this.tilastot[j] = d;
	}

	@Override
	public int compareTo(Object obj) {
		Tilastot t = (Tilastot) obj;
		return pelaajanNimi.compareTo(t.getPelaajanNimi());
	}

}
