package model;

import java.util.ArrayList;

public class Pelaaja implements Pelaaja_IF {

	// Ruudukon default koot.
	private static int DEFAULT_X_KOKO = 10;
	private static int DEFAULT_Y_KOKO = 10;

	private Ruudukko ruudukko;
	private Laivasto laivasto;

	/**
	 * Konstruktori. Luo ruudukon default koolla.
	 */
	public Pelaaja() {
		this.ruudukko = new Ruudukko(DEFAULT_X_KOKO, DEFAULT_Y_KOKO);
		this.laivasto = new Laivasto();
	}

	/**
	 * Asettaa pelaajan ruudukon annettuun kokoon.
	 *
	 * @param x Ruudukon X-koko.
	 * @param y Ruudukon Y-koko.
	 */
	@Override
	public void setKarttaKoko(int x, int y) {
		this.ruudukko = new Ruudukko(x, y);
	}


	/**
	 * Lisää pelaajalle laivan annettuihin ruutuihin. Lisäys epäonnistuu jos pelaajalla
	 * on jo ruuduissa laiva, koordinaatit ovat ohi ruudukon tai x- ja y-koordinaatteja
	 * on eri määrä.
	 *
	 *
	 * @param x int[] taulukko X koordinaateista
	 * @param y int[] taulukko Y koordinaateista
	 * @return True, jos lisäys onnistui
	 */
	@Override
	public boolean lisaaLaiva(int[] x, int[] y) {
		// koordinaatit puuttuvat
		if (x.length == 0 || y.length == 0)
			return false;

		// eri määrä koordinaatteja
		if (x.length != y.length)
			return false;

		// koordinaattien tarkistus
		for (int i = 0; i < x.length; i++) {
			if (!ruudukko.onTyhja(x[i], y[i]))
				return false;
		}

		ArrayList<Ruutu> ruudut = new ArrayList<Ruutu>();

		for (int i = 0; i < x.length; i++) {
			ruudut.add(ruudukko.getRuutu(x[i], y[i]));
		}

		return laivasto.lisaaLaiva(ruudut);
	}

	/**
	 * Palauttaa pelaajan laivaston.
	 *
	 * @return Laivasto-olio
	 */
	@Override
	public Laivasto getLaivasto() {
		return laivasto;
	}

	/**
	 * Palauttaa pelaajan ruudukon.
	 *
	 * @return Ruudukko-olio
	 */
	@Override
	public Ruudukko getRuudukko() {
		return ruudukko;
	}

}
