package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread implements Server_IF {
	private ServerSocket server;
	private int port;
	//Ekan pelaajan socket
	private Socket socket_1;
	//Toisen pelaajan socket
	private Socket socket_2;

    BufferedReader input_1;
    PrintWriter output_1;
    BufferedReader input_2;
    PrintWriter output_2;

	public Server(int port) {
		this.port = port;
	}

	public void createGame(int port) {
		try {
			server = new ServerSocket(port);
				//Odotetaan molempien yhdistämistä
				socket_1 = server.accept();
				socket_2 = server.accept();


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			createGame(port);
            input_1 = new BufferedReader(new InputStreamReader(socket_1.getInputStream()));
            output_1 = new PrintWriter(socket_1.getOutputStream(), true);
            input_2 = new BufferedReader(new InputStreamReader(socket_2.getInputStream()));
            output_2 = new PrintWriter(socket_2.getOutputStream(), true);
			System.out.println("Game created");

			//Vuoromuuttuja
			int turn = 0;

			//Pelin loop
			//TODO: Tämä käsittelee kaikki pelin päivitykset
			while(true) {
				if(turn % 2 == 0) {
					//AMMU
					String siirto_1 = input_1.readLine();
					if(siirto_1 != null) {
						output_2.write(siirto_1);
						String osuiko = input_2.readLine();
						output_1.write(osuiko);
					}
				}
				else {
					//AMMU
					String siirto_2 = input_2.readLine();
					if(siirto_2 != null) {
						output_1.write(siirto_2);
						String osuiko = input_1.readLine();
						output_2.write(osuiko);
					}
				}
				turn++;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {

				socket_1.close();
				socket_2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}