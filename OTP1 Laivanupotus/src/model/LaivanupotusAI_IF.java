package model;

public interface LaivanupotusAI_IF {

	public abstract boolean asetaLaiva(String pelaajanNimi, int laivanPituus);

	public abstract boolean ammu(String pelaajanNimi);

	public abstract int getLaukausX();
	public abstract int getLaukausY();

	public abstract int[][] getXY();

	public abstract boolean normiAmmu(String pelaajanNimi);
	public abstract boolean huijausAmmu(String pelaajanNimi);
	public abstract boolean superAmmu(String pelaajanNimi);
	public abstract boolean smartAmmu(String pelaajanNimi);

	public abstract int getVaikeusTaso();
	public abstract void setVaikeusTaso(int vaikeusTaso);

	public abstract int getHuijausProsentti();
	public abstract void setHuijausProsentti(int prosentti);

}
