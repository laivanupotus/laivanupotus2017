package model;

public interface Client_IF {

	public abstract void start();

	public abstract String sendData(String string);

	public abstract String receiveData();

}
