package model;

import java.util.ArrayList;

public interface Laivasto_IF {
	boolean lisaaLaiva(ArrayList<Ruutu> ruudut);
	boolean uponnut();
	int getLaivojenYhteiskoko();
	int[][] getLaivojenKoordinaatit();
}
