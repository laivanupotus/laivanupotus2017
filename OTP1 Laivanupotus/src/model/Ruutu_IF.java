package model;

public interface Ruutu_IF {

	Laiva getLaiva();

	int getY();

	int getX();

	boolean onLaiva();

	boolean ammuttu();

	boolean ammu();

	void setLaiva(Laiva laiva);

	void setY(int koordinaatti_Y);

	void setX(int koordinaatti_X);


}
