package model;

public class Ruudukko implements Ruudukko_IF {

	public static final char SYMBOL_LAIVA = 'S';
	public static final char SYMBOL_MERI = '~';
	public static final char SYMBOL_SUMU = '#';
	public static final char SYMBOL_LAUKAUS_OSUMA = 'X';
	public static final char SYMBOL_LAUKAUS_HUTI = 'O';

	private Ruutu[][] ruudut;
	private int koko_X;
	private int koko_Y;

	/**
	 * Konstruktori.
	 *
	 * @param koko_X
	 *            Ruudukon X-koko.
	 * @param koko_Y
	 *            Ruudukon Y-koko.
	 */
	public Ruudukko(int koko_X, int koko_Y) {
		this.koko_X = koko_X;
		this.koko_Y = koko_Y;

		ruudut = new Ruutu[koko_Y][koko_X];

		for (int i = 0; i < koko_Y; i++) {
			for (int j = 0; j < koko_X; j++) {
				ruudut[i][j] = new Ruutu(j, i);
			}
		}
	}

	/**
	 * Kertoo jos koordinaatit ovat ruudukon sisällä.
	 *
	 *
	 * @param x X-koordinaatti
	 * @param y Y-Koordinaatti
	 * @return True, jos ruutu on ruudukossa
	 */
	@Override
	public boolean onRuudukossa(int x, int y) {
		if (x < 0 || y < 0 || x >= koko_X || y >= koko_Y)
			return false;

		return true;
	}

	/**
	 * Kertoo onko ruudukossa laiva vai ei.
	 *
	 * @param x Ruudun x-koordinaatti.
	 * @param y Ruudun y-koordinaatti.
	 * @return True, jos ruudussa ei ole laivaa
	 */
	@Override
	public boolean onTyhja(int x, int y) {
		if (onRuudukossa(x, y))
			if (!ruudut[y][x].onLaiva())
				return true;

		return false;
	}


	/**
	 * Palauttaa ruudukon X-koordinaattien koon.
	 *
	 * @return Ruudukon X-koko
	 */
	@Override
	public int getX() {
		return koko_X;
	}


	/**
	 * Palauttaa ruudukon Y-koordinaattien koon.
	 *
	 *
	 * @return Ruudukon Y-koko
	 */
	@Override
	public int getY() {
		return koko_Y;
	}


	/**
	 *	Palauttaa ruudun annetuista koordinaateista.
	 *
	 *
	 * @param x X-koordinaatti
	 * @param y Y-koordinaatti
	 * @return Koordinaateissa oleva ruutu, tai null
	 */
	@Override
	public Ruutu getRuutu(int x, int y) {
		try {
			return ruudut[y][x];
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			return null;
		}
	}

	/**
	 * Yrittää ampua ruutuun jos ruutu on ruudukon sisällä.
	 *
	 *
	 * @param x X-koordinaatti
	 * @param y Y-koordinaatti
	 * @return True, jos ampuminen onnistui
	 */
	@Override
	public boolean ammu(int x, int y) {
		if (onRuudukossa(x, y))
			return ruudut[y][x].ammu();

		return false;
	}

	/**
	 * Luo char[][] taulukon ruudukosta. Jos fog_of_war on päällä, piilotetaan ruudut joihin
	 * ei ole ammuttu vielä.
	 *
	 * Symbolit:
	 * S = laiva
	 * # = sumu
	 * ~ = meri
	 * X = osuma
	 * O = huti
	 *
	 * @param fog_of_war Ovatko ruudut sumun alla.
	 * @return char[][] taulukko
	 */
	@Override
	public char[][] getKartta(boolean fog_of_war) {
		char ruudukko[][] = new char[koko_Y][koko_X];

		for (int i = 0; i < koko_Y; i++) {
			for (int j = 0; j < koko_X; j++) {
				if (ruudut[i][j].ammuttu()) {
					if (ruudut[i][j].onLaiva())
						ruudukko[i][j] = SYMBOL_LAUKAUS_OSUMA;
					else
						ruudukko[i][j] = SYMBOL_LAUKAUS_HUTI;
				} else {
					if (fog_of_war)
						ruudukko[i][j] = SYMBOL_SUMU;
					else if (ruudut[i][j].onLaiva())
						ruudukko[i][j] = SYMBOL_LAIVA;
					else
						ruudukko[i][j] = SYMBOL_MERI;
				}
			}
		}
		return ruudukko;
	}
}
