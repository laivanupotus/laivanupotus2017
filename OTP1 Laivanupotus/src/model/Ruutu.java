package model;

public class Ruutu implements Ruutu_IF {

    private int koordinaatti_X;
    private int koordinaatti_Y;
    private Laiva laiva;
    private boolean ammuttu;

    /**
     * Konstruktori.
     *
     * @param koordinaatti_X Ruudun X-koordinaatti.
     * @param koordinaatti_Y Ruudun Y-koordinaatti.
     */
    public Ruutu(int koordinaatti_X, int koordinaatti_Y) {
        this.koordinaatti_X = koordinaatti_X;
        this.koordinaatti_Y = koordinaatti_Y;
        ammuttu = false;
        laiva = null;
    }


	/**
	 * Asettaa ruudun x-koordinaatin.
	 *
	 * @param koordinaatti_X X-koordinaatti
	 */
    @Override
    public void setX(int koordinaatti_X) {
        this.koordinaatti_X = koordinaatti_X;
    }

    /**
     * Asettaa ruudun y-koordinaatin.
     *
     * @param koordinaatti_Y Y-koordinaatti
     */
    @Override
    public void setY(int koordinaatti_Y) {
        this.koordinaatti_Y = koordinaatti_Y;
    }

    /**
     * Asettaa ruudussa olevan laivan
     *
     * @param laiva Laiva
     */
    @Override
    public void setLaiva(Laiva laiva) {
        this.laiva = laiva;
    }

    /**
     * Ampuu ruutuun.
     *
     * @return True, jos ruutuun ei ole aikaisemmin ammuttu
     */
    @Override
    public boolean ammu() {
    	if (ammuttu)
    		return false;
    	else {
    		ammuttu = true;
    		if (laiva != null)
    			laiva.upota();
    	}

    	return true;
    }

    /**
     * Kertoo onko ruutuun ammuttu
     *
     * @return True, jos ruutuun on ammuttu
     */
    @Override
    public boolean ammuttu() {
        return ammuttu;
    }

    /**
     * Kertoo onko ruudussa laiva
     *
     * @return True, jos ruudussa on laiva
     */
    @Override
    public boolean onLaiva() {
    	if (laiva == null)
    		return false;
    	else
    		return true;
    }


    /**
     * Palauttaa ruudun x-koordinaatin
     *
     * @return X-koordinaatti
     */
    @Override
    public int getX() {
        return koordinaatti_X;
    }

    /**
     * Palauttaa ruudun y-koordinaatin.
     *
     * @return Y-koordinaatti
     */
    @Override
    public int getY() {
        return koordinaatti_Y;
    }

    /**
     * Palauttaa ruudukossa olevan laivan
     *
     * @return Laiva, tai null jos ruudussa ei ole laivaa.
     */
    @Override
    public Laiva getLaiva() {
        return laiva;
    }
}
