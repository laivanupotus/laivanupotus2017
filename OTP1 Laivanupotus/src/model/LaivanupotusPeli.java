package model;

import java.util.HashMap;

public class LaivanupotusPeli implements LaivanupotusPeli_IF {

	HashMap<String, Pelaaja> pelaajat;

	/**
	 * Konstruktori.
	 *
	 */
	public LaivanupotusPeli() {
		pelaajat = new HashMap<String, Pelaaja>();
	}

	/**
	 * Uuden pelaajan lisäys. Default ruudukko.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @return True, jos lisäys onnistui.
	 */
	@Override
	public boolean uusiPelaaja(String nimi) {
		if (pelaajat.containsKey(nimi))
			return false;

		pelaajat.put(nimi, new Pelaaja());

		return true;
	}

	/**
	 *
	 * Pelaajan lisäys. Custom ruudukko.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param x
	 *            Ruudukon X koordinaatin koko.
	 * @param y
	 *            Ruudukon Y koordinaatin koko.
	 * @return True, jos lisäys onnistui.
	 */
	@Override
	public boolean uusiPelaaja(String nimi, int x, int y) {
		if (pelaajat.containsKey(nimi) || x < 1 || y < 1)
			return false;

		pelaajat.put(nimi, new Pelaaja());
		pelaajat.get(nimi).setKarttaKoko(x, y);

		return true;
	}

	/**
	 * Poistaa pelaajan.
	 *
	 * @param nimi
	 *            Poistettavan pelaajan nimi.
	 * @return True, jos poisto onnistui.
	 */
	@Override
	public boolean poistaPelaaja(String nimi) {
		if (pelaajat.containsKey(nimi)) {
			pelaajat.remove(nimi);
			return true;
		} else
			return false;
	}

	/**
	 *
	 * Kertoo onko pelissä pelaajaa annetulla nimellä.
	 *
	 * @param nimi
	 *            Haettava nimi
	 * @return True, jos pelaaja on luotu.
	 */
	@Override
	public boolean onPelaaja(String nimi) {
		return pelaajat.containsKey(nimi);
	}

	/**
	 * Poistaa kaikki pelaajat.
	 */
	@Override
	public void poistaKaikkiPelaajat() {
		pelaajat.clear();
	}

	/**
	 *
	 * Laivan lisäys.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param x
	 *            Taulukko x-koordinaateista.
	 * @param y
	 *            Taulukko y-koordinaateista.
	 * @return True, jos lisäys onnistui.
	 */
	@Override
	public boolean lisaaLaiva(String nimi, int[] x, int[] y) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).lisaaLaiva(x, y);
		else
			return false;
	}

	/**
	 * Ampuu annetun pelaajan ruudukkoon annettuihin koordinaatteihin.
	 *
	 *
	 * @param nimi Pelaajan nimi
	 * @param x X-koordinaatti
	 * @param y Y-koordinaatti
	 * @return True, jos ampuminen onnistui
	 */
	@Override
	public boolean ammu(String nimi, int x, int y) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).getRuudukko().ammu(x, y);

		return false;
	}

	/**
	 * Hakee pelaajan ruudukon char[y][x] taulukkona.
	 *
	 * @param nimi
	 *            Pelaajan nimi.
	 * @param fog_of_war
	 *            onko fog of war päällä.
	 * @return char[][] taulukko, tai null jos pelaajaa ei ollut.
	 */
	@Override
	public char[][] getPelaajanKartta(String nimi, boolean fog_of_war) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).getRuudukko().getKartta(fog_of_war);
		else
			return null;
	}

	/**
	 * Palauttaa annetun pelaajan koordinaatiston x-akselin koon.
	 *
	 * @param pelaajanNimi Pelaajan nimi
	 * @return Kartan X-akselin koko, tai -1
	 */
	@Override
	public int getPelaajanKartanKokoX(String pelaajanNimi) {
		if (pelaajat.containsKey(pelaajanNimi))
			return pelaajat.get(pelaajanNimi).getRuudukko().getX();
		else
			return -1;
	}

	/**
	 * Palauttaa annetun pelaajan koordinaatiston y-akselin koon.
	 *
	 * @param pelaajanNimi Pelaajan nimi
	 * @return Kartan Y-akselin koko, tai -1
	 */
	@Override
	public int getPelaajanKartanKokoY(String pelaajanNimi) {
		if (pelaajat.containsKey(pelaajanNimi))
			return pelaajat.get(pelaajanNimi).getRuudukko().getY();
		else
			return -1;
	}

	/**
	 * Kertoon onko annetun pelaajan laivasto uponnut.
	 *
	 * @param nimi Pelaajan nimi
	 *
	 * @return True, jos pelaajan laivasto on uponnut
	 */
	@Override
	public boolean pelaajanLaivatUponnut(String nimi) {
		if (pelaajat.containsKey(nimi))
			return pelaajat.get(nimi).getLaivasto().uponnut();
		else
			return false;
	}

	/**
	 * Kertoo onko annetulla pelaajalla annetuissa koordinaateissa laiva.
	 *
	 * @param nimi Pelaajan nimi
	 * @param x x-koordinaatti
	 * @param y y-koordinaatti
	 *
	 * @return True, jos laiva löytyi ruudusta
	 */
	@Override
	public boolean onkoRuudussaLaiva(String nimi, int x, int y) {
		if (pelaajat.containsKey(nimi))
			try {
				return pelaajat.get(nimi).getRuudukko().getRuutu(x, y).onLaiva();
			} catch (NullPointerException npe) {
				return false;
			}
		else
			return false;
	}

	/**
	 * Palauttaa taulukon jossa on annetun pelaajan laivojen käyttämien ruutujen koordinaatit.
	 *
	 * @param pelaajanNimi Pelaajan nimi
	 * @return Koordinaatti-taulukko, [0][x] [1][y]
	 */
	@Override
	public int[][] getPelaajanLaivojenKoordinaatit(String pelaajanNimi) {
		if (!pelaajat.containsKey(pelaajanNimi))
			return null;

		return pelaajat.get(pelaajanNimi).getLaivasto().getLaivojenKoordinaatit();
	}

	/**
	 * Palauttaa annetun pelaajan laivojen yhteiskoon.
	 *
	 * @param pelaajanNimi Pelaajan nimi
	 *
	 * @return laivojen yhteiskoko
	 */
	@Override
	public int getPelaajanLaivojenYhteiskoko(String pelaajanNimi) {
		if (!pelaajat.containsKey(pelaajanNimi))
			return -1;

		return pelaajat.get(pelaajanNimi).getLaivasto().getLaivojenYhteiskoko();
	}

}
