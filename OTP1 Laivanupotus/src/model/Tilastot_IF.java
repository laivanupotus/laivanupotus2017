package model;

public interface Tilastot_IF {
	public abstract String getPelaajanNimi();
	public abstract Double[] getTilastot();
	public abstract void setPelaajanNimi(String nimi);
	public abstract void setTilastot(Double d, int j);
	public abstract void setTilastot(Double[] tilastot);

}
