package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

import controller.Controller;

public class Client extends Thread implements Client_IF {
	private Socket socket;
	private int port;
    BufferedReader input;
    PrintWriter output;
	Scanner lukija = new Scanner(System.in);
	Controller ctrl;


	public Client(Controller ctrl, String ip, int port) {
		this.port = port;
		this.ctrl = ctrl;
	}

	public void createConnection() {
		try {
			socket = new Socket(InetAddress.getLocalHost(), port);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
			System.out.println("Connected");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void run() {
		try {
			if (socket == null) {
				createConnection();
			}
			while(true) {
				String osuiko = input.readLine();
				osuiko = ctrl.mpOsuiko(osuiko);
				output.write(osuiko);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//Lähettää serverille "AMMU" tiedon ja vastaanottaa tiedon osuiko
	@Override
	public String sendData(String string) {
		output.write(string);
		String osuiko = receiveData();
		return osuiko;
	}

	// Lähettää controllerille tiedon osuiko
	@Override
	public String receiveData() {
		try {
			return input.readLine();
		} catch (IOException e) {
			return null;
		}
	}

	public void osuiko(String string) {

	}
}