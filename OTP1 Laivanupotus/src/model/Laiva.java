package model;

import java.util.ArrayList;

public class Laiva implements Laiva_IF {

	private boolean uponnut;
	private ArrayList<Ruutu> ruudut;

	/**
	 * Konstruktori.
	 *
	 * @param ruudut Ruudut joissa laiva sijaitsee.
	 */
	public Laiva(ArrayList<Ruutu> ruudut) {
		this.ruudut = ruudut;
		this.ruudut.forEach(ruutu->ruutu.setLaiva(this));
		this.uponnut = false;
	}

	/**
	 * Upottaa laivan jos kaikkiin ruutuihin joissa laiva on, on ammuttu.
	 *
	 *
	 */
	@Override
	public void upota() {
		uponnut = true;
		ruudut.forEach(ruutu-> {
			if (!ruutu.ammuttu())
				uponnut = false;
		});
	}

	/**
	 * Kertoo onko laiva uponnut.
	 *
	 * @return True, jos laiva on uponnut.
	 */
	@Override
	public boolean getUponnut() {
		return uponnut;
	}

	/**
	 * Asettaa laivan uponnut-statuksen.
	 *
	 * @param uponnut True jos laiva on uponnut.
	 */
	@Override
	public void setUponnut(boolean uponnut) {
		this.uponnut = uponnut;
	}

	/**
	 * Palauttaa ruudut joissa laiva on.
	 *
	 * @return Laivan ruudut
	 */
	@Override
	public ArrayList<Ruutu> getRuudut() {
		return ruudut;
	}

	/**
	 * Asettaa laivan ruudut annettuihin.
	 *
	 * @param ruudut Ruudut
	 */
	@Override
	public void setRuudut(ArrayList<Ruutu> ruudut) {
		this.ruudut = ruudut;
		this.ruudut.forEach(ruutu->ruutu.setLaiva(this));
	}
}
