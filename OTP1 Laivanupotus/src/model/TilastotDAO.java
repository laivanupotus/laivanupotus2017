package model;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import model.Tilastot_IF;

public class TilastotDAO implements TilastotDAO_IF {

	SessionFactory sf;

	public TilastotDAO() {
		sf = null;
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
		try {
			sf = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			StandardServiceRegistryBuilder.destroy(registry);
			System.out.println("SessionFactoryn luomisessa tapahtui virhe, yhteyttä tietokantaan ei muodostettu.");
		}
	}

	@Override
	public boolean createTilastot(Tilastot tilastot) {
		Session session = sf.openSession();
		Transaction tr = null;
		try {
			tr = session.beginTransaction();
			session.saveOrUpdate(tilastot);
			tr.commit();
			return true;
		} catch (Exception e) {
			if (tr != null)
				tr.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	@Override
	public Tilastot readTilastot(String pelaajanNimi) {
		Session session = sf.openSession();
		Transaction tr = null;
		Tilastot tilastot = new Tilastot();
		try {
			tr = session.beginTransaction();
			session.load(tilastot, pelaajanNimi);
			tr.commit();
			return tilastot;
		} catch (Exception e) {
			if (tr != null)
				tr.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	@Override
	public Tilastot[] readTilastot() {
		Session session = sf.openSession();
		Transaction tr = null;
		List<Tilastot> result;
		try {
			tr = session.beginTransaction();
			result = session.createQuery("FROM " + Tilastot.class.getName()).getResultList();
			Tilastot[] tilastot = new Tilastot[result.size()];
			for (int i = 0; i < tilastot.length; i++) {
				tilastot[i] = result.get(i);
				System.out.println("i: " + i + ", tilastot.name: " + tilastot[i].getPelaajanNimi() + ", tilastot.data: "
						+ tilastot[i].getTilastot()[0] + "," + tilastot[i].getTilastot()[1] + ","
						+ tilastot[i].getTilastot()[0] + "," + tilastot[i].getTilastot()[3] + ","
						+ tilastot[i].getTilastot()[4] + "," + tilastot[i].getTilastot()[5] + ",");
			}
			tr.commit();
			return tilastot;
		} catch (Exception e) {
			if (tr != null)
				tr.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	@Override
	public boolean updateTilastot(Tilastot tilastot) {
		Session session = sf.openSession();
		Transaction tr = null;
		try {
			tr = session.beginTransaction();
			Tilastot t = (Tilastot) session.get(Tilastot.class, tilastot.getPelaajanNimi());
			if (t != null) {
				t.setTilastot(tilastot.getTilastot());
			} else {
				return false;
			}
			tr.commit();
			return true;
		} catch (Exception e) {
			if (tr != null)
				tr.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	@Override
	public boolean deleteTilastot(String pelaajanNimi) {
		Tilastot tilastot = new Tilastot();
		tilastot.setPelaajanNimi(pelaajanNimi);
		Session session = sf.openSession();
		Transaction tr = null;
		try {
			tr = session.beginTransaction();
			session.delete(tilastot);
			tr.commit();
			return true;
		} catch (Exception e) {
			if (tr != null)
				tr.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	@Override
	public boolean isSessionFactory() {
		if (sf == null)
			return false;
		else
			return true;
	}

	@Override
	public void closeSessionFactory() {
		if (isSessionFactory() && sf.isOpen()) {
			sf.close();
		}
	}
}