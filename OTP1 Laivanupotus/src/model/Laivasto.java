package model;

import java.util.ArrayList;

public class Laivasto implements Laivasto_IF {

	private ArrayList<Laiva> laivat;

	/**
	 * Konstruktori
	 */
	public Laivasto() {
		laivat = new ArrayList<Laiva>();
	}

	/**
	 * Lisää laivan laivastoon
	 *
	 * @return True, jos laivan lisäys onnistui
	 */
	@Override
	public boolean lisaaLaiva(ArrayList<Ruutu> ruudut) {
		return laivat.add(new Laiva(ruudut));
	}

	/**
	 * Palauttaa true jos laivaston laivat ovat uponneet, false jos kaikki eivät ole,
	 * tai laivastossa ei ole laivoja.
	 *
	 *
	 * @return True, jos laivaston kaikki laivat ovat uponneet
	 */
	@Override
	public boolean uponnut() {
		if (laivat.isEmpty())
			return false;

		for (Laiva laiva: laivat) {
			if (!laiva.getUponnut())
				return false;
		}

		return true;
	}

	/**
	 * Palauttaa taulukon jossa on laivaston laivojen käyttämien ruutujen koordinaatit.
	 *
	 *
	 * @return Koordinaatit, [0][x] [1][y]
	 */
	@Override
	public int[][] getLaivojenKoordinaatit() {

		int laivojenYhteiskoko = getLaivojenYhteiskoko();

		int[][] xy = new int[2][laivojenYhteiskoko];
		int counter = 0;

		for (Laiva laiva: laivat) {
			for (int i = 0; i < laiva.getRuudut().size(); i++) {
				xy[0][counter + i] = laiva.getRuudut().get(i).getX();
				xy[1][counter + i] = laiva.getRuudut().get(i).getY();
			}
			counter += laiva.getRuudut().size();
		}

		return xy;
	}

	/**
	 * Palauttaa laivastossa olevien laivojen käyttämien ruutujen määrän.
	 *
	 * @return Laivojen yhteiskoko
	 */
	@Override
	public int getLaivojenYhteiskoko() {
		int yhteiskoko = 0;

		for (Laiva laiva: laivat) {
			yhteiskoko += laiva.getRuudut().size();
		}

		return yhteiskoko;
	}
}
