package model;

public interface LaivanupotusPeli_IF {

	public abstract boolean uusiPelaaja(String nimi);
	public abstract boolean uusiPelaaja(String nimi, int x, int y);
	public abstract boolean poistaPelaaja(String nimi);
	public abstract void poistaKaikkiPelaajat();
	public abstract boolean lisaaLaiva(String nimi, int[] x, int[] y);
	public abstract char[][] getPelaajanKartta(String nimi, boolean fog_of_war);
	public abstract boolean pelaajanLaivatUponnut(String nimi);
	public abstract boolean ammu(String nimi, int x, int y);
	public abstract boolean onkoRuudussaLaiva(String nimi, int x, int y);
	public abstract int getPelaajanLaivojenYhteiskoko(String s);
	int[][] getPelaajanLaivojenKoordinaatit(String pelaajanNimi);
	int getPelaajanKartanKokoY(String pelaajanNimi);
	int getPelaajanKartanKokoX(String pelaajanNimi);
	boolean onPelaaja(String nimi);
}
