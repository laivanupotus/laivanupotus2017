package model;

public interface Pelaaja_IF {

	Ruudukko getRuudukko();
	Laivasto getLaivasto();
	boolean lisaaLaiva(int[] x, int[] y);
	void setKarttaKoko(int x, int y);

}
