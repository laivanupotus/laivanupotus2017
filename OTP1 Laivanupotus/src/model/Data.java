package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.control.Button;

public class Data implements Data_IF {
	private TilastotDAO_IF tdao;

	private String[] styles = new String[2];
	private String[] labels = new String[15];
	private String[] buttonLabels = new String[15];
	private String[] strings = new String[50];
	private String[] alerts = new String[30];

	private ArrayList<Tilastot> pelaajienTilastot = new ArrayList();

	private Map<Integer, Button> hashPlayerGrid = new HashMap();
	private Map<Integer, Button> hashOpponentGrid = new HashMap();

	public Data(TilastotDAO_IF tdao) {
		this.tdao = tdao;

		// Styles
		styles[0] = "-fx-background-image: url('https://i.imgur.com/qubRIW3.jpg');-fx-background-size: stretch;";
		styles[1] = "-fx-background-image: url('https://i.imgur.com/8477yAN.jpg');-fx-background-size: stretch;";

		// Labels
		labels[0] = "Moderni Laivanupotus";
		labels[1] = "Laivanupotus 2017";
		labels[2] = "Ampuminen: \nValitaan vastustajan ruutu, johon sitten ammutaan. Jos ruutu on tyhjä, se merkitään valkoisella. "
				+ "Jos ruudussa on vastustajan laiva, se merkitään värillisellä ruudulla. \n\nHäviö: \nPelaaja häviää, kun pelaajan laivasto on uponnut.\n\n"
				+ "Laiva: \nYksittäinen pelinappula, jonka pelaaja sijoittaa pelaajan ruudukkoon. "
				+ "Laivat voivat olla yhden tai useamman ruudun kokoisia.\n\nLaivasto: \nPelaajan kaikki laivat.\n\n"
				+ "Pelaajan ruudukko: \nAlue, johon pelaaja sijoittaa laivastonsa. Pelaajan ruudukkoon merkitään myös vastustajan ampumiset.\n\n"
				+ "Pelaaja: \nPelaaja on sovelluksen käyttäjä.\n\nRuutu: \nPienin yksittäinen osa ruudukkoa. Ruudun väri on harmaa.\n\n"
				+ "Uponnut laiva: \nLaiva uppoaa, kun jokaiseen sen viemään ruutuun on ammuttu.\n\n"
				+ "Vastustaja: \nOn osapuoli, jota vastaan pelaaja pelaa.\n\n"
				+ "Vastustajan ruudukko: \nTyhjä ruudukko, johon merkitään pelaajan ampumiset. Sisältö on pelaajalle tuntematon. "
				+ "Ruutujen sisältö paljastuu niihin ampumalla.\n\nVoitto: \nPelaaja voittaa, kun vastustajan koko laivasto on uponnut.\n\n"
				+ "Vuoro: \nAika, jonka sisällä pelaaja suorittaa yhden ampumisen. Vuoron päätyttyä alkaa vastustajan vuoro.";
		labels[3] = "Laivan koko: ";
		labels[4] = "Tekoälyn vaikeusaste: ";
		labels[5] = "Syötä pelaajan nimi: ";
		labels[6] = "Laivaruudut: ";

		// Button Labels
		buttonLabels[0] = "Uusi peli";
		buttonLabels[1] = "Asetukset";
		buttonLabels[2] = "Ohje";
		buttonLabels[3] = "Yksinpeli";
		buttonLabels[4] = "Moninpeli";
		buttonLabels[5] = "Päävalikkoon";
		buttonLabels[6] = "Uusi peli";
		buttonLabels[7] = "Liity peliin";
		buttonLabels[8] = "Lopeta";
		buttonLabels[9] = "Tilastot";
		buttonLabels[10] = "Luo / Vaihda Pelaaja";
		buttonLabels[11] = "Poista Pelaaja";

		// Strings
		strings[0] = "\n\n= = = = = = = = = = = = = = =\n\n";
		strings[1] = "\n\n\t# # # # #\n\n";
		strings[2] = "Nykyistä pelaajaa ei ole valittu. ";
		strings[3] = "Nykyinen pelaaja: ";
		strings[4] = "Osumia ";
		strings[5] = "Huteja ";
		strings[6] = "Osumaprosentti ";
		strings[7] = "Voittoja ";
		strings[8] = "Häviöitä ";
		strings[9] = "Voittoprosentti ";
		strings[10] = "Voittoja ";
		strings[11] = "Laiva lisätty pisteisiin ";
		strings[12] = " ruutuja käytetty ";
		strings[13] = "Kaikki laivat lisätty! ";
		strings[14] = "Vastustaja osui laivaasi. ";
		strings[15] = "Hävisit pelin!";
		strings[16] = "Vastustaja ei osunut laivaasi. ";
		strings[17] = "Et voi ampua samaan ruutuun enempää kuin kerran. ";
		strings[18] = "Osuit laivaan. ";
		strings[19] = "Voitit pelin! ";
		strings[20] = "Et osunut laivaan. ";
		strings[21] = "Luo uusi peli";
		strings[22] = "Portti tarvitaan pelin luomiseen. ";
		strings[23] = "Syötä validi portti:";
		strings[24] = "Liity peliin";
		strings[25] = "IP-osoite tarvitaan pelin yhdistämiseen.";
		strings[26] = "Syötä validi IP-osoite:";
		strings[27] = "Portti tarvitaan pelin yhdistämiseen.";
		strings[28] = "Laivanupotus | Asetukset";
		strings[29] = "";
		strings[30] = "";


		// Dialog & Alert strings
		alerts[0] = "KAIKKI LAIVAT LISäTTY";
		alerts[1] = "Valmista!";
		alerts[2] = "Laivoja ei voi lisätä enempää.";
		alerts[3] = "YRITIT AMPUA";
		alerts[4] = "Ei voi ampua!";
		alerts[5] = "Tähän ruutuun on jo ammuttu.";
		alerts[6] = "AMMUIT";
		alerts[7] = "Upotit vastustajan laivaston.";
		alerts[8] = "Voitit pelin!";
		alerts[9] = "VASTUSTAJA AMPUI";
		alerts[10] = "Koko laivastosi upposi.";
		alerts[11] = "Hävisit pelin!";
		alerts[12] = "LUOVUTIT";
		alerts[13] = "Lähdit pelistä kesken kaiken.";
		alerts[14] = "VUORO";
		alerts[15] = "Vastapelaajan vuoro";
		alerts[16] = "Ole hyvä ja odota kunnes vastapelaaja on luovuttanut vuoronsa.";
		alerts[17] = "Luo uusi peli - Virheellinen portti";
		alerts[18] = "Portti ei ollut numeerista muotoa.";
		alerts[19] = "Ole hyvä ja yritä uudelleen.";
		alerts[20] = "Liity peliin - Kohdattiin virhe";
		alerts[21] = "Tuntematon virhe.";
		alerts[22] = "TIETOKANTA";
		alerts[23] = "Päivitys onnistui.";
		alerts[24] = "Pelikierroksen tilastot päivitettiin onnistuneesti tietokantaan.";
		alerts[25] = "Päivitys epäonnistui.";
		alerts[26] = "Pelikierroksen tilastot ei päivitetty tietokantaan.";
		alerts[27] = "";
	}

	/**
	 * @return Palauta merkkijono
	 */
	@Override
	public String getString(char c, int i) {
		switch (c) {
		case 's':
			if (i < styles.length) {
				return styles[i];
			}
		case 'l':
			if (i < labels.length) {
				return labels[i];
			}
		case 'b':
			if (i < buttonLabels.length) {
				return buttonLabels[i];
			}
		case 't':
			if (i < strings.length) {
				return strings[i];
			}
		case 'a':
			if (i < alerts.length) {
				return alerts[i];
			}
		}
		return null;
	}

	/**
	 * @return Palauta peliruudukko
	 */
	@Override
	public Map<Integer, Button> getHashMap(char c) {
		switch (c) {
		case 'p':
			return hashPlayerGrid;
		case 'o':
			return hashOpponentGrid;
		}
		return null;
	}

	/**
	 * @return Palauta annetun koordinaatistopisteen button valitussa ruudukossa
	 */
	@Override
	public Button getHashButton(int h, char c) {
		switch (c) {
		case 'p':
			return hashPlayerGrid.get(h);
		case 'o':
			return hashOpponentGrid.get(h);
		}
		return null;
	}

	/**
	 * Aseta button annettuun koordinaattipisteeseen valitussa ruudukossa
	 */
	@Override
	public void setHashButton(Button gb, int x, int y, char c) {
		switch (c) {
		case 'p':
			hashPlayerGrid.put(Integer.valueOf(y + "" + x), gb);
			break;
		case 'o':
			hashOpponentGrid.put(Integer.valueOf(y + "" + x), gb);
			break;
		}
	}

	/**
	 * @param pelaajanNimi
	 *            Lisättävän pelaajan nimi mikäli sitä ei ole jo olemassa.
	 */
	@Override
	public void addPlayer(String pelaajanNimi) {
		boolean found = false;
		// Tarkistetaan löytyykö annetun nimistä pelaajaa
		found = searchForPlayer(pelaajanNimi);
		// Jos ei löytynyt niin lisätään
		if (!found) {
			pelaajienTilastot.add(new Tilastot(pelaajanNimi));
			if (tdao.isSessionFactory()) {
				tdao.createTilastot(getLocalPlayer(pelaajanNimi));
			}
		}
		// Järjestetään pelaajat aakkosjärjestykseen:
		Collections.sort(pelaajienTilastot);
	}

	/**
	 * @param s
	 *            Ilmaisee mikä switchin case-vaihtoehto valitaan
	 * @param pelaajanNimi
	 *            Minkä nimistä pelaajaa muutos koskee
	 */
	@Override
	public void setTilastot(String s, String pelaajanNimi) {
		Tilastot t = new Tilastot();
		boolean found = false;
		// Tarkistetaan löytyykö annetun nimistä pelaajaa
		found = searchForPlayer(pelaajanNimi);
		// Jos ei löytynyt, niin lisätään pelaaja
		if (!found) {
			addPlayer(pelaajanNimi);
			t = getLocalPlayer(pelaajanNimi);
			// Muutoin asetetaan t:ksi olemassa oleva pelaaja
		} else {
			t = getLocalPlayer(pelaajanNimi);
		}
		double osumaProsentti;
		double voittoProsentti;
		switch (s) {
		case "osuma":
			// Yksi (1) lisätään nykyisiin osumiin, osumaprosentti muuttuu
			// (osumat / (osumat + hudit))
			t.setTilastot(t.getTilastot()[0] + 1, 0);
			osumaProsentti = t.getTilastot()[0] / (t.getTilastot()[0] + t.getTilastot()[1]);
			t.setTilastot(osumaProsentti, 2);
			break;
		case "huti":
			// Yksi (1) lisätään nykyisiin huteihin, osumaprosentti muuttuu
			// (osumat / (osumat + hudit))
			t.setTilastot(t.getTilastot()[1] + 1, 1);
			osumaProsentti = t.getTilastot()[0] / (t.getTilastot()[0] + t.getTilastot()[1]);
			t.setTilastot(osumaProsentti, 2);
			break;
		case "voitto":
			// Yksi (1) lisätään nykyisiin voittoihin, voittoprosentti muuttuu
			// (voitot / (voitot + häviöt))
			t.setTilastot(t.getTilastot()[3] + 1, 3);
			voittoProsentti = t.getTilastot()[3] / (t.getTilastot()[3] + t.getTilastot()[4]);
			t.setTilastot(voittoProsentti, 5);
			break;
		case "häviö":
			// Yksi (1) lisätään nykyisiin häviöihin, voittoprosentti muuttuu
			// (voitot / (voitot + häviöt))
			t.setTilastot(t.getTilastot()[4] + 1, 4);
			voittoProsentti = t.getTilastot()[3] / (t.getTilastot()[3] + t.getTilastot()[4]);
			t.setTilastot(voittoProsentti, 5);
			break;
		default:
			break;
		}
	}

	/**
	 * @param pelaajanNimi
	 *            Etsittävän pelaajan nimi.
	 * @return Palautetaan true jos löytyi, false jos ei löytynyt
	 */
	private boolean searchForPlayer(String pelaajanNimi) {
		for (Tilastot x : pelaajienTilastot) {
			if (x.getPelaajanNimi().equals(pelaajanNimi)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param pelaajanNimi
	 *            Etsittävän pelaajan nimi.
	 * @return Palautetaan löydetty pelaaja jos löytyi, null jos ei löytynyt
	 */
	private Tilastot getLocalPlayer(String pelaajanNimi) {
		for (Tilastot x : pelaajienTilastot) {
			if (x.getPelaajanNimi().equals(pelaajanNimi)) {
				return x;
			}
		}
		return null;
	}

	/**
	 * @param pelaajanNimi
	 *            Haettavan pelaajan nimi kaikkien pelaajien joukosta
	 * @return Palautetaan Tilastot-olio (sisältää yksittäisen pelaajan tiedot)
	 *         tai null (jos pelaajaa ei löytynyt)
	 */
	@Override
	public Tilastot getTilastot(String pelaajanNimi) {
		if (tdao.isSessionFactory()) {
			pelaajienTilastot.clear();
			Collections.addAll(pelaajienTilastot, tdao.readTilastot());
		}
		for (Tilastot x : pelaajienTilastot) {
			if (x.getPelaajanNimi().equals(pelaajanNimi)) {
				return x;
			}
		}
		return null;
	}

	@Override
	public int getTilastotIndex(String pelaajanNimi) {
		return pelaajienTilastot.indexOf(getTilastot(pelaajanNimi));
	}

	/**
	 * @param index
	 *            Haettavan pelaajan indeksi ArrayList-rakenteesta
	 * @return Palautetaan Tilastot-olio (sisältää yksittäisen pelaajan tiedot)
	 *         tai null (jos indeksi ei ollut pienempi kuin ArrayListin koko)
	 */
	@Override
	public Tilastot_IF getTilastot(int index) {
		if (tdao.isSessionFactory()) {
			pelaajienTilastot.clear();
			Collections.addAll(pelaajienTilastot, tdao.readTilastot());
		}
		if (index < pelaajienTilastot.size()) {
			return pelaajienTilastot.get(index);
		}
		return null;
	}

	/**
	 * @return Palautetaan koko ArrayList, joka sisältää kaikki Tilastot-oliot
	 */
	@Override
	public ArrayList<Tilastot> getTilastot() {
		if (tdao.isSessionFactory()) {
			pelaajienTilastot.clear();
			Collections.addAll(pelaajienTilastot, tdao.readTilastot());
		}
		return pelaajienTilastot;
	}

	@Override
	public boolean updateTilastotDAO(String pelaajanNimi) {
		if (!tdao.isSessionFactory()) {
			tdao = new TilastotDAO();
		} else if (tdao.isSessionFactory() && searchForPlayer(pelaajanNimi)) {
			tdao.updateTilastot(getLocalPlayer(pelaajanNimi));
			return true;
		}
		return false;
	}

}
