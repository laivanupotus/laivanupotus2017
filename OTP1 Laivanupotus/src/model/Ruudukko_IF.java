package model;

public interface Ruudukko_IF {

	char[][] getKartta(boolean fog_of_war);

	boolean ammu(int x, int y);

	Ruutu getRuutu(int x, int y);

	int getY();

	int getX();

	boolean onTyhja(int x, int y);

	boolean onRuudukossa(int x, int y);


}
