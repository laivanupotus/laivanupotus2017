package model;

import java.util.ArrayList;

public interface Laiva_IF {

	void setRuudut(ArrayList<Ruutu> ruudut);

	ArrayList<Ruutu> getRuudut();

	void setUponnut(boolean uponnut);

	boolean getUponnut();

	void upota();


}
