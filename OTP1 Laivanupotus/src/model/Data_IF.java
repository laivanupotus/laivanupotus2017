package model;

import java.util.ArrayList;
import java.util.Map;

import javafx.scene.control.Button;

public interface Data_IF {

	public abstract String getString(char c, int i);
	public abstract Map<Integer, Button> getHashMap(char c);
	public abstract Button getHashButton(int h, char c);
	public abstract void setHashButton(Button gb, int i, int j, char c);
	public abstract void setTilastot(String s, String pelaajanNimi);
	public abstract Tilastot getTilastot(String pelaajanNimi);
	public abstract Tilastot_IF getTilastot(int index);
	public abstract ArrayList<Tilastot> getTilastot();
	public abstract void addPlayer(String pelaajanNimi);
	public abstract int getTilastotIndex(String pelaajanNimi);
	public abstract boolean updateTilastotDAO(String pelaajanNimi);

}
