package model;

import java.util.Random;

public class LaivanupotusAI implements LaivanupotusAI_IF {

	private final static int DEFAULT_VAIKEUSTASO = 2;
	private final static int DEFAULT_HUIJAUSPROSENTTI = 40;
	private LaivanupotusPeli laivanupotusPeli;
	private int laukausX; // viimeisimmän ampumisen koordinaatit
	private int laukausY;
	private int[][] xy;
	private char[][] kartta;
	private int vaikeusTaso;
	private Random random;
	private int huijausProsentti;

	/**
	 * Konstruktori.
	 *
	 * @param lup
	 *            LaivanupotusPeli mitä AI pelaa.
	 */
	public LaivanupotusAI(LaivanupotusPeli_IF lup) {
		this.laivanupotusPeli = (LaivanupotusPeli) lup;
		vaikeusTaso = DEFAULT_VAIKEUSTASO;
		huijausProsentti = DEFAULT_HUIJAUSPROSENTTI;
		random = new Random();
	}

	/**
	 * Suorittaa ampumisen annetun pelaajan ruudukolle.
	 *
	 * @param pelaajanNimi
	 *            Kohdepelaajan nimi.
	 * @return True jos ampuminen onnistui.
	 */
	@Override
	public boolean ammu(String pelaajanNimi) {
		switch(vaikeusTaso) {
			case 1:
				return normiAmmu(pelaajanNimi);
			case 2:
				return smartAmmu(pelaajanNimi);
			case 3:
				return superAmmu(pelaajanNimi);
			case 4:
				return huijausAmmu(pelaajanNimi);
			default:
				return normiAmmu(pelaajanNimi);
		}
	}

	/**
	 *	Suoritetaan täysin satunnainen ampuminen annetun pelaajan ruudukkoon.
	 *
	 * @param pelaajanNimi annetun pelaajan nimi
	 * @return True, jos ampuminen onnistui
	 */
	@Override
	public boolean normiAmmu(String pelaajanNimi) {
		if (!laivanupotusPeli.onPelaaja(pelaajanNimi))
			return false;

		// kaikkiin ruutuihin ammuttu ??
		// TODO

		int ruudunKokoX = laivanupotusPeli.getPelaajanKartanKokoX(pelaajanNimi);
		int ruudunKokoY = laivanupotusPeli.getPelaajanKartanKokoY(pelaajanNimi);

		// random-laukaus
		do {
			laukausX = random.nextInt(ruudunKokoX);
			laukausY = random.nextInt(ruudunKokoY);
		} while (!laivanupotusPeli.ammu(pelaajanNimi, laukausX, laukausY));

		return true;
	}

	/**
	 * Jos ruudulla on laiva johon on osuttu, suoritetaan ampuminen sen ympärillä oleviin
	 * ruutuihin 4-ilman suunnan mukaan. Jos laivaa ei ole ruudulla näkyvissä, tai kaikkiin
	 * niiden ympärillä oleviin ruutuihin on jo ammuttu, siirrytään normiAmmu metodiin
	 *
	 *  @param pelaajanNimi Ammuttavan pelaajan nimi
	 *  @return True jos ampuminen onnistui.
	 *
	 */
	@Override
	public boolean smartAmmu(String pelaajanNimi) {
		if (!laivanupotusPeli.onPelaaja(pelaajanNimi))
			return false;

		char[][] kartta = laivanupotusPeli.getPelaajanKartta(pelaajanNimi, true);

		for (int i = 0; i < kartta.length; i++) {
			for (int j = 0; j < kartta[0].length; j++) {

				if (kartta[i][j] == Ruudukko.SYMBOL_LAUKAUS_OSUMA) {
					if (laivanupotusPeli.ammu(pelaajanNimi, j + 1, i)) {
						laukausX = j + 1;
						laukausY = i;
						return true;
					}
					else if (laivanupotusPeli.ammu(pelaajanNimi, j - 1, i)) {
						laukausX = j - 1;
						laukausY = i;
						return true;
					}
					else if (laivanupotusPeli.ammu(pelaajanNimi, j, i + 1)) {
						laukausX = j;
						laukausY = i + 1;
						return true;
					}
					else if (laivanupotusPeli.ammu(pelaajanNimi, j, i - 1)) {
						laukausX = j;
						laukausY = i - 1;
						return true;
					}
				}
			}
		}

		return normiAmmu(pelaajanNimi);
	}

	/**
	 * Ampuminen jossa jokaisella laukauksella on mahdollisuus osua ammuttavan pelaajan laivaan.
	 * Jos osutaan laivaan johon on jo ammuttu, siirrytään smartAmmu metodiin.
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui
	 *
	 */
	@Override
	public boolean superAmmu(String pelaajanNimi) {
		int[][] xy = laivanupotusPeli.getPelaajanLaivojenKoordinaatit(pelaajanNimi);

		if (xy[0].length > 0) {
			int target = random.nextInt(xy[0].length);

			if (laivanupotusPeli.ammu(pelaajanNimi, xy[0][target], xy[1][target])) {
				laukausX = xy[0][target];
				laukausY = xy[1][target];

				return true;
			}
		}
		return smartAmmu(pelaajanNimi);
	}


	/**
	 * Suorittaa huijausampumisen. Ampumisella on huijausprosentin määräämä mahdollisuus
	 * osua annetun pelaajan laivaan. Jos laukaus ei osu laivaan, siirrytään smartAmmu metodiin.
	 *
	 *
	 * @param pelaajanNimi Ammuttavan pelaajan nimi
	 * @return True jos ampuminen onnistui
	 */
	@Override
	public boolean huijausAmmu(String pelaajanNimi) {
		int huijaus = random.nextInt(100) + 1;

		if (huijaus <= huijausProsentti) {
			int[][] xy = laivanupotusPeli.getPelaajanLaivojenKoordinaatit(pelaajanNimi);

			int r = random.nextInt(xy[0].length);

			for (int i = 0; i < xy[0].length; i++) {
				if (r == xy[0].length)
					r = 0;

				if (laivanupotusPeli.ammu(pelaajanNimi, xy[0][r], xy[1][r])) {
					laukausX = xy[0][r];
					laukausY = xy[1][r];

					return true;
				}

				r++;
			}
		}

		return smartAmmu(pelaajanNimi);
	}


	/**
	 * Yrittää lisätä laivan määritellylle pelaajalle satunnaiseen paikkaan kartalla.
	 *
	 *
	 * @param pelaajanNimi Pelaajan nimi jolle laiva lisätään
	 * @param laivanPituus Lisättävän laivan pituus
	 *
	 * @return True, jos lisäys onnistui
	 */
	@Override
	public boolean asetaLaiva(String pelaajanNimi, int laivanPituus) {
		if (!laivanupotusPeli.onPelaaja(pelaajanNimi))
			return false;

		kartta = laivanupotusPeli.getPelaajanKartta(pelaajanNimi, false);
		int kartanKokoX = laivanupotusPeli.getPelaajanKartanKokoX(pelaajanNimi);
		int kartanKokoY = laivanupotusPeli.getPelaajanKartanKokoY(pelaajanNimi);

		int suunnat = 4;
		Random random = new Random();
		int x = random.nextInt(kartanKokoX);
		int y = random.nextInt(kartanKokoY);
		int suunta = random.nextInt(suunnat);
		xy = new int[2][laivanPituus];

		if (kartta[y][x] == Ruudukko.SYMBOL_MERI) {
			xy[0][0] = x;
			xy[1][0] = y;

			for (int i = 0; i < suunnat; i++) {

				if (suunta == suunnat)
					suunta = 0;

				switch (suunta) {
				case 0:
					for (int j = 1; j < laivanPituus; j++)
						tarkastaRuudut(kartta, xy, j, 0, j);

					if (laivanupotusPeli.lisaaLaiva(pelaajanNimi, xy[0], xy[1])) {
						return true;
					}
					break;
				case 1:
					for (int j = 1; j < laivanPituus; j++)
						tarkastaRuudut(kartta, xy, j, j, 0);

					if (laivanupotusPeli.lisaaLaiva(pelaajanNimi, xy[0], xy[1])) {
						return true;
					}
					break;
				case 2:
					for (int j = 1; j < laivanPituus; j++)
						tarkastaRuudut(kartta, xy, j, 0, -j);

					if (laivanupotusPeli.lisaaLaiva(pelaajanNimi, xy[0], xy[1])) {
						return true;
					}
					break;
				case 3:
					for (int j = 1; j < laivanPituus; j++)
						tarkastaRuudut(kartta, xy, j, -j, 0);

					if (laivanupotusPeli.lisaaLaiva(pelaajanNimi, xy[0], xy[1])) {
						return true;
					}
					break;
				}
				suunta++;
			}
		}
		return false;
	}

	/**
	 * Tarkastaa onko annetussa ruudussa merta vai ei, ja muokkaa xy taulukkoa sen perusteella.
	 * -1 arvoiksi jos tarkasteltavaan kohtaan ei voi lisätä laivaa.
	 *
	 * @param kartta Pelaajan char[y][x] kartta
	 * @param xy [0][x] [1][y] taulukko koordinaateista
	 * @param offset tarkastettavan taulukon kohdan muutos xy-taulukon [0][0] [1][0] ruuduista
	 * @param x x-koordinaatin muutos
	 * @param y y-koordinaatin muutos
	 */
	private void tarkastaRuudut(char[][] kartta, int[][] xy, int offset, int x, int y) {
		try {
			if (kartta[xy[1][0] + y][xy[0][0] + x] == Ruudukko.SYMBOL_MERI) {
				xy[0][offset] = xy[0][0] + x;
				xy[1][offset] = xy[1][0] + y;
			} else {
				xy[0][offset] = -1;
				xy[1][offset] = -1;
			}
		} catch (Exception e) {
			xy[0][offset] = -1;
			xy[1][offset] = -1;
		}
	}

	/**
	 * Viimeisimmän ampumisen X koordinaatti.
	 *
	 * @return X - koordinaatti.
	 */
	@Override
	public int getLaukausX() {
		return laukausX;
	}

	/**
	 * Viimeisimmän ampumisen Y koordinaatti.
	 *
	 * @return Y - koordinaatti.
	 */
	@Override
	public int getLaukausY() {
		return laukausY;
	}

	/**
	 * Viimeisimmän asennetun laivan koordinaatit
	 *
	 * @return int[][] taulukko, [0][x] [1][y]
	 */
	@Override
	public int[][] getXY() {
		return xy;
	}


	/**
	 * Palauttaa vaikeustason.
	 *
	 * @return Vaikeustaso
	 */
	@Override
	public int getVaikeusTaso() {
		return vaikeusTaso;
	}

	/**
	 * Asettaa vaikeustason (1, 2, 3, 4, ...)
	 *
	 * @param vaikeusTaso Vaikeustaso
	 */
	@Override
	public void setVaikeusTaso(int vaikeusTaso) {
		this.vaikeusTaso = vaikeusTaso;
	}

	/**
	 * @return Palauttaa huijausprosentin.
	 */
	@Override
	public int getHuijausProsentti() {
		return huijausProsentti;
	}

	/**
	 * Asettaa huijausprosentin.
	 *
	 * @param prosentti Huijausprosentti
	 */
	@Override
	public void setHuijausProsentti(int prosentti) {
		huijausProsentti = prosentti;
	}
}
