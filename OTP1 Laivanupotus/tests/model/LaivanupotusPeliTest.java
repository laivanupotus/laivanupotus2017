package model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class LaivanupotusPeliTest {

	private static LaivanupotusPeli lp = new LaivanupotusPeli();


	// tyhjentaa pelaajalista aina testin paatyttya
	@After
	public void tyhjennaPelaajat() {
		lp.poistaKaikkiPelaajat();
	}

	// uuden pelaajan lisays
	@Test
	public void testLisaaPelaaja() {
		boolean uusi1 = lp.uusiPelaaja("apina");
		boolean uusi2 = lp.uusiPelaaja("apina");
		assertTrue("pelaajan lisays", uusi1);
		assertFalse("pelaajan lisays toistamiseen", uusi2);
	}

	// pelaajan poistos
	@Test
	public void testPoistaPelaaja() {
		boolean lisays = lp.uusiPelaaja("apina");
		boolean poisto = lp.poistaPelaaja("apina");
		boolean poisto2 = lp.poistaPelaaja("apina");
		assertTrue("pelaajan lisays", lisays);
		assertTrue("pelaajan poisto", poisto);
		assertFalse("pelaajan poisto toistamiseen", poisto2);
	}

	// uuden pelaajan lisays ja kartan koon maarittaminen
	@Test
	public void testLisaaPelaaja2() {
		boolean uusi1 = lp.uusiPelaaja("apina", 4, 5);
		boolean uusi2 = lp.uusiPelaaja("apina", 3, 5);
		assertTrue("pelaajan lisays", uusi1);
		assertFalse("pelaajan lisays toistamiseen", uusi2);
	}

	// uuden pelaajan lisays huonolla kartan koolla
	@Test
	public void testLisaaPelaaja3() {
		boolean uusi1 = lp.uusiPelaaja("apina", -1, -1);
		assertFalse("pelaajan lis�ys huonot koordinaatit", uusi1);
	}

	// laivan lisays
	@Test
	public void testLisaaLaiva1() {
		lp.uusiPelaaja("apina");
		int[] x = {1, 2, 3};
		int[] y = {1, 2, 3};
		boolean lisays1 = lp.lisaaLaiva("keke", x, y); // tuntematon pelaaja
		boolean lisays2 = lp.lisaaLaiva("apina", x, y);

		assertFalse("lisays olemattomalle pelaajalle", lisays1);
		assertTrue("Lisays", lisays2);
	}

	// laivan lisays koordinaattien testit
	@Test
	public void testLisaaLaiva2() {
		lp.uusiPelaaja("apina");
		int[] x = {1, 2, 3};
		int[] y = {};
		int[] z = {1, 2};
		boolean lisays1 = lp.lisaaLaiva("apina", x, z); // x y koord. eri kokoisia
		boolean lisays2 = lp.lisaaLaiva("apina", x, x); // x y koord. samankokoisia, ok
		boolean lisays3 = lp.lisaaLaiva("apina", y, y); // ei koordinaatteja

		assertFalse("eri maara koordinaatteja", lisays1);
		assertTrue("Lisays", lisays2);
		assertFalse("Lisays ilman koordinaatteja", lisays3);
	}


	// kartanhaku testit
	@Test
	public void testKartta1() {
		char[][] kartta1 = lp.getPelaajanKartta("apina", false);

		lp.uusiPelaaja("apina");
		char[][] kartta2 = lp.getPelaajanKartta("apina", false);
		char[][] vertailuKartta2 = new char[10][10];

		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++) {
				vertailuKartta2[i][j] = '~';
			}

		int x = 3;
		int y = 5;

		lp.uusiPelaaja("apina2", x, y);
		char[][] kartta3 = lp.getPelaajanKartta("apina2", false);
		char[][] vertailuKartta3 = new char[y][x];

		for (int i = 0; i < y; i++)
			for (int j = 0; j < x; j++) {
				vertailuKartta3[i][j] = '~';
			}

		assertArrayEquals("kartta olemattomalta pelaajalta", null, kartta1);
		assertArrayEquals("default kartta", vertailuKartta2, kartta2);
		assertArrayEquals("custom kartta", vertailuKartta3, kartta3);
	}

	// fix later
	/*
	@Ignore
	@Test
	public void testRuudukkoToString() {
		lp.uusiPelaaja("apina", 3, 5);
		int[] x = {1,2};
		int[] y = {1,1};
		lp.lisaaLaiva("apina", x, y);
		y[1] = 2; // toinen ruutu j�� vanhan laivan p��lle
		lp.lisaaLaiva("apina", x, y);
		x[0] = 2;
		x[1] = 3;// out of bounds
		y[1] = 0;
		y[0] = 0;
		lp.lisaaLaiva("apina", x, y);

		String vertailu = "~~~\n~SS\n~~~\n~~~\n~~~\n";
		String generoitu = lp.pelaajanRuudukkoToString("apina");

		System.out.println(vertailu);
		System.out.println(generoitu);

		assertEquals("piirrosten vertailu", vertailu, generoitu);
	}
*/

	@Test
	public void testKartta2() {
		lp.uusiPelaaja("apina", 4, 4);
		int[] x = {0,1,2,3};
		int[] y = {0,1,2,3};
		lp.lisaaLaiva("apina", x, y);

		char[][] kartta1 = lp.getPelaajanKartta("apina", false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}
	}

	@Test
	public void testAmmu() {
		lp.uusiPelaaja("apina", 4, 4);
		int[] x = {0,1,2,3};
		int[] y = {0,1,2,3};
		lp.lisaaLaiva("apina", x, y);

		boolean ammu1 = lp.ammu("apina", 0, 0);
		boolean ammu2 = lp.ammu("apina", 1, 0);
		boolean ammu3 = lp.ammu("apina", 1, 0);
		boolean ammu4 = lp.ammu("apina", 5, 0);

		char[][] kartta1 = lp.getPelaajanKartta("apina", false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}

		assertTrue("ampuminen laivaan", ammu1);
		assertTrue("ampuminen veteen", ammu2);
		assertFalse("ampuminen uudestaan samaan", ammu3);
		assertFalse("ampuminen ohi kartan", ammu4);


	}

	@Test
	public void testUpotus() {
		lp.uusiPelaaja("apina", 4, 4);
		int[] x = {0,1,2,3};
		int[] y = {0,1,2,3};

		boolean uponnut1 = lp.pelaajanLaivatUponnut("apina");

		lp.lisaaLaiva("apina", x, y);

		lp.ammu("apina", 0, 0);
		lp.ammu("apina", 1, 1);
		lp.ammu("apina", 2, 2);

		boolean uponnut2 = lp.pelaajanLaivatUponnut("apina");

		lp.ammu("apina", 3, 3);

		boolean uponnut3 = lp.pelaajanLaivatUponnut("apina");


		x = new int[]{2,3};
		y = new int[]{0,0};

		lp.lisaaLaiva("apina", x, y);

		char[][] kartta1 = lp.getPelaajanKartta("apina", false);

		for (int i = 0; i < kartta1.length; i++) {
			for (int j = 0; j < kartta1[i].length; j++) {
				System.out.print(kartta1[i][j]);
			}
			System.out.printf("\n");
		}

		boolean uponnut4 = lp.pelaajanLaivatUponnut("apina");

		assertFalse("ei laivoja laivastossa", uponnut1);
		assertFalse("laiva ei taysin ammuttu", uponnut2);
		assertTrue("uponnut", uponnut3);
		assertFalse("toine laiva ehja", uponnut4);

	}
}
