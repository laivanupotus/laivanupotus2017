package model;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javafx.scene.control.Button;

public class DataTest {

	private static Data data;
	private Map<Integer, Boolean> hashShips = new HashMap(); // Laivat ruudukossa
	private Map<Integer, Button> hashPlayerGrid = new HashMap(); // Pelaajan ruudukon hashmap
	private Map<Integer, Button> hashOpponentGrid = new HashMap(); // Vastapelaajan ruudukon hashmap
	private Map<Integer, Boolean> hashOpponentShips = new HashMap(); // Vastapelaajan laivat ruudukossa



	@Before
	public void LuoData(){
		data = new Data(null);

	}

	@Test
	public void TestgetString(){
		int i = 0;
		char c = 's';

		String vastaus1 = data.getString(c, i);

		c = 'l';

		String vastaus2 = data.getString(c, i);

		c = 'b';

		String vastaus3 = data.getString(c, i);

		assertEquals("-fx-background-image: url('https://i.imgur.com/qubRIW3.jpg');-fx-background-size: stretch;", vastaus1);
		assertEquals("Moderni Laivanupotus", vastaus2);
		assertEquals("Uusi peli", vastaus3);
	}

	@Test
	public void TestgetHashMap(){
		char c = 'p';

		hashPlayerGrid = data.getHashMap(c);

		c = '0';

		hashOpponentGrid = data.getHashMap(c);
	}

	@Test
	public void TestgetHashShip(){
		int h = 4;
//		boolean c = data.getHashShip(h);

//		assertFalse(c);

	}



}
